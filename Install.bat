ECHO OFF
SET BASEDIR= %~dp0
SET ZIPERAPP= %BASEDIR%tools\7z
CLS
:MENU
ECHO.
ECHO ...............................................
ECHO =========== Sistem Informasi Akuntansi Bimba AIUEO Ungasan ===========
ECHO ----------------------------------------------------------------------
ECHO Pilih menu berikut untuk melakukan proses installasi aplikasi
ECHO ...............................................
ECHO.
ECHO 1 - Install Applikasi
ECHO 2 - Kosongkan Database
ECHO 3 - Update Aplikasi
ECHO 4 - Update Database
ECHO 5 - Extract Asset
ECHO 6 - Generate Sample Data
ECHO 7 - Start Aplikasi
ECHO 8 - EXIT
ECHO.
SET /P M=Type 1, 2, 3, 4 or 5 then press ENTER:
IF %M%==1 GOTO INST
IF %M%==2 GOTO CLEANDB
IF %M%==3 GOTO UPDT
IF %M%==4 GOTO UPDTDB
IF %M%==5 GOTO EXTRACX
IF %M%==6 GOTO GENDUM
IF %M%==7 GOTO STRT
IF %M%==8 GOTO EOF
:INST
CLS
ECHO ...............................................
ECHO Extract vendor zip
copy .env.sample .env
%ZIPERAPP%\7z x vendor.zip -y
ECHO proses migration database
mysql -u root < migration/install.sql
yii migrate
ECHO Finish install.
ECHO ...............................................
GOTO MENU

:CLEANDB
CLS
ECHO ...............................................
ECHO Rebuid Database
mysql -u root < migration/clean_db.sql
ECHO proses migration database
yii migrate
ECHO Finish Cleaning.
ECHO ...............................................
GOTO MENU


:UPDT
CLS
ECHO ...............................................
ECHO Pastikan koneksi internet stabil dan sudah terinstall git pada komputer
ECHO ...............................................
ECHO Download update aplikasi dari server
git pull origin master
ECHO Finish update.
ECHO ...............................................
GOTO MENU

:UPDTDB
CLS
ECHO proses migration database
yii migrate
ECHO Finish update.
ECHO ...............................................
GOTO MENU

:EXTRACX
CLS
ECHO Extract Vendor Assets
ECHO Extract vendor zip terbaru
%ZIPERAPP%\7z x vendor.zip -y
ECHO Finish Extract.
ECHO ...............................................
GOTO MENU

:GENDUM
CLS
ECHO Generata Sample Data
yii sample
GOTO MENU

:STRT
CLS
ECHO ...............................................
ECHO Start Server Aplikasi
ECHO Pastikan halaman ini tetap aktif saat mengakses aplikasi
ECHO Akses aplikasi pada web browser dengan alamat http://localhost:89
ECHO ...............................................
start http://localhost:89
yii serve --port=89
GOTO MENU

