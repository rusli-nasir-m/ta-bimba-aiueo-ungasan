<?php

namespace app\bin\behavior;

use yii\base\UserException;
/**
 * Description of StateChangeException
 *
 * @author Misbahul D Munir <misbahuldmunir@gmail.com>
 * @since 1.0
 */
class StateChangeException extends UserException
{
    
}
