<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\TbMurid;
use Faker\Factory;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\ArrayHelper;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SampleController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($count = 100)
    {

        $faker = Factory::create('id_ID');
        /** @var TbMurid[] $daftarMurid */
        $daftarMurid =[];
        for ($i = 0;$i< $count;$i++){
            $tbMurid = new TbMurid();
            $tbMurid->nim_murid = $faker->numerify('##########');
            $tbMurid->nama = $faker->name;
            $tbMurid->kode_kelas = $faker->randomElement(['A','B','C','E']);
            $tbMurid->tanggal_lahir = $faker->dateTimeBetween('-25 year','-15 year')->format('Y-m-d');
                $tbMurid->alamat = $faker->address;
                $tbMurid->no_telp = $faker->phoneNumber;
                $tbMurid->asal_sekolah = $faker->company;
                $tbMurid->tgl_masuk = $faker->dateTimeBetween('-12 month')->format('Y-m-d');
                $tbMurid->status = $faker->randomElement(['aktif','non_aktif']);
                $tbMurid->agama = $faker->randomElement(['islam','kristen','hindu','budha']);
//                'created_at' => $faker->dateTime(),
//                'updated_at' => $faker->dateTime()
            $tbMurid->save();
            $daftarMurid[] = $tbMurid;
            unset($tbMurid);
        }

        foreach ($daftarMurid as $murid) {

        }
    }
}
