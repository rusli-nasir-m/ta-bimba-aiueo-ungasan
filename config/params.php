<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'status_jensi_pengeluaran' => [
        '1' => 'aktif',
        '0' => 'non aktif',
        '2' => 'dikunci',
    ],
    'bulan' => [
        "1" => "Januari",
        "2" => "Februari",
        "3" => "Maret",
        "4" => "April",
        "5" => "Mei",
        "6" => "Juni",
        "7" => "Juli",
        "8" => "Augustus",
        "9" => "September",
        "10" => "Oktober",
        "11" => "November",
        "12" => "Desember",
    ],
    'user_access' => [
        10 => 'Kepala Akademik',
        20 => 'Pimpinan',
        30 => 'Financial Account',
        40 => 'Accounting',
        50 => 'Staf Keuangan',
    ],
    'sumber_jurnal' => [
//        'CSSKR' => 'penerimaaniurancalon',
        'CSWJB' => 'penerimaanjttcalon',
        'JTT' => 'penerimaanjtt',
        'LNN' => 'penerimaanlain',
//        'SKR' => 'penerimaaniuran',
        'OUT' => 'pengeluaran',
        'UMUM' => 'jurnalumum',
        'KAS' => 'permintaan_kas'
    ],
    'status_data' => [
        0 => 'Deleted',
        10 => 'Non Aktif',
        20 => 'Aktif',
    ],
    'menu_kepala_akademik' => [
        'master' => [
            'label' => 'Master',
            'id' => 'master_menu',
            'items' => [
                ['label' => 'Kelas', 'url' => ['/master/kelas'], 'id' => 'kelas_menu'],
            ],
        ],
        'murid'=> ['id' => 'murid_menu','label' => 'Data Murid', 'url' => ['/master/data-murid/index']],
        'userx'=> ['id' => 'user_menu','label' => 'User', 'url' => ['master/user/index']],
    ],
    'menu_pimpinan' => [
        'murid'=> ['id' => 'murid_menu','label' => 'Data Murid', 'url' => ['/master/data-murid/index']],
        'lap'=> ['id' => 'laporan_menu','label' => 'Laporan', 'url' => ['/laporan/index']],
        'userx'=>['id' => 'user_menu','label' => 'User', 'url' => ['master/user/index']],
    ],
    'menu_accounting' => [
        'akun'=> [
            'id' => 'akun_menu','label' => 'Akun',
            'items' => [
                ['id' => 'tahunbuku_menu','label' => 'Tahun Buku', 'url' => ['acc/tahunbuku/index']],
                ['id' => 'tambah_akun_menu','label' => 'Tambah Akun', 'url' => ['acc/akun/create']],
                ['id' => 'saldo_awal_menu','label' => 'Saldo Awal', 'url' => ['acc/akun/saldo-awal']],
                ['id' => 'daftar_akun_menu','label' => 'Daftar Akun', 'url' => ['acc/akun/index']],
                ['label' => 'Kategori Akun', 'url' => ['/master/kategori-akun/index'], 'id' => 'kat_akun_menu'],
                ['label' => 'Kelompok Akun', 'url' => ['/master/kelompok-akun/index'], 'id' => 'kel_akun_menu'],
                ['label' => 'Jenis Penerimaan', 'url' => ['/acc/jenis-penerimaan/index'], 'id' => 'jenis_penerimaan_menu'],
                ['label' => 'Jenis Pengeluaran', 'url' => ['/acc/jenis-pengeluaran/index'], 'id' => 'jenis_pengeluaran_menu'],
            ]
        ],
        'jurnal'=> ['id' => 'jurnal_menu','label' => 'Jurnal', 'url' => ['/acc/jurnal-umum/index']],
        'trans'=>[
            'label' => 'Transaksi',
            'id' => 'transaksi_menu',
            'items' => [
                'kaskecil' =>['id' => 'daftar_transaksi_pengajuan-kaskecil_menu','label' => 'Pengajuan Kas Kecil', 'url' => ['/acc/pengajuan-kaskecil']],
                'keluar' => ['id' => 'daftar_transaksi_pengeluaran_menu','label' => 'Transaksi Pengeluaran', 'url' => ['/pengeluaran']],
            ],
        ],
        'lap'=> ['id' => 'laporan_menu','label' => 'Laporan', 'url' => ['/laporan/index']],
    ],
    'menu_staf_keuangan' => [
        'trans' => [
            'label' => 'Transaksi',
            'id' => 'transaksi_menu',
            'items' => [
                'terima' => ['id' => 'daftar_transaksi_penerimaan_menu','label' => 'Transaksi Penerimaan', 'url' => ['/penerimaan']],
            ],
        ],
        'lap'=>['id' => 'laporan_menu','label' => 'Laporan', 'url' => ['/laporan/index']],
    ],
    'menu_finance' => [
        'trans'=> [
            'label' => 'Transaksi',
            'id' => 'transaksi_menu',
            'items' => [
                'kaskecil' =>['id' => 'daftar_transaksi_pengajuan-kaskecil_menu','label' => 'Pengajuan Kas Kecil', 'url' => ['/acc/pengajuan-kaskecil']],
                'keluar' =>['id' => 'daftar_transaksi_pengeluaran_menu','label' => 'Transaksi Pengeluaran', 'url' => ['/pengeluaran']],
            ],
        ],
        'lap'=> ['id' => 'laporan_menu','label' => 'Laporan', 'url' => ['/laporan/index']],
    ]
];
