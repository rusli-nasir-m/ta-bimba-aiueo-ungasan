<?php

namespace app\controllers;

use app\models\LabaRugi;

class LabaRugiController extends \yii\web\Controller
{
    public function actionIndex()
    {

        $model = new LabaRugi();
        $curDate = date('Y-m-d');
        $model->fromDate = $curDate;
        $model->toDate = $curDate;
        $dataProvider = $model->searchLabarugi(\Yii::$app->request->post());

        return $this->render('index',[
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPrint()
    {
        $model = new LabaRugi();
        $dataProvider = $model->searchLabarugi2(\Yii::$app->request->get());
        $this->layout = 'print';
        return $this->render('print',[
            'model'=> $model,
            'dataProvider'=> $dataProvider
        ]);
    }

}
