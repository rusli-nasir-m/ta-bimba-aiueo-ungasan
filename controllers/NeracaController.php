<?php

namespace app\controllers;

use app\models\Neraca;

class NeracaController extends \yii\web\Controller
{
    public function actionIndex()
    {

        $model = new Neraca();
        $curDate = date('Y-m-d');
        $model->fromDate = $curDate;
        $model->toDate = $curDate;
        $dataProvider = $model->searchNeraca(\Yii::$app->request->post());

        return $this->render('index',[
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPrint()
    {
        $model = new Neraca();
        $dataProvider = $model->searchNeraca2(\Yii::$app->request->get());
        $this->layout = 'print';
        return $this->render('print',[
            'model'=> $model,
            'dataProvider'=> $dataProvider
        ]);
    }

}
