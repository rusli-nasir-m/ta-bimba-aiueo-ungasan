<?php

namespace app\controllers;

use app\models\TbSpp;
use app\models\TransaksiPenerimaan;
use Yii;
use app\models\TbKasMasuk;
use app\models\search\TbKasMasuk as TbKasMasukSearch;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PenerimaanController implements the CRUD actions for TbKasMasuk model.
 */
class PenerimaanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TbKasMasuk models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TbKasMasukSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TbKasMasuk model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    /**
     * Creates a new TbKasMasuk model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param int $jp Jenis Permohonan
     * @return mixed
     */
    public function actionCreate($jp)
    {
        $model = new TbKasMasuk();
        $model->scenario = $model::SCENARIO_INSERT;
        $model->jenis_permohonan = $jp;
        switch ($jp){
            case 1:
                $model->sumber_penerimaan = 14;
                break;
            case 2:
                $model->sumber_penerimaan = 13;
                break;
            case 3:
                break;
        }
        $model->tanggal_penerimaan = date('Y-m-d');
        if ($model->load(Yii::$app->request->post())) {
            $validation = ActiveForm::validate($model);
            if($validation){
                Yii::$app->session->setFlash('error',$validation);
                goto skipData;
            }
            $transaction = $model::getDb()->beginTransaction();
            try {
                if($model->save()){
                    $transaction->commit();
                    Yii::$app->session->setFlash('success',Yii::t('app','Transaksi Penerimaan {key} Berhasil disimpan',['key' => $model->id_kas_masuk]));
                    return $this->redirect(['view', 'id' => $model->id_kas_masuk]);
                }
            }catch (\Exception $exception){
                $transaction->rollBack();
            }
        }
        
skipData:
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    public function actionListSppSiswa($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' =>TbSpp::find()->where(['nim_murid' => $id]),
            'pagination' => false,
        ]);
        return $this->renderAjax('list-spp',[
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionDetailPermohonan()
    {
        $id = Yii::$app->request->post('id');
        switch ($id){
            case 1:
                $model = new TbKasMasuk();
                return $this->renderAjax('pembayaran-spp',[
                    'model' => $model,
                ]);
                break;
        }
    }

    /**
     * Updates an existing TbKasMasuk model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        switch ($model->jenis_permohonan){
            case 1:
                $model->cariSiswa = $model->nim_murid?$model->nim_murid . ' | ' . $model->siswa->nama:null;
                break;
            case 2:
                $model->cariSiswa = $model->nim_murid?$model->nim_murid . ' | ' . $model->siswa->nama:null;
                break;
            case 3:
                break;
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_kas_masuk]);
        }
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TbKasMasuk model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TbKasMasuk model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TbKasMasuk the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TbKasMasuk::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
