<?php

namespace app\controllers;

use app\models\Akun;
use app\models\Datapengeluaran;
use app\models\search\JenisPengeluaran;
use Yii;
use app\models\TbPengeluaran;
use app\models\search\TbPengeluaran as TbPengeluaranSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * PengeluaranController implements the CRUD actions for TbPengeluaran model.
 */
class PengeluaranController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TbPengeluaran models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TbPengeluaranSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TbPengeluaran model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TbPengeluaran model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TbPengeluaran();
        $model->scenario = $model::SCENARIO_INSERT;
        $model->tanggal_realisasi = date('Y-m-d');
        $model->id_user = getMyId();
        if ($model->load(Yii::$app->request->post())){
            $jurnal = $model->generateJurnal();
            if($jurnal->save()){
                $model->id_jurnal = $jurnal->id;
                $model->jenis_pengeluaran = $model->jenisPermohonan->nama;
                if ($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id_pengeluaran]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TbPengeluaran model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_pengeluaran]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TbPengeluaran model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionJenisPengeluaran($id)
    {
        $model = Datapengeluaran::findOne(['replid' => $id]);
        if($model){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'ketrangan' => $model->keterangan,
                'rekdebit' => Akun::getIDByKode($model->rekdebet),
                'rekkredit' => Akun::getIDByKode($model->rekkredit),
            ];
        }
    }

    /**
     * Finds the TbPengeluaran model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TbPengeluaran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TbPengeluaran::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
