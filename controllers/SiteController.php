<?php

namespace app\controllers;

use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
//                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['login','captcha','error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index','logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        setDotEnvVar('TEST_ENV','contoh nilai2');
        $role = Yii::$app->user->identity->role;
        switch ($role){
            case User::ROLE_SUPER_USER:
                return $this->render('index-super-user');
                break;
            case User::ROLE_KEPALA_AKADEMIK:
                return $this->render('index-kepala-akademik');
                break;
            case User::ROLE_PIMPINAN:
                return $this->render('index-pimpinan');
                break;
            case User::ROLE_FINANCIAL_ACCOUNT:
                return $this->render('index-fin-acc');
                break;
            case User::ROLE_ACCOUNTING:
                return $this->render('index-accounting');
                break;
            case User::ROLE_STAF_KEUANGAN:
                return $this->render('index-keuangan');
                break;

        }

        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return array|string|Response
     */
    public function actionLogin()
    {
        $model = new LoginForm();

        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (!Yii::$app->user->isGuest) {
                return [
                    'success' => 0,
                    'status' => 'error',
                    'message' => 'Anda sudah login...!'
                ];
            }

            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return [
                    'success' => 1,
                    'status' => 'success',
                    'message' => 'Login berhasil...!'
                ];
            }
            return [
                'success' => 0,
                'status' => 'error',
                'message' => implode(',',$model->firstErrors)
            ];
        }

        $this->layout = 'login.php';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
