<?php

namespace app\controllers\acc;

use Yii;
use app\models\Akun;
use app\models\search\Akun as AkunSearch;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AkunController implements the CRUD actions for Akun model.
 */
class AkunController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Akun models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AkunSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSaldoAwal()
    {
        $request = Yii::$app->request;
        $query = Akun::find()->joinWith('kategoriAkun')
            ->where(['not in','kelompok_akun_id',[4,5]])
            ->orderBy(['kelompok_akun_id'=>SORT_ASC]);
        if($request->isPost){
            $akunPostList = $request->post('Akun');
            $listAkun  = $query->all();
            $transaction = Yii::$app->db->beginTransaction();

            try {
                foreach ($listAkun as $akun) {
                    $data = $akunPostList[$akun->id];
                    if(isset($data['debit']) && $data['debit'])
                    {
                        $akun->saldo_awal = $data['debit'];
                    }
                    elseif(isset($data['kredit']) && $data['kredit'])
                    {
                        $akun->saldo_awal = -($data['kredit']);
                    }
                    else
                    {
                        $akun->saldo_awal = 0;
                    }
                    $akun->update(false);
                }
                $transaction->commit();
                Yii::$app->session->setFlash('success','Perbaharui saldo awal berhasil');
            }catch (Exception $exception){
                $transaction->rollBack();
                Yii::$app->session->setFlash('error',$exception->getMessage());
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => false
        ]);

        return $this->render('saldo_awal',['dataProvider' =>$dataProvider]);
    }

    /**
     * Displays a single Akun model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Akun model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Akun();
        $model->setScenario($model::SCENARIO_CREATE);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Akun model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->setScenario($model::SCENARIO_UPDATE);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Akun model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Akun model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Akun the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Akun::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
