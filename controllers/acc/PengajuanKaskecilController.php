<?php

namespace app\controllers\acc;

use app\models\Datapengeluaran;
use app\models\Jurnal;
use app\models\search\JenisPengeluaran;
use app\models\User;
use Yii;
use app\models\TbPengajuan;
use app\models\search\TbPengajuan as TbPengajuanSearch;
use yii\base\UserException;
use yii\bootstrap\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PengajuanKaskecilController implements the CRUD actions for TbPengajuan model.
 */
class PengajuanKaskecilController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'validasi' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TbPengajuan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TbPengajuanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TbPengajuan model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TbPengajuan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TbPengajuan();
        $model->scenario = $model::SCENARIO_INSERT;
        $model->jenis_pengajuan = 'kaskecil';
        $model->pengajuan_oleh = getMyId();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_pengajuan]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TbPengajuan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if($model->validasi_pengejuan === 1){
            throw new UserException('Maaf Pengajuan yang sudah divalidasi tidak dapat diubah.',500);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_pengajuan]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionValidasi($id)
    {
           $role = getMyRole();
           if(!in_array($role,[User::ROLE_FINANCIAL_ACCOUNT,User::ROLE_SUPER_USER])){
               throw new UserException('Maaf anda tidak memiliki akses');
           }



           $model = $this->findModel($id);
           $model->scenario = $model::SCENARIO_VALIDASI;
           $jenisPengeluaran = Datapengeluaran::find()->where(['nama' => $model->jenis_pengajuan,'aktif' => 2])->one();
           $model->rekDebit = $jenisPengeluaran->rekdebet;
           $model->rekKredit = $jenisPengeluaran->rekkredit;
           $request = Yii::$app->request;
           if($request->isPost){
               $transaction = Yii::$app->db->beginTransaction();
               try {
                   $model->validasi_tanggal = date('Y-m-d');
                   $model->validasi_oleh = getMyId();
                   $model->validasi_pengejuan = 1;

                   $jurnal = $model->generateJurnal();
                   if($jurnal instanceof Jurnal){
                       if ($jurnal->save()){
                           $model->id_jurnal = $jurnal->id;
                           if($model->save()){
                               Yii::$app->session->setFlash('success','Validasi berhasil');
                               $transaction->commit();
                               return $this->redirect(['view', 'id' => $model->id_pengajuan]);
                           }
                       }
                   }else{
                       $transaction->rollBack();
                       Yii::$app->session->setFlash('error','Tahun buku belum ditentukan');
                   }
               }catch (\Exception $exception){
                   $transaction->rollBack();
                   Yii::$app->session->setFlash('error',$exception->getMessage());
               }
           }
           return $this->render('validasi',['model' => $model]);
    }

    /**
     * Deletes an existing TbPengajuan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TbPengajuan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TbPengajuan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TbPengajuan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
