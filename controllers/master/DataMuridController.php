<?php

namespace app\controllers\master;

use app\models\TbSpp;
use Yii;
use app\models\TbMurid;
use app\models\search\TbMurid as TbMuridSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * DataMuridController implements the CRUD actions for TbMurid model.
 */
class DataMuridController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TbMurid models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TbMuridSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TbMurid model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TbMurid model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TbMurid();
        $model->setScenario('create');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->nim_murid]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TbMurid model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->setScenario('update');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->nim_murid]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TbMurid model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = $model::STATUS_NON_AKTIF;
        $model->update();
        return $this->redirect(['index']);
    }

    public function actionDaftarTagihanSpp($id)
    {
        $model = $this->findModel($id);
        $dataProvider = [];
        if($model){
            $listSpp = TbSpp::find()->where(['nim_murid' => $model->nim_murid,'id_pembayaran' => null]);
            $dataProvider = new ActiveDataProvider([
                'query' => $listSpp,
                'pagination' => false,
            ]);
        }
        return $this->render('daftar-tagihan-spp',[
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Finds the TbMurid model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TbMurid the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TbMurid::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionListSppMurid()
    {
        $request = Yii::$app->request;
        $term = $request->get('term');
        $siswa = TbMurid::find()->where(['nim_murid' => $term])
            ->orWhere(['like','nama',$term])->all();
        $listSiswa = [];
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($siswa){
            foreach ($siswa as $murid) {
                $listSiswa[$murid->nim_murid]= [
                    'nim_murid' => $murid->nim_murid,
                    'nama' => $murid->nama,
                    'kelas' => $murid->kelas->nama_kelas,
                ];
            }
        }

        $spp = TbSpp::find()
            ->innerJoin(['s' => TbMurid::tableName()],'s.nim_murid = tb_spp.nim_murid')
            ->where(['lunas' => 0])
            ->andFilterWhere(['like','nama',$term])
            ->all();

        if ($spp){
            foreach ($spp as $item) {
                /** @var $item TbSpp */
                if(isset($listSiswa[$item->nim_murid])){
                    $listSiswa[$item->nim_murid]['spp'][]= $item;
                }
            }
        }

        return $listSiswa;
    }
}
