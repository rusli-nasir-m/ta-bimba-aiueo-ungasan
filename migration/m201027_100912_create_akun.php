<?php

use yii\db\Schema;
use yii\db\Migration;

class m201027_100912_create_akun extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
//            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%kelompok_akun}}',[
            'id'=> $this->primaryKey()->unsigned(),
            'nama'=> $this->char(10)->notNull()->defaultValue(''),
        ], $tableOptions);

        $this->createTable('{{%kategori_akun}}',
            [
                'id_kategori'=> $this->primaryKey(11),
                'kategori_akun'=> $this->string(255)->null()->defaultValue(null),
                'kelompok_akun_id'=> $this->integer()->null()->defaultValue(null),
            ],$tableOptions
        );


        $this->createTable(
            '{{%tb_akun}}',
            [
                'id'=> $this->primaryKey()->unsigned(),
                'nama'=> $this->string(100)->notNull()->defaultValue(''),
                'kode'=> $this->string(20)->notNull()->defaultValue(''),
                'kategori_akun_id'=> $this->integer()->notNull(),
                'pajak'=> $this->tinyInteger(1)->notNull()->defaultValue(1),
                'saldo_awal'=> $this->decimal(15,2)->notNull()->defaultValue(0),
                'saldo'=> $this->decimal(15,2)->notNull()->defaultValue(0),
                'keterangan'=> $this->text()->notNull(),
            ],$tableOptions
        );

        $this->addForeignKey('fk_tb_akun_kategori_akun_id',
            '{{%tb_akun}}','kategori_akun_id',
            '{{%kategori_akun}}','id_kategori',
            'CASCADE','CASCADE'
        );

        $this->createIndex('idx_kategori_akun_id','{{%tb_akun}}',['kategori_akun_id'],false);
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_tb_akun_kategori_akun_id', '{{%tb_akun}}');
        $this->dropIndex('idx_kategori_akun_id', '{{%tb_akun}}');
//            $this->dropForeignKey('fk_tb_akun_kelompok_akun_id', '{{%tb_akun}}');
        $this->dropTable('{{%tb_akun}}');
        $this->dropTable('{{%kategori_akun}}');
        $this->dropTable('{{%kelompok_akun}}');
    }
}
