<?php

use yii\db\Schema;
use yii\db\Migration;

class m201027_100914_create_table_jurnal extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
//            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%fungsi_jurnal}}',[
            'id'=> $this->primaryKey(3)->unsigned(),
            'nama'=> $this->char(20)->notNull()->defaultValue(''),
        ], $tableOptions);

        $this->batchInsert('{{%fungsi_jurnal}}',['id','nama'],[
            ['id' => 1, 'nama' => 'Jurnal Umum'],
            ['id' => 2, 'nama' => 'Jurnal Penyesuaian'],
            ['id' => 3, 'nama' => 'Jurnal Penutup'],
        ]);

        $this->createTable('{{%jurnal}}',[
            'id'=> $this->primaryKey(10)->unsigned(),
            'no'=> $this->string(20)->notNull()->defaultValue(''),
            'tgl'=> $this->date()->notNull(),
            'f_id'=> $this->integer(3)->unsigned()->notNull()->defaultValue(0),
            'keterangan'=> $this->text()->notNull(),
            'login_id'=> $this->integer(10)->unsigned()->notNull()->defaultValue(0),
            'waktu_post'=> $this->datetime()->notNull(),
            'tahunbuku_id'=> $this->integer(11)->null()->defaultValue(null),
            'total'=> $this->decimal(15, 4)->null()->defaultValue(null),
            'sumberjurnal'=> $this->string(20)->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('f_id','{{%jurnal}}',['f_id'],false);

        $this->createTable('{{%jurnal_detail}}',[
            'id'=> $this->primaryKey(10)->unsigned(),
            'jurnal_id'=> $this->integer(10)->unsigned()->notNull()->defaultValue(0),
            'item'=> $this->tinyInteger(3)->notNull()->defaultValue(0),
            'akun_id'=> $this->integer(5)->unsigned()->notNull()->defaultValue(0),
            'debit'=> $this->decimal(15, 2)->notNull()->defaultValue('0.00'),
            'kredit'=> $this->decimal(15, 2)->notNull()->defaultValue('0.00'),
        ], $tableOptions);

        $this->createIndex('jurnal_id','{{%jurnal_detail}}',['jurnal_id'],false);
        $this->createIndex('akun_id','{{%jurnal_detail}}',['akun_id'],false);
        $this->addForeignKey(
            'fk_jurnal_detail_akun_id',
            '{{%jurnal_detail}}', 'akun_id',
            '{{%tb_akun}}', 'id',
            'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'fk_jurnal_detail_jurnal_id',
            '{{%jurnal_detail}}', 'jurnal_id',
            '{{%jurnal}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_jurnal_detail_jurnal_id', '{{%jurnal_detail}}');
        $this->dropForeignKey('fk_jurnal_detail_akun_id', '{{%jurnal_detail}}');
        $this->dropTable('{{%fungsi_jurnal}}');
        $this->dropTable('{{%jurnal}}');
        $this->dropTable('{{%jurnal_detail}}');
    }
}
