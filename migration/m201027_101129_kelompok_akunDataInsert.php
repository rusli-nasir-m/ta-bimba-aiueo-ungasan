<?php

use yii\db\Schema;
use yii\db\Migration;

class m201027_101129_kelompok_akunDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('{{%kelompok_akun}}',
            ["id", "nama"],
            [
                [
                    'id' => '1',
                    'nama' => 'Aktiva',
                ],
                [
                    'id' => '2',
                    'nama' => 'Kewajiban',
                ],
                [
                    'id' => '3',
                    'nama' => 'Modal',
                ],
                [
                    'id' => '4',
                    'nama' => 'Pendapatan',
                ],
                [
                    'id' => '5',
                    'nama' => 'Biaya',
                ],
            ]
        );

        $this->batchInsert('{{%kategori_akun}}',
            ["id_kategori", "kategori_akun", "kelompok_akun_id"],
            [
                [
                    'id_kategori' => '1',
                    'kategori_akun' => ' Kas',
                    'kelompok_akun_id' => '1',
                ],
                [
                    'id_kategori' => '2',
                    'kategori_akun' => 'Aktiva Lancar',
                    'kelompok_akun_id' => '1',
                ],
                [
                    'id_kategori' => '3',
                    'kategori_akun' => 'Hutang',
                    'kelompok_akun_id' => '2',
                ],
                [
                    'id_kategori' => '4',
                    'kategori_akun' => 'Piutang',
                    'kelompok_akun_id' => '3',
                ],
                [
                    'id_kategori' => '5',
                    'kategori_akun' => 'Ativa Tetap',
                    'kelompok_akun_id' => '1',
                ],
                [
                    'id_kategori' => '6',
                    'kategori_akun' => 'Beban',
                    'kelompok_akun_id' => '5',
                ],
                [
                    'id_kategori' => '7',
                    'kategori_akun' => 'Beban Lainnya',
                    'kelompok_akun_id' => '5',
                ],
                [
                    'id_kategori' => '8',
                    'kategori_akun' => 'Depresiasi & Amortasi',
                    'kelompok_akun_id' => '5',
                ],
                [
                    'id_kategori' => '9',
                    'kategori_akun' => 'ekuitas',
                    'kelompok_akun_id' => '3',
                ],
                [
                    'id_kategori' => '10',
                    'kategori_akun' => 'Kewajiban Lancar lainya',
                    'kelompok_akun_id' => '2',
                ],
                [
                    'id_kategori' => '11',
                    'kategori_akun' => 'Pendapatan',
                    'kelompok_akun_id' => '4',
                ],
                [
                    'id_kategori' => '12',
                    'kategori_akun' => 'Pendapatan lain lain',
                    'kelompok_akun_id' => '4',
                ],
            ]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('{{%kelompok_akun}} CASCADE');
        //$this->truncateTable('{{%kategori_akun}} CASCADE');
    }
}
