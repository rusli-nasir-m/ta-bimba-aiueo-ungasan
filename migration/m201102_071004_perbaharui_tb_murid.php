<?php

use yii\db\Migration;

/**
 * Class m201102_071004_perbaharui_tb_murid
 */
class m201102_071004_perbaharui_tb_murid extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tb_murid}}','status', $this->string(20));
        $this->addColumn('{{%tb_murid}}','agama', $this->string(20));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201102_071004_perbaharui_tb_murid cannot be reverted.\n";
        $this->dropColumn('{{%tb_murid}}','status');
        $this->dropColumn('{{%tb_murid}}','agama');
//        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201102_071004_perbaharui_tb_murid cannot be reverted.\n";

        return false;
    }
    */
}
