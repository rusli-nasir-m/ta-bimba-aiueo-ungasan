<?php

use yii\db\Schema;
use yii\db\Migration;

class m201109_085218_tb_kelas extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
//            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%tb_kelas}}',
            [
                'kode_kelas'=> $this->string(10)->notNull(),
                'nama_kelas'=> $this->string(255)->null()->defaultValue(null),
            ],$tableOptions
        );
        $this->addPrimaryKey('pk_on_tb_kelas','{{%tb_kelas}}',['kode_kelas']);


        $this->alterColumn('{{%tb_murid}}','kode_kelas', $this->string(10));
        $this->addForeignKey(
            'fk_murid_kelas',
            '{{%tb_murid}}', 'kode_kelas',
            '{{%tb_kelas}}', 'kode_kelas',
            'CASCADE', 'CASCADE'
        );

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_murid_kelas', '{{%tb_murid}}');
        $this->alterColumn('{{%tb_murid}}','kode_kelas', $this->string());
        $this->dropPrimaryKey('pk_on_tb_kelas','{{%tb_kelas}}');
        $this->dropTable('{{%tb_kelas}}');
    }
}
