<?php

use yii\db\Schema;
use yii\db\Migration;

class m201112_135900_tahunbuku extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
//            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%tahunbuku}}',
            [
                'id'=> $this->primaryKey()->unsigned(),
                'tahunbuku'=> $this->string(100)->notNull(),
                'awalan'=> $this->string(5)->notNull(),
                'aktif'=> $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1),
                'keterangan'=> $this->string(255)->null()->defaultValue(null),
                'cacah'=> $this->bigInteger(20)->unsigned()->notNull()->defaultValue('0'),
                'tanggalmulai'=> $this->date()->notNull(),
                'tanggalselesai'=> $this->date()->null()->defaultValue(null),
            ],$tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%tahunbuku}}');
    }
}
