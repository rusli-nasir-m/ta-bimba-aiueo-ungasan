<?php

use yii\db\Migration;

/**
 * Class m201124_154223_update_kelas
 */
class m201124_154223_update_kelas extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tb_kelas}}','jumlah_pertemuan', $this->integer()->comment('Jumlah Pertemuan Dalam 1 Bulan'));
        $this->addColumn('{{%tb_kelas}}','durasi', $this->integer()->comment('Lama Durasi Belajar'));
        $this->addColumn('{{%tb_kelas}}','harga', $this->integer()->comment('Harga'));

        $this->batchInsert('{{%tb_kelas}}',
            ["kode_kelas", "nama_kelas", "jumlah_pertemuan", "durasi", "harga"],
            [
                [
                    'kode_kelas' => 'A',
                    'nama_kelas' => 'Kelas A',
                    'jumlah_pertemuan' => '12',
                    'durasi' => '60',
                    'harga' => '350000',
                ],
                [
                    'kode_kelas' => 'B',
                    'nama_kelas' => 'Kelas B',
                    'jumlah_pertemuan' => '10',
                    'durasi' => '60',
                    'harga' => '300000',
                ],
                [
                    'kode_kelas' => 'C',
                    'nama_kelas' => 'Kelas C',
                    'jumlah_pertemuan' => '16',
                    'durasi' => '60',
                    'harga' => '400000',
                ],
                [
                    'kode_kelas' => 'E',
                    'nama_kelas' => 'Kelas E',
                    'jumlah_pertemuan' => '24',
                    'durasi' => '60',
                    'harga' => '550000',
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201124_154223_update_kelas cannot be reverted.\n";
        $this->truncateTable('{{%tb_kelas}} CASCADE');
        $this->dropColumn('{{%tb_murid}}','jumlah_pertemuan');
        $this->dropColumn('{{%tb_murid}}','durasi');
        $this->dropColumn('{{%tb_murid}}','harga');
//        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201124_154223_update_kelas cannot be reverted.\n";

        return false;
    }
    */
}
