<?php

use yii\db\Schema;
use yii\db\Migration;

class m201204_025930_spp_Mass extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
//            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tb_spp}}',[
            'id_spp'=> $this->string(255)->notNull(),
            'nim_murid'=> $this->string(100)->null()->defaultValue(null),
            'tahun'=> $this->integer(11)->null()->defaultValue(null),
            'bulan'=> $this->integer(11)->null()->defaultValue(null),
            'jumlah'=> $this->decimal(15, 2)->null()->defaultValue(null),
            'id_pembayaran'=> $this->integer(11)->null()->defaultValue(null),
            'id_user'=> $this->integer(11)->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('tb_spp_ibfk_1','{{%tb_spp}}',['id_pembayaran'],false);
        $this->addPrimaryKey('pk_on_tb_spp','{{%tb_spp}}',['id_spp']);

        $this->createTable('{{%tb_bayar_spp}}',[
            'id_pembayaran'=> $this->primaryKey(11),
            'no_kwitansi'=> $this->string(20)->null()->defaultValue(null),
            'nim_murid'=> $this->string(100)->null()->defaultValue(null),
            'tanggal_bayar'=> $this->date()->null()->defaultValue(null),
            'keterangan'=> $this->string(255)->null()->defaultValue(null),
        ], $tableOptions);

        $this->addForeignKey(
            'fk_tb_spp_id_pembayaran',
            '{{%tb_spp}}', 'id_pembayaran',
            '{{%tb_bayar_spp}}', 'id_pembayaran',
            'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
            $this->dropForeignKey('fk_tb_spp_id_pembayaran', '{{%tb_spp}}');
            $this->dropPrimaryKey('pk_on_tb_spp','{{%tb_spp}}');
            $this->dropTable('{{%tb_spp}}');
            $this->dropTable('{{%tb_bayar_spp}}');
    }
}
