<?php

use yii\db\Schema;
use yii\db\Migration;

class m201204_164113_datapengeluaran extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
//            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%datapengeluaran}}',[
            'replid'=> $this->primaryKey(10)->unsigned(),
            'nama'=> $this->string(100)->notNull(),
            'rekdebet'=> $this->string(20)->notNull(),
            'rekkredit'=> $this->string(20)->notNull(),
            'aktif'=> $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'keterangan'=> $this->string(255)->null()->defaultValue(null),
            'besar'=> $this->decimal(15)->notNull()->defaultValue('0'),
            'info1'=> $this->string(255)->null()->defaultValue(null),
            'info2'=> $this->string(255)->null()->defaultValue(null),
            'info3'=> $this->string(255)->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('FK_datapengeluaran_rekakun','{{%datapengeluaran}}',['rekdebet'],false);
        $this->createIndex('FK_datapengeluaran_rekakun2','{{%datapengeluaran}}',['rekkredit'],false);
//        $this->addForeignKey(
//            'fk_datapengeluaran_rekdebet',
//            '{{%datapengeluaran}}', 'rekdebet',
//            '{{%tb_akun}}', 'kode',
//            'CASCADE', 'CASCADE'
//        );
//        $this->addForeignKey(
//            'fk_datapengeluaran_rekkredit',
//            '{{%datapengeluaran}}', 'rekkredit',
//            '{{%tb_akun}}', 'kode',
//            'CASCADE', 'CASCADE'
//        );
    }

    public function safeDown()
    {
//            $this->dropForeignKey('fk_datapengeluaran_rekdebet', '{{%datapengeluaran}}');
//            $this->dropForeignKey('fk_datapengeluaran_rekkredit', '{{%datapengeluaran}}');
            $this->dropTable('{{%datapengeluaran}}');
    }
}
