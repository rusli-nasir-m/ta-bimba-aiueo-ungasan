<?php

use yii\db\Schema;
use yii\db\Migration;

class m201204_164114_kategori_penerimaan_Mass extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
//            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%kategoripenerimaan}}',[
            'replid'=> $this->integer(10)->unsigned()->notNull(),
            'kode'=> $this->string(10)->notNull(),
            'kategori'=> $this->string(100)->notNull(),
            'urutan'=> $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'siswa'=> $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1),
            'info1'=> $this->string(255)->null()->defaultValue(null),
            'info2'=> $this->string(255)->null()->defaultValue(null),
            'info3'=> $this->string(255)->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('Index_1','{{%kategoripenerimaan}}',['replid'],true);
        $this->addPrimaryKey('pk_on_kategoripenerimaan','{{%kategoripenerimaan}}',['kode']);

        $this->createTable('{{%datapenerimaan}}',[
            'replid'=> $this->primaryKey(10)->unsigned(),
            'nama'=> $this->string(100)->notNull(),
            'besar'=> $this->decimal(15)->null()->defaultValue(null),
            'idkategori'=> $this->string(15)->notNull(),
            'rekkas'=> $this->string(20)->notNull(),
            'rekpendapatan'=> $this->string(20)->notNull(),
            'rekpiutang'=> $this->string(20)->null()->defaultValue(null),
            'aktif'=> $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1),
            'keterangan'=> $this->string(255)->null()->defaultValue(null),
            'departemen'=> $this->string(50)->null()->defaultValue(null),
            'info1'=> $this->string(255)->null()->defaultValue(null),
            'info2'=> $this->string(255)->null()->defaultValue(null),
            'info3'=> $this->string(255)->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('FK_datapenerimaan_rekakun_kas','{{%datapenerimaan}}',['rekkas'],false);
        $this->createIndex('FK_datapenerimaan_rekakun_pendapatan','{{%datapenerimaan}}',['rekpendapatan'],false);
        $this->createIndex('FK_datapenerimaan_rekakun_piutang','{{%datapenerimaan}}',['rekpiutang'],false);
        $this->createIndex('FK_datapenerimaan_kategoripenerimaan','{{%datapenerimaan}}',['idkategori'],false);
        $this->createIndex('FK_datapenerimaan_departemen','{{%datapenerimaan}}',['departemen'],false);
//        $this->addForeignKey(
//            'fk_datapenerimaan_rekkas',
//            '{{%datapenerimaan}}', 'rekkas',
//            '{{%tb_akun}}', 'kode',
//            'CASCADE', 'CASCADE'
//        );
//        $this->addForeignKey(
//            'fk_datapenerimaan_rekpendapatan',
//            '{{%datapenerimaan}}', 'rekpendapatan',
//            '{{%tb_akun}}', 'kode',
//            'CASCADE', 'CASCADE'
//        );
//        $this->addForeignKey(
//            'fk_datapenerimaan_rekpiutang',
//            '{{%datapenerimaan}}', 'rekpiutang',
//            '{{%tb_akun}}', 'kode',
//            'CASCADE', 'CASCADE'
//        );
//        $this->addForeignKey(
//            'fk_datapenerimaan_idkategori',
//            '{{%datapenerimaan}}', 'idkategori',
//            '{{%kategoripenerimaan}}', 'kode',
//            'CASCADE', 'CASCADE'
//        );
    }

    public function safeDown()
    {
//            $this->dropForeignKey('fk_datapenerimaan_rekkas', '{{%datapenerimaan}}');
//            $this->dropForeignKey('fk_datapenerimaan_rekpendapatan', '{{%datapenerimaan}}');
//            $this->dropForeignKey('fk_datapenerimaan_rekpiutang', '{{%datapenerimaan}}');
//            $this->dropForeignKey('fk_datapenerimaan_idkategori', '{{%datapenerimaan}}');
//            $this->dropPrimaryKey('pk_on_kategoripenerimaan','{{%kategoripenerimaan}}');
            $this->dropTable('{{%kategoripenerimaan}}');
            $this->dropTable('{{%datapenerimaan}}');
    }
}
