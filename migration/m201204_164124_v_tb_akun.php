<?php

use yii\db\Schema;
use yii\db\Migration;

class m201204_164124_v_tb_akun extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';
        $this->db->createCommand()->createView('v_tb_akun',"
        SELECT
	a.*, 
	b.kategori_akun AS nama_kategori_akun, 
	b.kelompok_akun_id, 
	c.nama AS nama_kelompok_akun
FROM
	tb_akun AS a
	INNER JOIN
	kategori_akun AS b
	ON 
		a.kategori_akun_id = b.id_kategori
	INNER JOIN
	kelompok_akun AS c
	ON 
		b.kelompok_akun_id = c.id
ORDER BY
	b.kelompok_akun_id ASC, 
	a.kategori_akun_id ASC 
        ")->execute();
    }

    public function safeDown()
    {
        $this->db->createCommand()->dropView('v_tb_akun')->execute();
    }
}
