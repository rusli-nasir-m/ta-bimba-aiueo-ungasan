<?php

use yii\db\Schema;
use yii\db\Migration;

class m201204_171909_kategoripenerimaanDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('{{%kategoripenerimaan}}',
                           ["replid", "kode", "kategori", "urutan", "siswa", "info1", "info2", "info3"],
                            [
//    [
//        'replid' => '4',
//        'kode' => 'CSSKR',
//        'kategori' => 'Iuran Sukarela Calon Siswa',
//        'urutan' => '4',
//        'siswa' => '1',
//        'info1' => null,
//        'info2' => null,
//        'info3' => null,
//    ],
    [
        'replid' => '3',
        'kode' => 'CSWJB',
        'kategori' => 'Iuran Wajib Calon Siswa',
        'urutan' => '3',
        'siswa' => '1',
        'info1' => null,
        'info2' => null,
        'info3' => null,
    ],
    [
        'replid' => '1',
        'kode' => 'JTT',
        'kategori' => 'Iuran Wajib Siswa',
        'urutan' => '1',
        'siswa' => '1',
        'info1' => null,
        'info2' => null,
        'info3' => null,
    ],
    [
        'replid' => '5',
        'kode' => 'LNN',
        'kategori' => 'Penerimaan Lainnya',
        'urutan' => '5',
        'siswa' => '1',
        'info1' => null,
        'info2' => null,
        'info3' => null,
    ],
//    [
//        'replid' => '2',
//        'kode' => 'SKR',
//        'kategori' => 'Iuran Sukarela Siswa',
//        'urutan' => '2',
//        'siswa' => '1',
//        'info1' => null,
//        'info2' => null,
//        'info3' => null,
//    ],
]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('{{%kategoripenerimaan}} CASCADE');
    }
}
