<?php

use yii\db\Schema;
use yii\db\Migration;

class m201204_171935_datapenerimaanDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('{{%datapenerimaan}}',
                           ["replid", "nama", "besar", "idkategori", "rekkas", "rekpendapatan", "rekpiutang", "aktif", "keterangan", "departemen", "info1", "info2", "info3"],
                            [
    [
        'replid' => '9',
        'nama' => 'SPP Bulanan',
        'besar' => null,
        'idkategori' => 'JTT',
        'rekkas' => '1-10001',
        'rekpendapatan' => '4-40000',
        'rekpiutang' => '1-10100',
        'aktif' => '1',
        'keterangan' => null,
        'departemen' => 'SMA',
        'info1' => '7-70099',
        'info2' => null,
        'info3' => null,
    ],
    [
        'replid' => '11',
        'nama' => 'Pendaftaran',
        'besar' => null,
        'idkategori' => 'CSWJB',
        'rekkas' => '1-10001',
        'rekpendapatan' => '7-70099',
        'rekpiutang' => '1-10100',
        'aktif' => '1',
        'keterangan' => null,
        'departemen' => 'SMA',
        'info1' => '7-70099',
        'info2' => null,
        'info3' => null,
    ],
]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('{{%datapenerimaan}} CASCADE');
    }
}
