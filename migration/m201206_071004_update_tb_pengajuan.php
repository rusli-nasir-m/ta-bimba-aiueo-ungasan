<?php

use yii\db\Migration;

/**
 * Class m201102_071004_perbaharui_tb_murid
 */
class m201206_071004_update_tb_pengajuan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('{{%tb_pengajuan}}','id_jurnal', $this->integer(10));
        $this->addColumn('{{%tb_pengajuan}}','total_pengajuan', $this->decimal(15,2));
        $this->addColumn('{{%tb_pengajuan}}','validasi_pengejuan', $this->integer());
        $this->addColumn('{{%tb_pengajuan}}','validasi_tanggal', $this->date());
        $this->addColumn('{{%tb_pengajuan}}','validasi_oleh', $this->integer());

//        $this->addForeignKey('fk_tb_pengajuan-id_jurnal',
//            '{{%tb_pengajuan}}','id_jurnal',
//            '{{%jurnal}}','id',
//            'RESTRICT','CASCADE'
//        );

        $this->createIndex('idx_tb_pengajuan-id_jurnal','{{%tb_pengajuan}}',['id_jurnal'],false);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
//        $this->dropForeignKey('fk_tb_pengajuan-id_jurnal', '{{%tb_pengajuan}}');
        $this->dropIndex('idx_tb_pengajuan-id_jurnal', '{{%tb_pengajuan}}');
        $this->dropColumn('{{%tb_pengajuan}}','id_jurnal');
        $this->dropColumn('{{%tb_pengajuan}}','total_pengajuan');
        $this->dropColumn('{{%tb_pengajuan}}','validasi_pengejuan');
        $this->dropColumn('{{%tb_pengajuan}}','validasi_tanggal');
        $this->dropColumn('{{%tb_pengajuan}}','validasi_oleh');
//        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201102_071004_perbaharui_tb_murid cannot be reverted.\n";

        return false;
    }
    */
}
