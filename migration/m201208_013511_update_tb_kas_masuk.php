<?php

use yii\db\Migration;

/**
 * Class m201208_013511_update_tb_kas_masuk
 */
class m201208_013511_update_tb_kas_masuk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tb_kas_masuk}}','id_jurnal', $this->integer(10));
        $this->addColumn('{{%tb_kas_masuk}}','sumber_penerimaan', $this->string(20));
        $this->addColumn('{{%tb_kas_masuk}}','nim_murid', $this->string(100));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201208_013511_update_tb_kas_masuk cannot be reverted.\n";
        $this->dropColumn('{{%tb_kas_masuk}}','id_jurnal');
        $this->dropColumn('{{%tb_kas_masuk}}','sumber_penerimaan');
        $this->dropColumn('{{%tb_kas_masuk}}','nim_murid');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201208_013511_update_tb_kas_masuk cannot be reverted.\n";

        return false;
    }
    */
}
