<?php

use yii\db\Migration;

/**
 * Class m201213_014742_update_tb_bayar_spp
 */
class m201213_014742_update_tb_bayar_spp extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tb_bayar_spp}}','cara_bayar', $this->string(10));
        $this->addColumn('{{%tb_bayar_spp}}','bank', $this->string(20));
        $this->addColumn('{{%tb_bayar_spp}}','reff_no', $this->string(50));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201213_014742_update_tb_bayar_spp cannot be reverted.\n";
        $this->dropColumn('{{%tb_bayar_spp}}','cara_bayar');
        $this->dropColumn('{{%tb_bayar_spp}}','bank');
        $this->dropColumn('{{%tb_bayar_spp}}','reff_no');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201213_014742_update_tb_bayar_spp cannot be reverted.\n";

        return false;
    }
    */
}
