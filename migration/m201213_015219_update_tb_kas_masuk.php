<?php

use yii\db\Migration;

/**
 * Class m201213_015219_update_tb_kas_masuk
 */
class m201213_015219_update_tb_kas_masuk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tb_kas_masuk}}','cara_bayar', $this->string(10));
        $this->addColumn('{{%tb_kas_masuk}}','bank', $this->string(20));
        $this->addColumn('{{%tb_kas_masuk}}','reff_no', $this->string(50));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201213_015219_update_tb_kas_masuk cannot be reverted.\n";

        $this->dropColumn('{{%tb_kas_masuk}}','cara_bayar');
        $this->dropColumn('{{%tb_kas_masuk}}','bank');
        $this->dropColumn('{{%tb_kas_masuk}}','reff_no');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201213_015219_update_tb_kas_masuk cannot be reverted.\n";

        return false;
    }
    */
}
