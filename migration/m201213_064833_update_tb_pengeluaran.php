<?php

use yii\db\Migration;

/**
 * Class m201213_064833_update_tb_pengeluaran
 */
class m201213_064833_update_tb_pengeluaran extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tb_pengeluaran}}','keperluan', $this->string());
        $this->addColumn('{{%tb_pengeluaran}}','keterangan', $this->string());
        $this->addColumn('{{%tb_pengeluaran}}','id_jurnal', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201213_064833_update_tb_pengeluaran cannot be reverted.\n";

        $this->dropColumn('{{%tb_pengeluaran}}','keperluan');
        $this->dropColumn('{{%tb_pengeluaran}}','keterangan');
        $this->dropColumn('{{%tb_pengeluaran}}','id_jurnal');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201213_064833_update_tb_pengeluaran cannot be reverted.\n";

        return false;
    }
    */
}
