<?php

use yii\db\Migration;

/**
 * Class m201213_211924_update_tb_pengajuan
 */
class m201213_211924_update_tb_pengajuan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tb_pengajuan}}','tanggal', $this->date());
        $this->addColumn('{{%tb_pengajuan}}','pengajuan_oleh', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201213_211924_update_tb_pengajuan cannot be reverted.\n";
        $this->dropColumn('{{%tb_pengajuan}}','tanggal');
        $this->dropColumn('{{%tb_pengajuan}}','pengajuan_oleh');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201213_211924_update_tb_pengajuan cannot be reverted.\n";

        return false;
    }
    */
}
