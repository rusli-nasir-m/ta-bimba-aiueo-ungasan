<?php

use yii\db\Migration;

/**
 * Class m201214_005527_update_tb_pengeluaran
 */
class m201214_005527_update_tb_pengeluaran extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tb_pengeluaran}}','jenis_permohonan', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201214_005527_update_tb_pengeluaran cannot be reverted.\n";
        $this->dropColumn('{{%tb_pengeluaran}}','jenis_permohonan');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201214_005527_update_tb_pengeluaran cannot be reverted.\n";

        return false;
    }
    */
}
