<?php

use yii\db\Migration;

/**
 * Class m201214_011750_update_tb_kas_masuk
 */
class m201214_011750_update_tb_kas_masuk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tb_kas_masuk}}','jenis_permohonan', $this->integer());
        $this->addPrimaryKey('id_kas_masuk_pk', 'tb_kas_masuk', ['id_kas_masuk']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201214_011750_update_tb_kas_masuk cannot be reverted.\n";
        $this->dropColumn('{{%tb_kas_masuk}}','jenis_permohonan');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201214_011750_update_tb_kas_masuk cannot be reverted.\n";

        return false;
    }
    */
}
