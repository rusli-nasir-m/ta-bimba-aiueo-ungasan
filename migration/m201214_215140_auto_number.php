<?php

use yii\db\Schema;
use yii\db\Migration;

class m201214_215140_auto_number extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%auto_number}}',
            [
                'group'=> $this->string(32)->notNull(),
                'format'=> $this->string(200)->null()->defaultValue(null),
                'number'=> $this->integer(11)->null()->defaultValue(null),
                'optimistic_lock'=> $this->integer(11)->null()->defaultValue(null),
                'update_time'=> $this->integer(11)->null()->defaultValue(null),
                'template'=> $this->string(255)->null()->defaultValue(null),
            ],$tableOptions
        );
        $this->addPrimaryKey('pk_on_auto_number','{{%auto_number}}',['group']);

    }

    public function safeDown()
    {
    $this->dropPrimaryKey('pk_on_auto_number','{{%auto_number}}');
        $this->dropTable('{{%auto_number}}');
    }
}
