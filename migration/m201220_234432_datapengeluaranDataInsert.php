<?php

use yii\db\Schema;
use yii\db\Migration;

class m201220_234432_datapengeluaranDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->batchInsert('{{%datapengeluaran}}',
                           ["replid", "nama", "rekdebet", "rekkredit", "aktif", "keterangan", "besar", "info1", "info2", "info3"],
                            [
    [
        'replid' => '4',
        'nama' => 'kaskecil',
        'rekdebet' => '1-10001',
        'rekkredit' => '1-10002',
        'aktif' => '2',
        'keterangan' => 'Pengajuan Kas Kecil',
        'besar' => '0',
        'info1' => null,
        'info2' => null,
        'info3' => null,
    ],
    [
        'replid' => '5',
        'nama' => 'Biaya Air',
        'rekdebet' => '1-10001',
        'rekkredit' => '6-60218',
        'aktif' => '1',
        'keterangan' => 'Pembayaran Air PDAM',
        'besar' => '0',
        'info1' => null,
        'info2' => null,
        'info3' => null,
    ],
    [
        'replid' => '6',
        'nama' => 'Biaya Listrik',
        'rekdebet' => '1-10001',
        'rekkredit' => '6-60217',
        'aktif' => '1',
        'keterangan' => 'Pembayaran Listrik PLN',
        'besar' => '0',
        'info1' => null,
        'info2' => null,
        'info3' => null,
    ],
]
        );
    }

    public function safeDown()
    {
        //$this->truncateTable('{{%datapengeluaran}} CASCADE');
    }
}
