<?php

use yii\db\Migration;

/**
 * Class m201220_234819_update_tb_spp
 */
class m201220_234819_update_tb_spp extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tb_spp}}','lunas', $this->integer(1)->defaultValue(0)->notNull());
        $this->addColumn('{{%tb_spp}}','jatuh_tempo', $this->date());
        $this->addColumn('{{%tb_spp}}','id_jurnal_piutang', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201220_234819_update_tb_spp cannot be reverted.\n";
        $this->dropColumn('{{%tb_spp}}','lunas');
        $this->dropColumn('{{%tb_spp}}','jatuh_tempo');
        $this->dropColumn('{{%tb_spp}}','id_jurnal_piutang');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201220_234819_update_tb_spp cannot be reverted.\n";

        return false;
    }
    */
}
