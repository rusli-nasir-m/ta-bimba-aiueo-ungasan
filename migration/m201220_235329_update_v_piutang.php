<?php

use yii\db\Migration;

/**
 * Class m201220_235329_update_v_piutang
 */
class m201220_235329_update_v_piutang extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';
        $this->db->createCommand()->createView('v_piutang_spp',"
        SELECT
	SUM(tb_spp.jumlah) as total
FROM
	tb_spp
	WHERE tb_spp.lunas <> 1 
        ")->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201220_235329_update_v_piutang cannot be reverted.\n";

        $this->db->createCommand()->dropView('v_piutang_spp')->execute();
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201220_235329_update_v_piutang cannot be reverted.\n";

        return false;
    }
    */
}
