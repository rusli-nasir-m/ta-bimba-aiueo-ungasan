<?php

use yii\db\Migration;

/**
 * Class m210104_073008_update_tb_kas_masuk
 */
class m210104_073008_update_tb_kas_masuk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tb_kas_masuk}}','reff_spp', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210104_073008_update_tb_kas_masuk cannot be reverted.\n";
        $this->dropColumn('{{%tb_kas_masuk}}','reff_spp');
//        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210104_073008_update_tb_kas_masuk cannot be reverted.\n";

        return false;
    }
    */
}
