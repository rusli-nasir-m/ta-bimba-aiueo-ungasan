<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tb_akun}}".
 *
 * @property int $id
 * @property string $nama
 * @property string $kode
 * @property int $kategori_akun_id
 * @property int $pajak
 * @property int $saldo_awal
 * @property int $saldo
 * @property string $keterangan
 *
 * @property JurnalDetail[] $jurnalDetails
 * @property KategoriAkun $kategoriAkun
 */
class Akun extends \yii\db\ActiveRecord
{

    const SCENARIO_CREATE ='create';
    const SCENARIO_UPDATE ='update';

    public $debit;
    public $kredit;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tb_akun}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kategori_akun_id', 'keterangan','saldo','saldo_awal'], 'required'],
            [['kode'],'unique', 'on' => self::SCENARIO_CREATE],
            [['kode'],'unique','on' => self::SCENARIO_UPDATE,'when' => function($x){
                return $x->isAttributeChanged('kode');
            }],
            [['kategori_akun_id'], 'integer'],
            [['pajak', 'saldo_awal', 'saldo'],'number'],
            [['keterangan'], 'string'],
            [['debit','kredit'], 'number'],
            [['nama'], 'string', 'max' => 30],
            [['kode'], 'string', 'max' => 20],
            [['kategori_akun_id'], 'exist', 'skipOnError' => true, 'targetClass' => KategoriAkun::className(), 'targetAttribute' => ['kategori_akun_id' => 'id_kategori']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama Akun'),
            'kode' => Yii::t('app', 'Kode Akun'),
            'kelompok_akun_id' => Yii::t('app', 'Kategori Akun'),
            'pajak' => Yii::t('app', 'Pajak'),
            'saldo_awal' => Yii::t('app', 'Saldo Awal'),
            'saldo' => Yii::t('app', 'Saldo'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'kelompokAkun' => Yii::t('app', 'Kategori Akun'),
        ];
    }

    /**
     * Gets query for [[JurnalDetails]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\JurnalDetailQuery
     */
    public function getJurnalDetails()
    {
        return $this->hasMany(JurnalDetail::className(), ['akun_id' => 'id']);
    }

    /**
     * Gets query for [[KategoriAkun]].
     *
     * @return \yii\db\ActiveQuery|KategoriAkun
     */
    public function getKategoriAkun()
    {
        return $this->hasOne(KategoriAkun::className(), ['id_kategori' => 'kategori_akun_id']);
    }

    public static function getIDByKode($kode)
    {
        $model = self::findOne(['kode' => $kode]);
        return $model?$model->id:null;
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\TbAkunQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\TbAkunQuery(get_called_class());
    }
}
