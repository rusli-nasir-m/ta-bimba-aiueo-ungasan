<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%datapenerimaan}}".
 *
 * @property int $replid
 * @property string $nama
 * @property float|null $besar
 * @property string $idkategori
 * @property string $rekkas
 * @property string $rekpendapatan
 * @property string|null $rekpiutang
 * @property int $aktif
 * @property string|null $keterangan
 * @property string $departemen
 * @property string|null $info1
 * @property string|null $info2
 * @property string|null $info3
 *
 * @property Akun $rekkas0
 * @property Akun $rekpendapatan0
 * @property Akun $rekpiutang0
 * @property Kategoripenerimaan $idkategori0
 */
class Datapenerimaan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%datapenerimaan}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['nama', 'idkategori', 'rekkas', 'rekpendapatan'], 'required'],
            [['besar'], 'number'],
            [['aktif'], 'integer'],
            [['nama'], 'string', 'max' => 100],
            [['idkategori'], 'string', 'max' => 15],
            [['rekkas', 'rekpendapatan', 'rekpiutang'], 'string', 'max' => 20],
            [['keterangan', 'info1', 'info2', 'info3'], 'string', 'max' => 255],
            [['departemen'], 'string', 'max' => 50],
            [['rekkas'], 'exist', 'skipOnError' => true, 'targetClass' => Akun::className(), 'targetAttribute' => ['rekkas' => 'kode']],
            [['rekpendapatan'], 'exist', 'skipOnError' => true, 'targetClass' => Akun::className(), 'targetAttribute' => ['rekpendapatan' => 'kode']],
            [['rekpiutang'], 'exist', 'skipOnError' => true, 'targetClass' => Akun::className(), 'targetAttribute' => ['rekpiutang' => 'kode']],
            [['idkategori'], 'exist', 'skipOnError' => true, 'targetClass' => Kategoripenerimaan::className(), 'targetAttribute' => ['idkategori' => 'kode']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'replid' => Yii::t('app', 'Replid'),
            'nama' => Yii::t('app', 'Nama'),
            'besar' => Yii::t('app', 'Besar'),
            'idkategori' => Yii::t('app', 'Kategori'),
            'rekkas' => Yii::t('app', 'Rekening Kas'),
            'rekpendapatan' => Yii::t('app', 'Rekening Pendapatan'),
            'rekpiutang' => Yii::t('app', 'Rekening Piutang'),
            'aktif' => Yii::t('app', 'Aktif'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'departemen' => Yii::t('app', 'Departemen'),
            'info1' => Yii::t('app', 'Rekening Diskon'),
            'info2' => Yii::t('app', 'Info2'),
            'info3' => Yii::t('app', 'Info3'),
        ];
    }

    /**
     * Gets query for [[Rekkas0]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\TbAkunQuery
     */
    public function getRekkas0()
    {
        return $this->hasOne(Akun::className(), ['kode' => 'rekkas']);
    }

    /**
     * Gets query for [[Rekpendapatan0]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\TbAkunQuery
     */
    public function getRekpendapatan0()
    {
        return $this->hasOne(Akun::className(), ['kode' => 'rekpendapatan']);
    }

    /**
     * Gets query for [[Rekpiutang0]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\TbAkunQuery
     */
    public function getRekpiutang0()
    {
        return $this->hasOne(Akun::className(), ['kode' => 'rekpiutang']);
    }

    /**
     * Gets query for [[Idkategori0]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\KategoripenerimaanQuery
     */
    public function getIdkategori0()
    {
        return $this->hasOne(Kategoripenerimaan::className(), ['kode' => 'idkategori']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\DatapenerimaanQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\DatapenerimaanQuery(get_called_class());
    }

    public static function selectOptions()
    {
        $model = self::find()->all();
        if ($model){
            return ArrayHelper::map($model,'replid','nama');
        }
        return [];
    }
}
