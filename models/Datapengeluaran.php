<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%datapengeluaran}}".
 *
 * @property int $replid
 * @property string $nama
 * @property string $rekdebet
 * @property string $rekkredit
 * @property int $aktif
 * @property string|null $keterangan
 * @property float $besar
 * @property string|null $info1
 * @property string|null $info2
 * @property string|null $info3
 *
 * @property Akun $rekdebet0
 * @property Akun $rekkredit0
 */
class Datapengeluaran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%datapengeluaran}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'rekdebet', 'rekkredit'], 'required'],
            [['aktif'], 'integer'],
            [['besar'], 'number'],
            [['nama'], 'string', 'max' => 100],
            [['rekdebet', 'rekkredit'], 'string', 'max' => 20],
            [['keterangan', 'info1', 'info2', 'info3'], 'string', 'max' => 255],
            [['rekdebet'], 'exist', 'skipOnError' => true, 'targetClass' => Akun::className(), 'targetAttribute' => ['rekdebet' => 'kode']],
            [['rekkredit'], 'exist', 'skipOnError' => true, 'targetClass' => Akun::className(), 'targetAttribute' => ['rekkredit' => 'kode']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'replid' => Yii::t('app', 'Replid'),
            'nama' => Yii::t('app', 'Nama'),
            'rekdebet' => Yii::t('app', 'Rek Debet'),
            'rekkredit' => Yii::t('app', 'Rek Kredit'),
            'aktif' => Yii::t('app', 'Aktif'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'besar' => Yii::t('app', 'Besar'),
            'info1' => Yii::t('app', 'Info1'),
            'info2' => Yii::t('app', 'Info2'),
            'info3' => Yii::t('app', 'Info3'),
        ];
    }

    /**
     * Gets query for [[Rekdebet0]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\TbAkunQuery
     */
    public function getRekdebet0()
    {
        return $this->hasOne(Akun::className(), ['kode' => 'rekdebet']);
    }

    /**
     * Gets query for [[Rekkredit0]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\TbAkunQuery
     */
    public function getRekkredit0()
    {
        return $this->hasOne(Akun::className(), ['kode' => 'rekkredit']);
    }

    public static function selectOptions()
    {
        $model = self::find()->where(['aktif' => 1])->all();
        if ($model){
            return ArrayHelper::map($model,'replid','nama');
        }
        return [];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\DatapengeluaranQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\DatapengeluaranQuery(get_called_class());
    }
}
