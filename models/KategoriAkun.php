<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%kategori_akun}}".
 *
 * @property int $id_kategori
 * @property string|null $kategori_akun
 * @property int|null $kelompok_akun_id
 *
 * @property Akun[] $akuns
 * @property KelompokAkun $kelompokAkun
 */
class KategoriAkun extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%kategori_akun}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kelompok_akun_id'], 'integer'],
            [['kategori_akun'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kategori' => Yii::t('app', 'Id Kategori'),
            'kategori_akun' => Yii::t('app', 'Kategori Akun'),
            'kelompok_akun_id' => Yii::t('app', 'Kelompok Akun ID'),
        ];
    }

    public static function selectOptions()
    {
        return ArrayHelper::map(self::find()->all(),'id_kategori','kategori_akun');
    }

    /**
     * Gets query for [[TbAkuns]].
     *
     * @return \yii\db\ActiveQuery|Akun[]
     */
    public function getAkuns()
    {
        return $this->hasMany(Akun::className(), ['kategori_akun_id' => 'id_kategori']);
    }

    /**
     * Gets query for [[KelompokAkun]].
     *
     * @return \yii\db\ActiveQuery|KelompokAkun
     */
    public function getKelompokAkun()
    {
        return $this->hasOne(KelompokAkun::className(), ['id' => 'kelompok_akun_id']);
    }
}
