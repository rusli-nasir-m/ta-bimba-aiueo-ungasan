<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%kategoripenerimaan}}".
 *
 * @property int $replid
 * @property string $kode
 * @property string $kategori
 * @property int $urutan
 * @property int $siswa
 * @property string|null $info1
 * @property string|null $info2
 * @property string|null $info3
 * @property string $ts
 * @property int $token
 * @property int $issync
 *
 * @property Datapenerimaan[] $datapenerimaans
 */
class Kategoripenerimaan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%kategoripenerimaan}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode', 'kategori'], 'required'],
            [['urutan', 'siswa', 'token', 'issync'], 'integer'],
            [['ts'], 'safe'],
            [['kode'], 'string', 'max' => 10],
            [['kategori'], 'string', 'max' => 100],
            [['info1', 'info2', 'info3'], 'string', 'max' => 255],
            [['kode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'replid' => Yii::t('app', 'Replid'),
            'kode' => Yii::t('app', 'Kode'),
            'kategori' => Yii::t('app', 'Kategori'),
            'urutan' => Yii::t('app', 'Urutan'),
            'siswa' => Yii::t('app', 'Siswa'),
            'info1' => Yii::t('app', 'Info1'),
            'info2' => Yii::t('app', 'Info2'),
            'info3' => Yii::t('app', 'Info3'),
            'ts' => Yii::t('app', 'Ts'),
            'token' => Yii::t('app', 'Token'),
            'issync' => Yii::t('app', 'Issync'),
        ];
    }

    /**
     * Gets query for [[Datapenerimaans]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\DatapenerimaanQuery
     */
    public function getDatapenerimaans()
    {
        return $this->hasMany(Datapenerimaan::className(), ['idkategori' => 'kode']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\KategoripenerimaanQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\KategoripenerimaanQuery(get_called_class());
    }
}
