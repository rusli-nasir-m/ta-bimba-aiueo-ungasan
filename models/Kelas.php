<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tb_kelas}}".
 *
 * @property string $kode_kelas
 * @property string|null $nama_kelas
 * @property int $jumlah_pertemuan [int(11)]  Jumlah Pertemuan Dalam 1 Bulan
 * @property int $durasi [int(11)]  Lama Durasi Belajar
 * @property int $harga [int(11)]  Harga
 */
class Kelas extends \yii\db\ActiveRecord
{
    const SCENARIO_INSERT = 'insert';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tb_kelas}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_kelas'], 'required'],
            [['kode_kelas'], 'autonumber', 'format' =>'KLS?', 'digit' => 4,'on' => self::SCENARIO_INSERT],
            [['kode_kelas'], 'string', 'max' => 10],
            [['nama_kelas'], 'string', 'max' => 255],
            [['kode_kelas'], 'unique'],
            [['jumlah_pertemuan','durasi','harga'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kode_kelas' => Yii::t('app', 'Kode Kelas'),
            'nama_kelas' => Yii::t('app', 'Nama Kelas'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\KelasQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\KelasQuery(get_called_class());
    }
}
