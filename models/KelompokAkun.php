<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%kelompok_akun}}".
 *
 * @property int $id
 * @property string $nama
 *
 * @property Akun[] $akuns
 */
class KelompokAkun extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%kelompok_akun}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama'),
        ];
    }

    /**
     * Gets query for [[TbAkuns]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\TbAkunQuery
     */
    public function getAkuns()
    {
        return $this->hasMany(Akun::className(), ['kelompok_akun_id' => 'id']);
    }

    public static function selectOptions()
    {
        return ArrayHelper::map(self::find()->all(),'id','nama');
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\KelompokAkunQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\KelompokAkunQuery(get_called_class());
    }
}
