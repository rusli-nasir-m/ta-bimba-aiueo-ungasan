<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%pegawai}}".
 *
 * @property string $id_pegawai
 * @property string|null $nama_pegawai
 * @property string|null $jenis_kelamin
 * @property string|null $golongan
 * @property string|null $jabatan
 * @property string|null $mulai_bekerja
 * @property integer|null $jumlah_tanggungan
 * @property string $gaji_pokok [decimal(15,2)]
 * @property-read User $user
 * @property int $id_user [int(11)]
 */
class Pegawai extends \yii\db\ActiveRecord
{

    const SCENARIO_CREATE ='create';
    const SCENARIO_UPDATE ='update';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%pegawai}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pegawai'], 'required'],
            [['id_pegawai'],'unique', 'on' => self::SCENARIO_CREATE],
            [['id_pegawai'],'unique','on' => self::SCENARIO_UPDATE,'when' => function($x){
                return $x->isAttributeChanged('kode');
            }],
            [['id_pegawai'], 'string', 'max' => 100],
            [['nama_pegawai'], 'string', 'max' => 50],
            [['jenis_kelamin'], 'string', 'max' => 20],
            [['golongan', 'jabatan'], 'string', 'max' => 25],
            [['id_pegawai'], 'unique'],
            [['mulai_bekerja'], 'safe'],
            [['jumlah_tanggungan','gaji_pokok','id_user'], 'integer'],
            [['jumlah_tanggungan','gaji_pokok'], 'default','value' => 0],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_pegawai' => Yii::t('app', 'Id Pegawai'),
            'nama_pegawai' => Yii::t('app', 'Nama Pegawai'),
            'jenis_kelamin' => Yii::t('app', 'Jenis Kelamin'),
            'golongan' => Yii::t('app', 'Golongan'),
            'jabatan' => Yii::t('app', 'Jabatan'),
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(),['id' => 'id_user']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\PegawaiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\PegawaiQuery(get_called_class());
    }
}
