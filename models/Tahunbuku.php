<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tahunbuku}}".
 *
 * @property int $id
 * @property string $tahunbuku
 * @property string $awalan
 * @property int $aktif
 * @property string|null $keterangan
 * @property int $cacah
 * @property string $tanggalmulai
 * @property string|null $tanggalselesai
 */
class Tahunbuku extends \yii\db\ActiveRecord
{

    const SCENARIO_INSERT = 'insert';
    const SCENARIO_UPDATE = 'update';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tahunbuku}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tahunbuku'],'unique','on' => self::SCENARIO_INSERT,'message' => 'Sudah Pernah digunakan dn tidak boleh sama...'],
            [['awalan'],'unique','on' => self::SCENARIO_INSERT,'message' => 'Sudah Pernah digunakan dn tidak boleh sama...'],
            [['tahunbuku', 'awalan', 'tanggalmulai'], 'required'],
            [['aktif', 'cacah'], 'integer'],
            [['tanggalmulai', 'tanggalselesai'], 'safe'],
            [['tahunbuku'], 'string', 'max' => 100],
            [['awalan'], 'string', 'max' => 5],
            [['keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tahunbuku' => Yii::t('app', 'Tahunbuku'),
            'awalan' => Yii::t('app', 'Awalan'),
            'aktif' => Yii::t('app', 'Aktif'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'cacah' => Yii::t('app', 'Cacah'),
            'tanggalmulai' => Yii::t('app', 'Tanggalmulai'),
            'tanggalselesai' => Yii::t('app', 'Tanggalselesai'),
        ];
    }



    /**
     * {@inheritdoc}
     * @return \app\models\query\TahunbukuQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\TahunbukuQuery(get_called_class());
    }

    public static function getActive()
    {
        return self::find()->active()->one();
    }

    public static function tutupBuku()
    {
        $active = self::find()->where(['aktif' => 1])->count();
        if ($active <= 0){
            $errmsg = "Belum ada tahun buku!<br>Tentukan terlebih dahulu tahun buku awal di menu Tahun Buku";
        }else{

        }
    }
}
