<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tb_bayar_spp}}".
 *
 * @property int $id_pembayaran
 * @property string|null $no_kwitansi
 * @property string|null $nim_murid
 * @property string|null $tanggal_bayar
 * @property string|null $keterangan
 *
 * @property TbSpp[] $tbSpps
 */
class TbBayarSpp extends \yii\db\ActiveRecord
{

    const CARA_BAYAR_CASH = 'cash';
    const CARA_BAYAR_TRANSFER = 'transfer';
    const CARA_BAYAR_EDC = 'edc';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tb_bayar_spp}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pembayaran'], 'required'],
            [['id_pembayaran'], 'integer'],
            [['tanggal_bayar'], 'safe'],
            [['no_kwitansi'], 'string', 'max' => 20],
            [['nim_murid'], 'string', 'max' => 100],
            [['keterangan'], 'string', 'max' => 255],
            [['id_pembayaran'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_pembayaran' => Yii::t('app', 'Id Pembayaran'),
            'no_kwitansi' => Yii::t('app', 'No Kwitansi'),
            'nim_murid' => Yii::t('app', 'Nim Murid'),
            'tanggal_bayar' => Yii::t('app', 'Tanggal Bayar'),
            'keterangan' => Yii::t('app', 'Keterangan'),
        ];
    }

    /**
     * Gets query for [[TbSpps]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTbSpps()
    {
        return $this->hasMany(TbSpp::className(), ['id_pembayaran' => 'id_pembayaran']);
    }
}
