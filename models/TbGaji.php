<?php

namespace app\models;

use Steevenz\IndonesiaPayrollCalculator\PayrollCalculator;
use Yii;

/**
 * This is the model class for table "{{%tb_gaji}}".
 *
 * @property string $id_gaji
 * @property string|null $id_pegawai
 * @property string|null $tanggal
 * @property string|null $masa_kerja
 * @property int|null $tunjangan_makan
 * @property int|null $tunjangan_kesehatan
 * @property int|null $tunjangan_jabatan
 * @property int|null $insentif
 * @property string|null $jabatan
 * @property int|null $gaji_pokok
 * @property int|null $potongan
 * @property-read Pegawai $pegawai
 * @property int|null $total_gaji
 */
class TbGaji extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tb_gaji}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_gaji'], 'required'],
            [['tanggal'], 'safe'],
            [['tunjangan_makan','insentif', 'tunjangan_kesehatan','tunjangan_jabatan', 'gaji_pokok', 'potongan', 'total_gaji'], 'integer'],
            [['id_gaji', 'id_pegawai'], 'string', 'max' => 100],
            [['masa_kerja'], 'string', 'max' => 50],
            [['jabatan'], 'string', 'max' => 25],
            [['id_gaji'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_gaji' => Yii::t('app', 'Id Gaji'),
            'id_pegawai' => Yii::t('app', 'Id Pegawai'),
            'tanggal' => Yii::t('app', 'Tanggal'),
            'masa_kerja' => Yii::t('app', 'Masa Kerja'),
            'tunjangan_makan' => Yii::t('app', 'Tunjangan Makan'),
            'tunjangan_kesehatan' => Yii::t('app', 'Tunjangan Kesehatan'),
            'tunjangan_jabatan' => Yii::t('app', 'Tunjangan Jabatan'),
            'insentif' => Yii::t('app', 'Insentif'),
            'jabatan' => Yii::t('app', 'Jabatan'),
            'gaji_pokok' => Yii::t('app', 'Gaji Pokok'),
            'potongan' => Yii::t('app', 'Potongan'),
            'total_gaji' => Yii::t('app', 'Total Gaji'),
        ];
    }

    public function getPegawai()
    {
        return $this->hasOne(Pegawai::className(),['id_pegawai' => 'id_pegawai']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\TbGajiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\TbGajiQuery(get_called_class());
    }

    public function kalkulasiGaji($id_pegawai)
    {
        $payrollCalculator = new PayrollCalculator();

        // Khusus Perhitungan PPH 21 -------

// Calculation method
        $payrollCalculator->method = PayrollCalculator::NETT_CALCULATION;

// Tax Number
        $payrollCalculator->taxNumber = 21;

// Set data karyawan
        $payrollCalculator->employee->permanentStatus = false; // Tetap (true), Tidak Tetap (false), secara default sudah terisi nilai true.
        $payrollCalculator->employee->maritalStatus = true; // Menikah (true), Tidak Menikah/Single (false), secara default sudah terisi nilai false.
        $payrollCalculator->employee->hasNPWP = true; // Secara default sudah terisi nilai true. Jika tidak memiliki npwp akan dikenakan potongan tambahan 20%
        $payrollCalculator->employee->numOfDependentsFamily = (int)$this->pegawai->jumlah_tanggungan; // Jumlah tanggungan, max 5 jika lebih akan dikenakan tambahannya perorang sesuai ketentuan BPJS Kesehatan

// Set data pendapatan karyawan
        $payrollCalculator->employee->earnings->base = (int)$this->pegawai->gaji_pokok; // Besaran nilai gaji pokok/bulan
        $payrollCalculator->employee->earnings->fixedAllowance = $this->tunjangan_jabatan; // Besaran nilai tunjangan tetap
        $payrollCalculator->employee->calculateHolidayAllowance = 0; // jumlah bulan proporsional
// NOTE: besaran nilai diatas bukan nilai hasil proses perhitungan absensi tetapi nilai default sebagai faktor perhitungan gaji.

// Set data kehadiran karyawan
        $payrollCalculator->employee->presences->workDays = 25; // jumlah hari masuk kerja
        $payrollCalculator->employee->presences->overtime = 0; //  perhitungan jumlah lembur dalam satuan jam
        $payrollCalculator->employee->presences->latetime = 0; //  perhitungan jumlah keterlambatan dalam satuan jam
        $payrollCalculator->employee->presences->travelDays = 0; //  perhitungan jumlah hari kepergian dinas
        $payrollCalculator->employee->presences->indisposedDays = 0; //  perhitungan jumlah hari sakit yang telah memiliki surat dokter
        $payrollCalculator->employee->presences->absentDays = 0; //  perhitungan jumlah hari alpha
        $payrollCalculator->employee->presences->splitShifts = 0; // perhitungan jumlah split shift

// Set data tunjangan karyawan diluar tunjangan BPJS Kesehatan dan Ketenagakerjaan
        $payrollCalculator->employee->allowances->offsetSet('tunjanganMakan', $this->tunjangan_makan);
//        $payrollCalculator->employee->allowances->offsetSet('tunjanganJabatan', $this->tunjangan_jabatan);
// NOTE: Jumlah allowances tidak ada batasan

// Set data potongan karyawan diluar potongan BPJS Kesehatan dan Ketenagakerjaan
        $payrollCalculator->employee->deductions->offsetSet('kasbon', 100000);
// NOTE: Jumlah deductions tidak ada batasan

// Set data bonus karyawan diluar tunjangan
        $payrollCalculator->employee->bonus->offsetSet('insentif', $this->insentif);
//        $payrollCalculator->employee->bonus->offsetSet('achivement', 1000000);
// NOTE: Jumlah bonus tidak ada batasan

// Set data ketentuan negara
        $payrollCalculator->provisions->state->overtimeRegulationCalculation = true; // Jika false maka akan dihitung sesuai kebijakan perusahaan
        $payrollCalculator->provisions->state->provinceMinimumWage = 2297967; // Ketentuan UMP sesuai propinsi lokasi perusahaan (Bali)

// Set data ketentuan perusahaan
        $payrollCalculator->provisions->company->numOfWorkingDays = 25; // Jumlah hari kerja dalam satu bulan
        $payrollCalculator->provisions->company->numOfWorkingHours = 8; // Jumlah hari kerja dalam satu hari
        $payrollCalculator->provisions->company->calculateOvertime = false; // Apakah perusahaan menghitung lembur

// Jika $payrollCalculator->provisions->state->overtimeRegulationCalculation = false;
        $payrollCalculator->provisions->company->overtimeRate = 10000; // Nilai rate overtime per jam, Jika bernilai 0 namun $payrollCalculator->provisions->company->calculateOvertime, maka rate akan dihitung secara otomatis berdasarkan renumerasi besaran gaji, hari dan jam kerja

        $payrollCalculator->provisions->company->calculateSplitShifts = false; // Apakah perusahan menghitung split shifts
        $payrollCalculator->provisions->company->splitShiftsRate = 25000; // Rate Split Shift perusahaan
        $payrollCalculator->provisions->company->calculateBPJSKesehatan = true; // Apakah perusahaan menyediakan BPJS Kesehatan / tidak untuk orang tersebut

// Apakah perusahaan menyediakan BPJS Ketenagakerjaan / tidak untuk orang tersebut
        $payrollCalculator->provisions->company->JKK = true; //Jaminan Kecelakaan Kerja (JKK)
        $payrollCalculator->provisions->company->JKM = true; //Jaminan Kematian
        $payrollCalculator->provisions->company->JHT = true; //Jaminan Hari Tua (JHT)
        $payrollCalculator->provisions->company->JIP = true; //Jaminan Pensiun

        $payrollCalculator->provisions->company->riskGrade = 5; // Golongan resiko ketenagakerjaan, umumnya 2
        $payrollCalculator->provisions->company->absentPenalty = 55000; // Perhitungan nilai potongan gaji/hari sebagai penalty.
        $payrollCalculator->provisions->company->latetimePenalty = 100000; // Perhitungan nilai keterlambatan sebagai penalty.

// Mengambil hasil perhitungan
        //$payrollCalculator->getCalculation(); // Berupa SplArrayObject yang berisi seluruh data perhitungan gaji, lengkap dengan perhitungan BPJS dan PPh21

// Khusus Perhitungan PPH 23 -------
//        $payrollCalculator->taxNumber = 23;
//        $payrollCalculator->employee->hasNPWP = true;
//        $payrollCalculator->employee->earnings->base = $this->gaji_pokok;

// Khusus Perhitungan PPH 26 -------
//        $payrollCalculator->taxNumber = 26;
//        $payrollCalculator->employee->hasNPWP = true;
//        $payrollCalculator->employee->earnings->base = $this->gaji_pokok;

// Mengambil hasil perhitungan
        return $payrollCalculator->getCalculation(); // Berupa SplArrayObject yang berisi lengkap dengan perhitungan pajak

    }
}
