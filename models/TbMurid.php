<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%tb_murid}}".
 *
 * @property string $nim_murid
 * @property string|null $nama
 * @property string|null $kode_kelas
 * @property string|null $tanggal_lahir
 * @property string|null $alamat
 * @property string|null $no_telp
 * @property string|null $asal_sekolah
 * @property string|null $tgl_masuk
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string $status [varchar(20)]
 * @property-read Kelas $kelas
 * @property string $agama [varchar(20)]
 */
class TbMurid extends \yii\db\ActiveRecord
{

    const STATUS_AKTIF = 'aktif';
    const STATUS_NON_AKTIF = 'non_aktif';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tb_murid}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nim_murid'], 'required'],
            ['nim_murid', 'unique', 'on'=>'create'],
            ['nim_murid', 'unique', 'on'=>'update', 'when' => function($model){
                return $model->isAttributeChanged('nim_murid');
            },'message' => 'NIM Sudah terdaftar'],
            [['tanggal_lahir', 'tgl_masuk', 'created_at', 'updated_at'], 'safe'],
            [['asal_sekolah'], 'string'],
            [['status','agama'], 'string', 'max' => 20],
            [['nim_murid'], 'string', 'max' => 100],
            [['nama', 'kode_kelas', 'alamat'], 'string', 'max' => 255],
            [['no_telp'], 'string', 'max' => 200],
            [['status'], 'in','range' => [self::STATUS_AKTIF,self::STATUS_NON_AKTIF]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nim_murid' => Yii::t('app', 'Nim Murid'),
            'nama' => Yii::t('app', 'Nama'),
            'kode_kelas' => Yii::t('app', 'Kode Kelas'),
            'tanggal_lahir' => Yii::t('app', 'Tanggal Lahir'),
            'alamat' => Yii::t('app', 'Alamat'),
            'no_telp' => Yii::t('app', 'No Telp'),
            'asal_sekolah' => Yii::t('app', 'Asal Sekolah'),
            'tgl_masuk' => Yii::t('app', 'Tgl Masuk'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getKelas()
    {
        return $this->hasOne(Kelas::className(),['kode_kelas' => 'kode_kelas']);
    }

    public static function listStatus()
    {
        return [
            self::STATUS_AKTIF => 'Aktif',
            self::STATUS_NON_AKTIF => 'Non Aktif',
        ];
    }

    public static function jumlahMuridaktif()
    {
        return self::find()->active()->count();
    }

    public static function jumlahMuridNonaktif()
    {
        return self::find()->nonActive()->count();
    }

    public static function perkembanganMuridPerTahun($tahun)
    {
        $query = Yii::$app->db->createCommand("
SELECT
	YEAR(a.tgl_masuk) as `year`, 
	MONTH(a.tgl_masuk) as `month`,
	COUNT(a.nim_murid) as count
FROM
	tb_murid AS a
	WHERE YEAR(a.tgl_masuk) =:paramTahun
	GROUP BY YEAR(a.tgl_masuk), MONTH(a.tgl_masuk) DESC",[':paramTahun' => $tahun]);
        $dataRes = $query->queryAll();
        $dataMap = [];
        if ($dataRes){
            $arrMap = ArrayHelper::map($dataRes,'month','count');
            $listBulan = getParams('bulan');
            foreach ($listBulan as $key => $value) {
                $dataMap[$value] = $arrMap[$key];
            }
        }
        return $dataMap;
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\TbMuridQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\TbMuridQuery(get_called_class());
    }
}
