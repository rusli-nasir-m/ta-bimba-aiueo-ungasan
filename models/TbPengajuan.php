<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tb_pengajuan}}".
 *
 * @property string $id_pengajuan
 * @property string|null $id_pegawai
 * @property string|null $jenis_pengajuan
 * @property string|null $kegiatan
 * @property int|null $id_jurnal
 * @property float|null $total_pengajuan
 * @property int|null $validasi_pengejuan
 * @property string|null $validasi_tanggal
 * @property int|null $validasi_oleh
 * @property string $tanggal [date]
 * @property-read Jurnal $jurnal
 * @property int|null $pengajuan_oleh [varchar(255)]
 */
class TbPengajuan extends \yii\db\ActiveRecord
{

    public $rekDebit;
    public $rekKredit;

    const SCENARIO_INSERT = 'insert';
    const SCENARIO_VALIDASI = 'validasi';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tb_pengajuan}}';
    }

    public function scenarios()
    {
        return [
          self::SCENARIO_VALIDASI => ['id_pengajuan','jenis_pengajuan','rekDebit','rekKredit','total_pengajuan','validasi_pengejuan', 'validasi_oleh','validasi_tanggal'],
          self::SCENARIO_INSERT => [
              'id_pengajuan','jenis_pengajuan','rekDebit','rekKredit','total_pengajuan','validasi_pengejuan',
              'validasi_oleh','validasi_tanggal','kegiatan','pengajuan_oleh','tanggal'
          ],
        ]; // TODO: Change the autogenerated stub
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            [['id_pengajuan'], 'required'],
            [['total_pengajuan','total_pengajuan','kegiatan'], 'required'],
            [['id_jurnal', 'validasi_pengejuan', 'validasi_oleh','pengajuan_oleh'], 'integer'],
            [['id_pengajuan'], 'autonumber', 'format' =>'JK?', 'digit' => 4,'on' => self::SCENARIO_INSERT],
            [['total_pengajuan'], 'number'],
            [['validasi_tanggal','tanggal','rekDebit','rekKredit'], 'safe'],
            [['id_pengajuan', 'id_pegawai', 'kegiatan'], 'string', 'max' => 100],
            [['jenis_pengajuan'], 'string', 'max' => 25],
            [['id_pengajuan'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_pengajuan' => Yii::t('app', 'No Pengajuan'),
            'id_pegawai' => Yii::t('app', 'Id Pegawai'),
            'jenis_pengajuan' => Yii::t('app', 'Jenis Pengajuan'),
            'kegiatan' => Yii::t('app', 'Kegiatan'),
            'id_jurnal' => Yii::t('app', 'Id Jurnal'),
            'total_pengajuan' => Yii::t('app', 'Total Pengajuan'),
            'validasi_pengejuan' => Yii::t('app', 'Validasi Pengejuan'),
            'validasi_tanggal' => Yii::t('app', 'Validasi Tanggal'),
            'validasi_oleh' => Yii::t('app', 'Validasi Oleh'),
        ];
    }

    public function generateJurnal()
    {
        $tahunBuku = Tahunbuku::find()->active()->one();
        if (!$tahunBuku){
            return false;
        }
        $jurnal = new Jurnal();
        $jurnal->scenario = $jurnal::SCENARIO_INSERT;
        $jurnal->f_id = 1;
        $jurnal->sumberjurnal = 'permintaan_kas';
        $jurnal->tgl = $this->validasi_tanggal;
        $jurnal->tahunbuku_id = $tahunBuku->id;
        $jurnal->keterangan = 'Pengajuan kas kecil - '. $this->kegiatan;
        $jurnal->login_id = getMyId();
        $jurnal->jurnalDetails = [
            [
                'akun_id' => Akun::getIDByKode($this->rekDebit),
                'debit' => $this->total_pengajuan,
            ],
            [
                'akun_id' => Akun::getIDByKode($this->rekKredit),
                'kredit' => $this->total_pengajuan,
            ]
        ];
        return $jurnal;
    }

    public function getJurnal()
    {
        return $this->hasOne(Jurnal::className(),['id' => 'id_jurnal']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\TbPengajuanQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\TbPengajuanQuery(get_called_class());
    }
}
