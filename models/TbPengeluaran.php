<?php

namespace app\models;

use app\models\query\TbPengeluaranQuery;
use Yii;

/**
 * This is the model class for table "{{%tb_pengeluaran}}".
 *
 * @property string $id_pengeluaran
 * @property int|null $id_user
 * @property string|null $id_pengajuan
 * @property string|null $jenis_pengeluaran
 * @property string|null $tanggal_realisasi
 * @property int|null $jumlah_pengeluaran
 * @property string|null $keperluan
 * @property string|null $keterangan
 * @property int|null $id_jurnal
 * @property Datapengeluaran $jenisPermohonan
 * @property-read User $user
 * @property-read Jurnal $jurnal
 * @property int $jenis_permohonan [int(11)]
 */
class TbPengeluaran extends \yii\db\ActiveRecord
{
    const SCENARIO_INSERT = 'insert';

    public $rekeningDebit;
    public $rekeningKredit;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tb_pengeluaran}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            [['id_pengeluaran'], 'required'],
            [['id_user', 'jumlah_pengeluaran', 'id_jurnal','jenis_permohonan'], 'integer'],
            [['rekeningDebit','rekeningKredit'],'string'],
            [['id_pengeluaran'], 'autonumber', 'format' =>'OUT?', 'digit' => 4,'on' => self::SCENARIO_INSERT],
            [['tanggal_realisasi'], 'safe'],
            [['id_pengeluaran', 'id_pengajuan'], 'string', 'max' => 100],
            [['jenis_pengeluaran'], 'string', 'max' => 25],
            [['keperluan', 'keterangan'], 'string', 'max' => 255],
            [['id_pengeluaran'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_pengeluaran' => Yii::t('app', 'Id Pengeluaran'),
            'id_user' => Yii::t('app', 'Id User'),
            'id_pengajuan' => Yii::t('app', 'Id Pengajuan'),
            'jenis_pengeluaran' => Yii::t('app', 'Jenis Pengeluaran'),
            'tanggal_realisasi' => Yii::t('app', 'Tanggal Realisasi'),
            'jumlah_pengeluaran' => Yii::t('app', 'Jumlah Pengeluaran'),
            'keperluan' => Yii::t('app', 'Keperluan'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'id_jurnal' => Yii::t('app', 'Id Jurnal'),
        ];
    }

    public function getJenisPermohonan()
    {
        return $this->hasOne(Datapengeluaran::className(),['replid'=> 'jenis_permohonan']);
    }

    /**
     * {@inheritdoc}
     * @return TbPengeluaranQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TbPengeluaranQuery(get_called_class());
    }

    public static function getTotalPengeluaran($bulanTahun= null)
    {
        if(!$bulanTahun){
            $bulanTahun = date('Y-m');
        }
        $data = self::getDb()->createCommand("SELECT SUM(jumlah_pengeluaran) as pengeluaran FROM tb_pengeluaran WHERE  DATE_FORMAT(tanggal_realisasi,'%Y-%m') = {$bulanTahun}")->queryScalar();
        return $data?:0;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(),['id' => 'id_user']);
    }

    public function getJurnal()
    {
        return $this->hasOne(Jurnal::className(),['id' => 'id_jurnal']);
    }

    public static function getTotalPengeluaranPerTahun($tahun= null)
    {
        if(!$tahun){
            $tahun = date('Y');
        }
        $data = self::getDb()->createCommand("SELECT SUM(jumlah_pengeluaran) as pengeluaran FROM tb_pengeluaran WHERE  DATE_FORMAT(tanggal_realisasi,'%Y') = {$tahun}")->queryScalar();
        return (float)$data;
    }

    public function generateJurnal()
    {
        $tahunBuku = Tahunbuku::find()->active()->one();
        if (!$tahunBuku){
            return false;
        }
        $jurnal = new Jurnal();
        $jurnal->scenario = $jurnal::SCENARIO_INSERT;
        $jurnal->f_id = 1;
        $jurnal->tgl = $this->tanggal_realisasi;
        $jurnal->tahunbuku_id = $tahunBuku->id;
        $jurnal->waktu_post = date('Y-m-d H:i:s');
        $jurnal->keterangan = $this->keterangan;
        $jurnal->sumberjurnal = 'pengeluaran';
        $jurnal->login_id = getMyId();

        $jurnal->jurnalDetails = [
            [
                'akun_id' => $this->rekeningDebit,
                'debit' => $this->jumlah_pengeluaran,
            ],
            [
                'akun_id' => $this->rekeningKredit,
                'kredit' => $this->jumlah_pengeluaran,
            ],
        ];
        return $jurnal;
    }
}
