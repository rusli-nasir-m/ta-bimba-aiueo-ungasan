<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%tb_spp}}".
 *
 * @property string $id_spp
 * @property string|null $nim_murid
 * @property int|null $tahun
 * @property int|null $bulan
 * @property float|null $jumlah
 * @property int|null $id_pembayaran
 * @property int|null $id_user
 *
 * @property-read mixed $murid
 * @property-read mixed $namaBulan
 * @property TbBayarSpp $pembayaran
 * @property int $lunas [int(11)]
 * @property string $jatuh_tempo [date]
 * @property User $user
 * @property int $id_jurnal_piutang [int(11)]
 * @property Jurnal $jurnalPiutang
 */
class TbSpp extends \yii\db\ActiveRecord
{
    const SCENARIO_INSERT='insert';
    public $isLunas;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tb_spp}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            [['id_spp'], 'required'],
            [['tahun', 'bulan', 'id_pembayaran', 'id_user','isLunas','lunas','id_jurnal_piutang'], 'integer'],
            [['id_spp'], 'autonumber', 'format' => function(){
                $tBulan = sprintf("%02d", $this->bulan);
                $tahun = $this->tahun;
                return "SPP/{$tahun}{$tBulan}/?";
            }, 'digit' => 4,'on' => self::SCENARIO_INSERT],
            [['jatuh_tempo'], 'safe'],
            [['jumlah'], 'number'],
            [['id_spp'], 'string', 'max' => 255],
            [['nim_murid'], 'string', 'max' => 100],
            [['id_spp'], 'unique'],
            [['lunas'], 'default','value' => 0],
            [['id_pembayaran'], 'exist', 'skipOnError' => true, 'targetClass' => TbBayarSpp::className(), 'targetAttribute' => ['id_pembayaran' => 'id_pembayaran']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_spp' => Yii::t('app', 'Id Spp'),
            'nim_murid' => Yii::t('app', 'Nim Murid'),
            'tahun' => Yii::t('app', 'Tahun'),
            'jatuh_tempo' => Yii::t('app', 'Jatuh Tempo'),
            'bulan' => Yii::t('app', 'Bulan'),
            'jumlah' => Yii::t('app', 'Jumlah'),
            'id_pembayaran' => Yii::t('app', 'Bukti Pembayaran'),
            'id_user' => Yii::t('app', 'Id User'),
        ];
    }

    public function getMurid()
    {
        return $this->hasOne(TbMurid::className(),['nim_murid' => 'nim_murid']);
    }

    public function getNamaBulan()
    {
        return ArrayHelper::getValue(getParams('bulan'),$this->bulan);
    }

    public static function getPiutangSpp()
    {
        $data = self::getDb()->createCommand("SELECT total FROM v_piutang_spp")->queryScalar();
        return $data?:0;
    }



    public static function getTotalTerbayar($tahun, $moth)
    {
        $command = Yii::$app->db->createCommand("SELECT SUM(jumlah) as jumlah FROM {{%tb_spp}} WHERE tahun = :tahunInput AND bulan= :bulanInput WHERE lunas=1");
        $command->bindParam(':tahunInput',$tahun);
        $command->bindParam(':bulanInput',$moth);
        $result = $command->queryScalar();
        return (float)$result;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(),['id'=> 'id_user']);
    }

    public function getJurnalPiutang()
    {
        return $this->hasOne(Jurnal::className(),['id' => 'id_jurnal_piutang']);
    }

    /**
     * Gets query for [[Pembayaran]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPembayaran()
    {
        return $this->hasOne(TbBayarSpp::className(), ['id_pembayaran' => 'id_pembayaran']);
    }

}
