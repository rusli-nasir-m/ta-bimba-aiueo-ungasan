<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\Tahunbuku]].
 *
 * @see \app\models\Tahunbuku
 */
class TahunbukuQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[aktif]]=1');
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Tahunbuku[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Tahunbuku|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
