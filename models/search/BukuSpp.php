<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TbSpp;

/**
 * BukuSpp represents the model behind the search form of `app\models\TbSpp`.
 */
class BukuSpp extends TbSpp
{

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_spp', 'nim_murid','jatuh_tempo'], 'safe'],
            [['tahun', 'bulan', 'id_pembayaran', 'id_user','lunas'], 'integer'],
            [['jumlah'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TbSpp::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tahun' => $this->tahun,
            'bulan' => $this->bulan,
            'jumlah' => $this->jumlah,
            'id_pembayaran' => $this->id_pembayaran,
            'id_user' => $this->id_user,
            'lunas' => $this->lunas,
            'jatuh_tempo' => $this->jatuh_tempo,
        ]);

        $query->andFilterWhere(['like', 'id_spp', $this->id_spp])
            ->andFilterWhere(['like', 'nim_murid', $this->nim_murid]);

        return $dataProvider;
    }
}
