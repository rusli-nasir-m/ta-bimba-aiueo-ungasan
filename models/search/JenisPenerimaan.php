<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Datapenerimaan;

/**
 * JenisPenerimaan represents the model behind the search form of `app\models\Datapenerimaan`.
 */
class JenisPenerimaan extends Datapenerimaan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['replid', 'aktif'], 'integer'],
            [['nama', 'idkategori', 'rekkas', 'rekpendapatan', 'rekpiutang', 'keterangan', 'departemen', 'info1', 'info2', 'info3'], 'safe'],
            [['besar'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Datapenerimaan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'replid' => $this->replid,
            'besar' => $this->besar,
            'aktif' => $this->aktif,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'idkategori', $this->idkategori])
            ->andFilterWhere(['like', 'rekkas', $this->rekkas])
            ->andFilterWhere(['like', 'rekpendapatan', $this->rekpendapatan])
            ->andFilterWhere(['like', 'rekpiutang', $this->rekpiutang])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'departemen', $this->departemen])
            ->andFilterWhere(['like', 'info1', $this->info1])
            ->andFilterWhere(['like', 'info2', $this->info2])
            ->andFilterWhere(['like', 'info3', $this->info3]);

        return $dataProvider;
    }
}
