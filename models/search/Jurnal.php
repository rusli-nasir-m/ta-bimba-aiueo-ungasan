<?php

namespace app\models\search;

use app\models\JurnalDetail;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Jurnal as JurnalModel;

/**
 * Jurnal represents the model behind the search form of `app\models\Jurnal`.
 */
class Jurnal extends JurnalModel
{

    public $tanggalAwal;
    public $tanggalAkhir;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'f_id', 'login_id', 'tahunbuku_id'], 'integer'],
            [['no', 'tgl', 'keterangan', 'waktu_post', 'sumberjurnal','tanggalAwal','tanggalAkhir'], 'safe'],
            [['total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JurnalModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'login_id' => $this->login_id,
            'waktu_post' => $this->waktu_post,
            'tahunbuku_id' => $this->tahunbuku_id,
            'total' => $this->total,
        ]);

        $query->andFilterWhere(['BETWEEN','tgl',$this->tanggalAwal,$this->tanggalAkhir]);

        $query->andFilterWhere(['like', 'no', $this->no])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'sumberjurnal', $this->sumberjurnal]);

        $query->orderBy(['tgl' => SORT_DESC]);
        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchJurnalDetail($params)
    {
        $query = JurnalDetail::find()->joinWith('jurnal');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            self::tableName().'.login_id' => $this->login_id,
            self::tableName().'.waktu_post' => $this->waktu_post,
            self::tableName().'.tahunbuku_id' => $this->tahunbuku_id,
            self::tableName().'.total' => $this->total,
        ]);

        $query->andFilterWhere(['BETWEEN',self::tableName().'.tgl',$this->tanggalAwal,$this->tanggalAkhir]);

        $query->andFilterWhere(['like', self::tableName().'.no', $this->no])
            ->andFilterWhere(['like', self::tableName().'.keterangan', $this->keterangan])
            ->andFilterWhere(['like', self::tableName().'.sumberjurnal', $this->sumberjurnal]);
        $query->orderBy([self::tableName().'.tgl' => SORT_ASC,self::tableName().'.no' => SORT_ASC]);

        return $dataProvider;
    }
}
