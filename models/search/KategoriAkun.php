<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KategoriAkun as KategoriAkunModel;

/**
 * KategoriAkun represents the model behind the search form of `app\models\KategoriAkun`.
 */
class KategoriAkun extends KategoriAkunModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kategori', 'kelompok_akun_id'], 'integer'],
            [['kategori_akun'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KategoriAkunModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_kategori' => $this->id_kategori,
            'kelompok_akun_id' => $this->kelompok_akun_id,
        ]);

        $query->andFilterWhere(['like', 'kategori_akun', $this->kategori_akun]);

        return $dataProvider;
    }
}
