<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TbKasMasuk as TbKasMasukModel;

/**
 * TbKasMasuk represents the model behind the search form of `app\models\TbKasMasuk`.
 */
class TbKasMasuk extends TbKasMasukModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kas_masuk', 'kode_penerimaan', 'tanggal_penerimaan', 'created_at', 'updated_at', 'sumber_penerimaan', 'nim_murid', 'cara_bayar', 'bank', 'reff_no'], 'safe'],
            [['jumlah'], 'number'],
            [['id_jurnal', 'jenis_permohonan'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TbKasMasukModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tanggal_penerimaan' => $this->tanggal_penerimaan,
            'jumlah' => $this->jumlah,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'id_jurnal' => $this->id_jurnal,
            'jenis_permohonan' => $this->jenis_permohonan,
        ]);

        $query->andFilterWhere(['like', 'id_kas_masuk', $this->id_kas_masuk])
            ->andFilterWhere(['like', 'kode_penerimaan', $this->kode_penerimaan])
            ->andFilterWhere(['like', 'sumber_penerimaan', $this->sumber_penerimaan])
            ->andFilterWhere(['like', 'nim_murid', $this->nim_murid])
            ->andFilterWhere(['like', 'cara_bayar', $this->cara_bayar])
            ->andFilterWhere(['like', 'bank', $this->bank])
            ->andFilterWhere(['like', 'reff_no', $this->reff_no]);

        return $dataProvider;
    }
}
