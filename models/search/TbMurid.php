<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TbMurid as TbMuridModel;

/**
 * TbMurid represents the model behind the search form of `app\models\TbMurid`.
 */
class TbMurid extends TbMuridModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nim_murid', 'nama', 'kode_kelas', 'tanggal_lahir', 'alamat', 'no_telp', 'asal_sekolah', 'tgl_masuk', 'created_at', 'updated_at','status','agama'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TbMuridModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tanggal_lahir' => $this->tanggal_lahir,
            'tgl_masuk' => $this->tgl_masuk,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'status' => $this->status,
            'agama' => $this->agama,
        ]);

        $query->andFilterWhere(['like', 'nim_murid', $this->nim_murid])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'kode_kelas', $this->kode_kelas])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'no_telp', $this->no_telp])
            ->andFilterWhere(['like', 'asal_sekolah', $this->asal_sekolah]);

        return $dataProvider;
    }
}
