<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TbPengajuan as TbPengajuanModel;

/**
 * TbPengajuan represents the model behind the search form of `app\models\TbPengajuan`.
 */
class TbPengajuan extends TbPengajuanModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pengajuan', 'id_pegawai', 'jenis_pengajuan', 'kegiatan', 'validasi_tanggal'], 'safe'],
            [['id_jurnal', 'validasi_pengejuan', 'validasi_oleh'], 'integer'],
            [['total_pengajuan'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TbPengajuanModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_jurnal' => $this->id_jurnal,
            'total_pengajuan' => $this->total_pengajuan,
            'validasi_pengejuan' => $this->validasi_pengejuan,
            'validasi_tanggal' => $this->validasi_tanggal,
            'validasi_oleh' => $this->validasi_oleh,
        ]);

        $query->andFilterWhere(['like', 'id_pengajuan', $this->id_pengajuan])
            ->andFilterWhere(['like', 'id_pegawai', $this->id_pegawai])
            ->andFilterWhere(['like', 'jenis_pengajuan', $this->jenis_pengajuan])
            ->andFilterWhere(['like', 'kegiatan', $this->kegiatan]);

        return $dataProvider;
    }
}
