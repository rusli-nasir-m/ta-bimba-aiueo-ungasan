<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TbPengeluaran as TbPengeluaranModel;

/**
 * TbPengeluaran represents the model behind the search form of `app\models\TbPengeluaran`.
 */
class TbPengeluaran extends TbPengeluaranModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pengeluaran', 'id_pengajuan', 'jenis_pengeluaran', 'tanggal_realisasi', 'keperluan', 'keterangan'], 'safe'],
            [['id_user', 'jumlah_pengeluaran', 'id_jurnal'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TbPengeluaranModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_user' => $this->id_user,
            'tanggal_realisasi' => $this->tanggal_realisasi,
            'jumlah_pengeluaran' => $this->jumlah_pengeluaran,
            'id_jurnal' => $this->id_jurnal,
        ]);

        $query->andFilterWhere(['like', 'id_pengeluaran', $this->id_pengeluaran])
            ->andFilterWhere(['like', 'id_pengajuan', $this->id_pengajuan])
            ->andFilterWhere(['like', 'jenis_pengeluaran', $this->jenis_pengeluaran])
            ->andFilterWhere(['like', 'keperluan', $this->keperluan])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}
