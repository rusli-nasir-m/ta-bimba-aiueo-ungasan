<?php


namespace app\models\transaksi;


use yii\base\Model;
use yii\data\SqlDataProvider;

class Transaksi extends Model
{

    public $tanggalTransaksi;
    public $jenisTransaksi;

    public function rules()
    {
        return [
            [['tanggalTransaksi'],'safe'],
            [['jenisTransaksi'],'safe']
        ];
    }

    public function search($params)
    {
        $this->load($params);


        $dataProvider = new SqlDataProvider([
            'sql' => $query,
            'totalCount' => $totalCount,
        ]);
    }

}