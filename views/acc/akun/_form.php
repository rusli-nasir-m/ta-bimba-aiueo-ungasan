<?php

use app\models\KelompokAkun;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Akun */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akun-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'kode')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'kategori_akun_id')->dropDownList(\app\models\KategoriAkun::selectOptions()) ?>
    <?='' //$form->field($model, 'pajak')->textInput() ?>
    <?= $form->field($model, 'saldo_awal')->textInput() ?>
    <?= $form->field($model, 'saldo')->textInput(['disabled' => !$model->isNewRecord]) ?>
    <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
