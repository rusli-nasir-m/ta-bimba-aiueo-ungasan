<?php

use app\models\Akun;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/** @var $this \yii\web\View **/
/** @var $dataProvider \yii\data\ActiveDataProvider **/

$this->title = 'Saldo Awal';
$title = $this->title;

if($dataProvider) {
    ?>

    <?php
}
?>
<div class="tb-pengeluaran-form">
    <div class="panel panel-primary">
        <div class="panel-heading"><?=  Html::encode($title) ?></div>
        <?php $form = ActiveForm::begin([
             'id' => 'form-saldo-awal'
        ]); ?>
        <div class="panel-body">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Simpan'), ['class' => 'btn btn-success']) ?>
            </div>
            <p class="text-info">** Saldo Awal yang diinput adalah saldo setelah penutupan. Maka akun pada kelompok Pendapatan dan Beban harus 0</p>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'showFooter' => true,
                'tableOptions' => [
                    'id' => 'daftar-akun',
                    'class' =>'table table-bordered'
                ],
                'columns' => [
                    'kode',
                    'nama',
                    [
                        'label' => 'Kelompok akun',
                        'value' => function($model, $key, $index, $column){
                            /** @var $model Akun */
                            $kelompok_akun = $model->kategoriAkun->kelompokAkun->nama;
                            return $kelompok_akun;
                        }
                    ],
                    [
                        'attribute' => 'debit',
                        'format' => 'raw',
                        'value' => function($model, $key, $index, $column) use($form){
                            /** @var $model Akun */
                            $kelompok_akun = $model->kategoriAkun->kelompok_akun_id;
                            $options['id'] = 'debit' . $model->id ;
//                            $options['disabled'] = TRUE;
                            $options['onKeyup'] = "this.value=this.value.replace(/[^\d]/,'')";
                            $options['data-akun'] = 'debit';
                            $model->debit = 0;
                            $input =[];
                            $input[]= Html::activeHiddenInput($model,"[{$key}]id");
                            if($kelompok_akun != 4 && $kelompok_akun != 5){
                                if ($model->saldo_awal > 0)
                                {
                                    $model->debit = $model->saldo_awal;
                                    $options['disabled'] = false;
                                }
                                $input[] = $form->field($model,"[{$key}]debit")->textInput($options)->label(false);
                                return implode('',$input);
                            }else{
                                return implode('',$input);
                            }
                        },
                        'footerOptions' => ['id' => 'total_debit','class' => 'text-right']
                    ],
                    [
                        'attribute' => 'kredit',
                        'format' => 'raw',
                        'value' => function($model, $key, $index, $column) use($form){
                            /** @var $model Akun */
                            $kelompok_akun = $model->kategoriAkun->kelompok_akun_id;
                            $options['id'] = 'kredit' . $model->id ;
//                            $options['disabled'] = TRUE;
                            $options['data-akun'] = 'kredit';
                            $options['onKeyup'] = "this.value=this.value.replace(/[^\d]/,'')";
                            $model->kredit = 0;
                            if($kelompok_akun != 4 && $kelompok_akun != 5){
                                if ($model->saldo_awal < 0)
                                {
                                    $model->kredit = -($model->saldo_awal);
                                    $options['disabled'] = false;
                                }
                                return $form->field($model,"[{$key}]kredit")->textInput($options)->label(false);
                            }else{
                                return '';
                            }
                        },
                        'footerOptions' => ['id' => 'total_kredit','class' => 'text-right']
                    ],
                ]
            ])?>

        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Simpan'), ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php $form::end()?>
    </div>
</div>
<?php
$js = <<<JS
var tbl = $('#daftar-akun');
kalkulasiAkun();

tbl.on('blur',':input[data-akun="debit"],:input[data-akun="kredit"]',function() {
  switch (dataakun = $(this).data('akun')){
      case 'debit':
          cekDebit(this);
          break;
      case 'kredit':
          cekKredit(this);
          break
  }
  kalkulasiAkun();
})

    function kalkulasiAkun(){
        var debitSum = 0;
        var kreditSum = 0;
        
        $('#daftar-akun > tbody > tr').each(function (index){
            var tr = $(this);
            var debit = tr.find('[data-akun="debit"]').val()||0
            var kredit = tr.find('[data-akun="kredit"]').val()||0
            debitSum += parseInt(debit);
            kreditSum += parseInt(kredit);
            
            cekDebit(tr.find('[data-akun="debit"]'));
            cekKredit(tr.find('[data-akun="kredit"]'));
        });
        $('#total_debit').html(Number(debitSum).toLocaleString('ID'));
        $('#total_kredit').html(Number(kreditSum).toLocaleString('ID'));
    }

    function cekAkun()
    {
        var lastRow = tbl.find("tr").length;
        var debitSum = 0;
        var kreditSum = 0;

        $('#daftar-akun > tbody > tr').each(function (index){
            var tr = $(this);
            var debit = tr.find('[data-akun="debit"]').val()||0
            var kredit = tr.find('[data-akun="kredit"]').val()||0
            debitSum += parseInt(debit);
            kreditSum += parseInt(kredit);
        });
        
        if(debitSum !== kreditSum) {
            alert("Jumlah debit harus sama dengan jumlah kredit.");
            return false;
        } else {
            return true;
        }
    }
    
    function cekDebit(elm) {
        var tr = $(elm).closest('tr');
        
        var debit = tr.find('[data-akun="debit"]');
        var kredit = tr.find('[data-akun="kredit"]');
         
        if (debit.val() != '' && debit.val() != '0' ) {
            kredit.prop("disabled", true);
        } else {
            kredit.prop("disabled",false);
        }
        
    }

    function cekKredit(elm) {
        var tr = $(elm).closest('tr');
        
        var debit = tr.find('[data-akun="debit"]');
        var kredit = tr.find('[data-akun="kredit"]');
        
        if (kredit.val() != '' && kredit.val() != '0' ) {
            debit.prop("disabled", true);
        } else {
            debit.prop("disabled",false);
        }
    }
    
    $('#form-saldo-awal').on('beforeSubmit',function(e) {
      return cekAkun();
    })
    
JS;
$this->registerJs($js);
?>