<?php



/* @var $this \yii\web\View */

use yii\bootstrap\Html;


$this->title = Yii::t('app', 'Pengeluaran');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="biaya-index">
    <div class="panel panel-primary">
        <div class="panel-heading"><?= Html::encode($this->title) ?></div>
        <div class="panel-body">
            <?= $this->render('sub/panel_pengeluaran')?>
            <?= $this->render('sub/daftar_pengeluaran')?>
        </div>
    </div>
</div>
