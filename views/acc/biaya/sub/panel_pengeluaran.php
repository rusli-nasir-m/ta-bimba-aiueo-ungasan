<?php



/* @var $this \yii\web\View */

?>
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">Total Biaya Bulan Ini</div>
            <div class="panel-body">
                <p>Total</p>
                <p>
                <?= Yii::$app->formatter->asCurrency(0,'IDR')?>
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">Total Biaya 30 Hari Terakhir</div>
            <div class="panel-body">
                <p>Total</p>
                <p>
                    <?= Yii::$app->formatter->asCurrency(0,'IDR')?>
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">Biaya Belum Dibayar</div>
            <div class="panel-body">
                <p>Total</p>
                <p>
                    <?= Yii::$app->formatter->asCurrency(0,'IDR')?>
                </p>
            </div>
        </div>
    </div>
</div>
