<?php

use app\models\Kategoripenerimaan;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Datapenerimaan */
/* @var $form yii\widgets\ActiveForm */

$listAkunAktiva = ArrayHelper::map(Yii::$app->db->createCommand("SELECT kode,CONCAT(kode,'~',nama ) as nama FROM v_tb_akun WHERE kelompok_akun_id = 1")->queryAll(),'kode','nama');
$listAkunKewajiban = ArrayHelper::map(Yii::$app->db->createCommand("SELECT kode,CONCAT(kode,'~',nama ) as nama FROM v_tb_akun WHERE kelompok_akun_id = 2")->queryAll(),'kode','nama');
$listAkunModal = ArrayHelper::map(Yii::$app->db->createCommand("SELECT kode,CONCAT(kode,'~',nama ) as nama FROM v_tb_akun WHERE kelompok_akun_id = 3")->queryAll(),'kode','nama');
$listAkunPendapatan = ArrayHelper::map(Yii::$app->db->createCommand("SELECT kode,CONCAT(kode,'~',nama ) as nama FROM v_tb_akun WHERE kelompok_akun_id = 4")->queryAll(),'kode','nama');
$listAkunBiaya = ArrayHelper::map(Yii::$app->db->createCommand("SELECT kode,CONCAT(kode,'~',nama ) as nama FROM v_tb_akun WHERE kelompok_akun_id = 5")->queryAll(),'kode','nama');

?>

<div class="datapenerimaan-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'idkategori')->dropDownList(ArrayHelper::map(Kategoripenerimaan::find()->orderBy(['urutan' => SORT_ASC])->all(),'kode','kategori')) ?>
    <?= $form->field($model, 'rekkas')->dropDownList($listAkunAktiva,['prompt' => '--Pilih Rekening--']) ?>
    <?= $form->field($model, 'rekpendapatan')->dropDownList($listAkunPendapatan,['prompt' => '--Pilih Rekening--']) ?>
    <?= $form->field($model, 'rekpiutang')->dropDownList($listAkunModal,['prompt' => '--Pilih Rekening--']) ?>
    <?= $form->field($model, 'info1')->dropDownList($listAkunPendapatan,['prompt' => '--Pilih Rekening--'])->label('Rekening Diskon') ?>
    <?= $form->field($model, 'keterangan')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'aktif')->dropDownList([1 => 'Aktif', 0=> 'Non Aktif']) ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
