<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\JenisPenerimaan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="datapenerimaan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'replid') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'besar') ?>

    <?= $form->field($model, 'idkategori') ?>

    <?= $form->field($model, 'rekkas') ?>

    <?php // echo $form->field($model, 'rekpendapatan') ?>

    <?php // echo $form->field($model, 'rekpiutang') ?>

    <?php // echo $form->field($model, 'aktif') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <?php // echo $form->field($model, 'departemen') ?>

    <?php // echo $form->field($model, 'info1') ?>

    <?php // echo $form->field($model, 'info2') ?>

    <?php // echo $form->field($model, 'info3') ?>

    <?php // echo $form->field($model, 'ts') ?>

    <?php // echo $form->field($model, 'token') ?>

    <?php // echo $form->field($model, 'issync') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
