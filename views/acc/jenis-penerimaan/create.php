<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Datapenerimaan */

$this->title = Yii::t('app', 'Create Jenis Penerimaan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Jenis Penerimaan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="datapenerimaan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
