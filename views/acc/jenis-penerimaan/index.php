<?php

use app\models\Kategoripenerimaan;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\JenisPenerimaan */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Jenis Penerimaan');
$this->params['breadcrumbs'][] = $this->title;

$listAkunAktiva = ArrayHelper::map(Yii::$app->db->createCommand("SELECT kode, CONCAT(kode,'~',nama ) as nama FROM v_tb_akun WHERE kelompok_akun_id = 1")->queryAll(),'kode','nama');
$listAkunKewajiban = ArrayHelper::map(Yii::$app->db->createCommand("SELECT kode,CONCAT(kode,'~',nama ) as nama FROM v_tb_akun WHERE kelompok_akun_id = 2")->queryAll(),'kode','nama');
$listAkunModal = ArrayHelper::map(Yii::$app->db->createCommand("SELECT kode,CONCAT(kode,'~',nama ) as nama FROM v_tb_akun WHERE kelompok_akun_id = 3")->queryAll(),'kode','nama');
$listAkunPendapatan = ArrayHelper::map(Yii::$app->db->createCommand("SELECT kode,CONCAT(kode,'~',nama ) as nama FROM v_tb_akun WHERE kelompok_akun_id = 4")->queryAll(),'kode','nama');
$listAkunBiaya = ArrayHelper::map(Yii::$app->db->createCommand("SELECT kode,CONCAT(kode,'~',nama ) as nama FROM v_tb_akun WHERE kelompok_akun_id = 5")->queryAll(),'kode','nama');
?>
<div class="datapenerimaan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Datapenerimaan'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nama',
//            [
//                'label' => 'Kode Rekening',
//                'format' => 'raw',
//                'value' => function($model) use ($listAkunAktiva, $listAkunPendapatan, $listAkunModal){
//                    $rekKas = isset($listAkunAktiva[$model->rekkas])?$listAkunAktiva[$model->rekkas]:null;
//                    $rekPendapatan = isset($listAkunPendapatan[$model->rekpendapatan])?$listAkunPendapatan[$model->rekpendapatan]:null;
//                    $rekPiutang = isset($listAkunModal[$model->rekpiutang])?$listAkunModal[$model->rekpiutang]:null;
//                    $rekDiskon = isset($listAkunPendapatan[$model->info1])?$listAkunPendapatan[$model->info1]:null;
//                    return strtr("")
//                }
//            ],
            [
                'attribute' => 'idkategori',
                'filter' => ArrayHelper::map(Kategoripenerimaan::find()->orderBy(['urutan' => SORT_ASC])->all(),'kode','kategori')
            ],
            [
                 'attribute' => 'rekkas',
                'value' => function($model) use ($listAkunAktiva) {
                    return isset($listAkunAktiva[$model->rekkas])?$listAkunAktiva[$model->rekkas]:null;
                },
                'filter' => $listAkunAktiva,
            ],
            [
                'attribute' => 'rekpendapatan',
                'value' => function($model) use ($listAkunPendapatan) {
                    return isset($listAkunPendapatan[$model->rekpendapatan])?$listAkunPendapatan[$model->rekpendapatan]:null;
                },
                'filter' => $listAkunPendapatan,
            ],
            [
                'attribute' => 'rekpiutang',
                'value' => function($model) use ($listAkunModal) {
                    return isset($listAkunModal[$model->rekpiutang])?$listAkunModal[$model->rekpiutang]:null;
                },
                'filter' => $listAkunModal,
            ],
            [
                'attribute' => 'info1',
                'value' => function($model) use ($listAkunPendapatan) {
                    return isset($listAkunPendapatan[$model->info1])?$listAkunPendapatan[$model->info1]:null;
                },
                'filter' => $listAkunPendapatan,
            ],
            //'aktif',
            'keterangan',
            //'departemen',
            //'info1',
            //'info2',
            //'info3',
            //'ts',
            //'token',
            //'issync',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
