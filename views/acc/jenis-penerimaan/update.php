<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Datapenerimaan */

$this->title = Yii::t('app', 'Update Jenis Penerimaan: {name}', [
    'name' => $model->nama,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Datapenerimaans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->replid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="datapenerimaan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
