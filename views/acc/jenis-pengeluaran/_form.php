<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Datapengeluaran */
/* @var $form yii\widgets\ActiveForm */
$title = $actionName;
$listAkunAktiva = \yii\helpers\ArrayHelper::map(Yii::$app->db->createCommand("SELECT kode,CONCAT(kode,'~',nama ) as nama, nama_kategori_akun FROM v_tb_akun GROUP BY id ORDER BY nama_kategori_akun")->queryAll(),'kode','nama','nama_kategori_akun');

?>

<div class="datapengeluaran-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true,'disabled' => $model->aktif== 2]) ?>

    <?= $form->field($model, 'rekdebet')->dropDownList($listAkunAktiva) ?>

    <?= $form->field($model, 'rekkredit')->dropDownList($listAkunAktiva) ?>

    <?= $form->field($model, 'aktif')->dropDownList(getParams('status_jensi_pengeluaran'),['disabled' => $model->aktif ==2]) ?>

    <?= $form->field($model, 'keterangan')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
