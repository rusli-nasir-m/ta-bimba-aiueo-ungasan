<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Datapengeluaran */
$actionName = Yii::t('app', 'Tambah Jenis Pengeluaran');
$this->title = Yii::$app->name . ' ' . $actionName;
$this->title = Yii::t('app', 'Tambah Jenis Pengeluaran');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Jenis Pengeluaran'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="datapengeluaran-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'actionName' => $actionName,
    ]) ?>

</div>
