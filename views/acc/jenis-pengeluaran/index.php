<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\JenisPengeluaran */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Jenis Pengeluaran');
$this->params['breadcrumbs'][] = $this->title;
$listAkunAktiva = \yii\helpers\ArrayHelper::map(Yii::$app->db->createCommand("SELECT id,CONCAT(kode,'~',nama ) as nama, nama_kategori_akun FROM v_tb_akun GROUP BY id ORDER BY nama_kategori_akun")->queryAll(),'id','nama','nama_kategori_akun');

?>
<div class="datapengeluaran-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Tambah Jenis Pengeluaran'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nama',
            [
                    'attribute' => 'rekdebet',
                'value' => function($model){
                    $rekDebitNama = $model->rekdebet? $model->rekdebet0->nama:null;
                    return $model->rekdebet .  ' - ' .  $rekDebitNama;
                },
                'filter' => $listAkunAktiva
            ],
            [
                'attribute' => 'rekkredit',
                'value' => function($model){
                    $rekKreditNama = $model->rekkredit? $model->rekkredit0->nama:null;
                    return $model->rekkredit .  ' - ' .  $rekKreditNama;
                },
                'filter' =>$listAkunAktiva
            ],
            'keterangan',
            [
                 'attribute' => 'aktif',
                'value' => function($model) {
                    return ArrayHelper::getValue(getParams('status_jensi_pengeluaran'),$model->aktif);
                },
                'filter' => getParams('status_jensi_pengeluaran')
            ],

            [
                 'class' => 'yii\grid\ActionColumn'
            ],
        ],
    ]); ?>


</div>
