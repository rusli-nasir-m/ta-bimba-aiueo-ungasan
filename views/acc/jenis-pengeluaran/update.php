<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Datapengeluaran */
$actionName = Yii::t('app', 'Update Jenis Pengeluaran: {name}', [
    'name' => $model->nama,
]);
$this->title = Yii::$app->name . ' ' . Yii::t('app', 'Update Jenis Pengeluaran: {name}', [
        'name' => $model->nama,
    ]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Jenis Pengeluaran'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->replid, 'url' => ['view', 'id' => $model->replid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="datapengeluaran-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'actionName' => $actionName,
    ]) ?>

</div>
