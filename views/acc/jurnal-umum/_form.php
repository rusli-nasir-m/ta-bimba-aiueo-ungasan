<?php

use mdm\widgets\TabularInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Jurnal */
/* @var $form yii\widgets\ActiveForm */
$tahunBuku = \yii\helpers\ArrayHelper::map(\app\models\Tahunbuku::find()->orderBy(['aktif' => SORT_ASC])->all(),'id','tahunbuku');
?>
<div class="jurnal-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'tahunbuku_id')->dropDownList($tahunBuku) ?>
            <?= $form->field($model, 'no')->textInput(['maxlength' => true,'disabled'=> true,'placeholder' => '--Auto Generate--']) ?>
            <?= $form->field($model, 'tgl')->widget(\yii\jui\DatePicker::className(),[
                'dateFormat' => 'php:Y-m-d',
                'options' => ['class' => 'form-control']
            ]) ?>
            <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-6">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Rekening Akun</th>
                    <th>Debit</th>
                    <th>Kredit</th>
                    <th width="100px"><a id="btn-add"><span class="glyphicon glyphicon-plus"></span> Add</a></th>
                </tr>
                </thead>
                <?= TabularInput::widget([
                    'id' => 'detail-grid',
                    'allModels' => $model->jurnalDetails,
                    'model' => \app\models\JurnalDetail::className(),
                    'tag' => 'tbody',
                    'form' => $form,
                    'itemOptions' => ['tag' => 'tr'],
                    'itemView' => '_item_detail',
                    'clientOptions' => [
                        'btnAddSelector' => '#btn-add',
                    ]
                ]);
                ?>
                <tfoot>
                    <tr>
                        <th>Total</th>
                        <th id="total-debit">0</th>
                        <th id="total-kredit">0</th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php
$js = <<<JS

JS;
$this->registerJs($js);
?>