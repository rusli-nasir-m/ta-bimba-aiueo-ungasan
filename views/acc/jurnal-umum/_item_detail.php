<?php

/** @var \yii\bootstrap\ActiveForm  $form */
/** @var \app\models\JurnalDetail $model */
/** @var mixed $key */

use yii\helpers\Html;

$listAkunAktiva = \yii\helpers\ArrayHelper::map(Yii::$app->db->createCommand("SELECT id,CONCAT(kode,'~',nama ) as nama, nama_kategori_akun FROM v_tb_akun GROUP BY id ORDER BY nama_kategori_akun")->queryAll(),'id','nama','nama_kategori_akun');
?>

<td><?= $form->field($model,"[$key]akun_id")->dropDownList($listAkunAktiva)->label(false); ?></td>
<td><?= $form->field($model,"[$key]debit")->textInput(['data-akun' => 'debit'])->label(false); ?></td>
<td><?= $form->field($model,"[$key]kredit")->textInput(['data-akun' => 'kredit'])->label(false); ?></td>
<td>
    <?= Html::activeHiddenInput($model,"[$key]id")?>
    <a data-action="delete"><span class="glypicon glypicon-minus"></span></a>
</td>
