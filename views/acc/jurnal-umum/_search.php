<?php

use app\models\Kategoripenerimaan;
use app\models\Tahunbuku;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\Jurnal */
/* @var $form yii\widgets\ActiveForm */

$tahunBuku = ArrayHelper::map(Tahunbuku::find()->all(),'id','tahunbuku');
$kategoriPenerimaan = ArrayHelper::map(Kategoripenerimaan::find()->all(),'kode','kategori');
$listSumberJurnal = ArrayHelper::merge($kategoriPenerimaan,[
    'OUT' => 'Pengeluaran',
    'UMUM' => 'Jurnal Umum',
]);
?>

<div class="jurnal-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'no') ?>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-6">
                    <?php echo $form->field($model, 'sumberjurnal')->dropDownList($listSumberJurnal,['prompt' => '----']) ?>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-6"><?= $form->field($model, 'tanggalAwal')->widget(\yii\jui\DatePicker::className(),[
                        'dateFormat' => 'php:Y-m-d',
                        'options' => ['class' => 'form-control']
                    ]) ?>
                </div>
                <div class="col-md-6"><?= $form->field($model, 'tanggalAkhir')->widget(\yii\jui\DatePicker::className(),[
                        'dateFormat' => 'php:Y-m-d',
                        'options' => ['class' => 'form-control']
                    ]) ?></div>
            </div>
        </div>
    </div>

    <?php // echo $form->field($model, 'login_id') ?>

    <?php // echo $form->field($model, 'waktu_post') ?>

    <?php // echo $form->field($model, 'total') ?>



    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
        <?= Html::a(Yii::t('app', 'Print'),['print','Jurnal' => $model], ['class' => 'btn btn-info','target' => '_blank']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
