<?php

use app\models\Kategoripenerimaan;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Jurnal */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Jurnal');
$this->params['breadcrumbs'][] = $this->title;

$kategoriPenerimaan = ArrayHelper::map(Kategoripenerimaan::find()->all(),'kode','kategori');
$listSumberJurnal = ArrayHelper::merge($kategoriPenerimaan,[
    'OUT' => 'Pengeluaran',
    'UMUM' => 'Jurnal Umum',
]);

$css = <<<CSS
    .table-jurnal{
        width: 100%;
        border-collapse: collapse;
    }
    .table-jurnal td, .table-jurnal th {
      border: 1px solid #ddd;
      padding: 3px;
    }
    .keterangan-jurnal{
        font-weight: bold;
        padding: 4px 3px 4px 3px;
    }
CSS;
$this->registerCss($css);

?>
<div class="jurnal-index">
    <div class="panel panel-primary">
        <div class="panel-heading"><?=  Html::encode($this->title) ?></div>
        <div class="panel-body">
            <p>
                <?= Html::a(Yii::t('app', 'Tambah Jurnal'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= \yii\widgets\LinkPager::widget([
                'pagination' => $dataProvider->pagination
            ]) ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'afterRow' => function($model, $key, $index, $widget){
                    /** @var $model \app\models\Jurnal */
                    $template = "<tr>
               <td colspan='2'><b>petugas:</b> {$model->login_id}</td>
               <td colspan='4'>
                   <b class='keterangan-jurnal'>{$model->keterangan}</b>
                   <table class='table-jurnal'>
                        {items}
                   </table>
                </td>
            </tr>";
                    $jdet=[];
                    foreach ($model->jurnalDetails as $detail) {
                        $rekKOde = $detail->akun->kode;
                        $rek = $detail->akun->nama;
                        $debit = Yii::$app->formatter->asDecimal($detail->debit);
                        $kredit = Yii::$app->formatter->asDecimal($detail->kredit);
                        $jdet[]= "<tr>
                            <td>{$rekKOde}</td>
                            <td>{$rek}</td>
                            <td class='text-right'>{$debit}</td>
                            <td class='text-right'>{$kredit}</td>
                        </tr>";
                    };

                    return strtr($template,[
                        '{items}' => implode('',$jdet)
                    ]);
                },
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'no',
                        'format' => 'raw',
                        'contentOptions' => ['class' => 'text-center'],
                        'value' => function($model){
                            $tanggal = Yii::$app->formatter->asDate($model->tgl);
                            return "<b>{$model->no}</b><br><span>{$tanggal}</span>";
                        }
                    ],
                    [
                        'attribute' => 'tahunbuku_id',
                        'filter' => ArrayHelper::map(\app\models\Tahunbuku::find()->all(),'id','tahunbuku'),
                        'value' => function($model){
                            return $model->tahunbuku_id? $model->tahunBuku->tahunbuku:null;
                        }
                    ],
                    [
                        'attribute' => 'tgl',
                        'filter' => \yii\jui\DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'tgl',
                            'dateFormat' => 'php:Y-m-d',
                            'options' => ['class' => 'form-control']
                        ])
                    ],
                    [
                        'attribute' => 'sumberjurnal',
                        'filter' => $listSumberJurnal,
                        'value' => function($model){
                            return $model->sumberjurnal;
                        }
                    ],
                    //'waktu_post',
                    //'tahunbuku_id',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
