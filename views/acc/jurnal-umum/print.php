<?php

use app\models\search\Jurnal as JurnalSearch;

/** @var $this \yii\web\View */
/** @var $searchModel JurnalSearch */
/** @var $dataProvider \yii\data\ActiveDataProvider|\app\models\JurnalDetail */

$totalDebet  = 0;
$totalKredit  = 0;
?>
<div class="text-center">
    <h3>Jurnal Umun BIMBA - AIUEO unit Ungasan</h3>
    <p>Tanggal: <?= Yii::$app->formatter->asDate($searchModel->tanggalAwal)?> - <?= Yii::$app->formatter->asDate($searchModel->tanggalAkhir)?></p>
</div>
<div class="row">
    <div class="col-xs-12">
        <table width="100%" class="table table-bordered table-condensed">
            <thead>
            <tr>
                <th>No Jurnal</th>
                <th>Tanggal</th>
                <th>No Akun</th>
                <th>Akun</th>
                <th>Debit</th>
                <th>Kredit</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if($dataProvider->count){
                foreach ($dataProvider->models as $jd) {
                    /** @var \app\models\JurnalDetail $jd */
                    $totalDebet += $jd->debit;
                    $totalKredit += $jd->kredit;
                    ?>
                    <tr>
                        <td><?= $jd->jurnal->no?></td>
                        <td><?= Yii::$app->formatter->asDate($jd->jurnal->tgl)?></td>
                        <td><?= $jd->akun->kode?></td>
                        <td><?= $jd->akun->nama?></td>
                        <td class="text-right"><?= Yii::$app->formatter->asDecimal($jd->debit)?></td>
                        <td class="text-right"><?= Yii::$app->formatter->asDecimal($jd->kredit)?></td>
                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
            <tfoot>
            <tr>
                <th colspan="4" style="text-align: center">Total</th>
                <th class="text-right"><?= Yii::$app->formatter->asDecimal($totalDebet)?></th>
                <th class="text-right"><?= Yii::$app->formatter->asDecimal($totalKredit)?></th>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
