<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Jurnal */

$this->title = $model->no;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Jurnals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$dataProvider = new \yii\data\ActiveDataProvider([
     'query' => $model->getJurnalDetails(),
    'pagination' => false,
    'sort' => false
]);
?>
<div class="jurnal-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Index'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'no',
            'tgl',
            'keterangan:ntext',
            'tahunBuku.tahunbuku',
            'sumberjurnal',
        ],
    ]) ?>
    <div class="row">
        <div class="col-md-12">
            <?= \yii\grid\GridView::widget([
                 'dataProvider' => $dataProvider,
                'columns' => [
                     'akun.kode',
                     'akun.nama',
                     'debit:decimal',
                     'kredit:decimal',
                ]
            ])?>
        </div>
    </div>
</div>
