<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TbPengajuan */
/* @var $form yii\widgets\ActiveForm */

$title = $actionName;
?>

<div class="tb-pengajuan-form">
    <div class="panel panel-primary">
        <div class="panel-heading"><?=  Html::encode($title) ?></div>
        <?php $form = ActiveForm::begin(); ?>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'id_pengajuan')->textInput(['disabled' => true,'placeholder' => '--Auto Number--']) ?>
                    <?= $form->field($model, 'tanggal')->widget(\yii\jui\DatePicker::className(),[
                        'dateFormat' => 'php:Y-m-d',
                        'options' => ['class' => 'form-control']
                    ]) ?>
                    <?= $form->field($model, 'jenis_pengajuan')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'kegiatan')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'total_pengajuan')->textInput(['maxlength' => true]) ?>


                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
