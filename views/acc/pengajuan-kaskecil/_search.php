<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\TbPengajuan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tb-pengajuan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_pengajuan') ?>

    <?= $form->field($model, 'id_pegawai') ?>

    <?= $form->field($model, 'jenis_pengajuan') ?>

    <?= $form->field($model, 'kegiatan') ?>

    <?= $form->field($model, 'id_jurnal') ?>

    <?php // echo $form->field($model, 'total_pengajuan') ?>

    <?php // echo $form->field($model, 'validasi_pengejuan') ?>

    <?php // echo $form->field($model, 'validasi_tanggal') ?>

    <?php // echo $form->field($model, 'validasi_oleh') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
