<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TbPengajuan */
$actionName = Yii::t('app', 'Pengajuan Kas Kecil');
$this->title = Yii::$app->name . ' ' . $actionName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengajuan Kas Kecil'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-pengajuan-create">

    <?= $this->render('_form', [
        'model' => $model,
        'actionName' => $actionName
    ]) ?>

</div>
