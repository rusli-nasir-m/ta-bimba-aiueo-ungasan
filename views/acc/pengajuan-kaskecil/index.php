<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TbPengajuan */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Kas Kecil');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-pengajuan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Pengajuan Kas Kecil'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id_pengajuan',
            'tanggal',
            'jenis_pengajuan',
            'kegiatan',
            [
                 'attribute' => 'total_pengajuan',
                'format' => 'decimal',
                'contentOptions' => ['class'=> 'text-right' ]
            ],
            [
                 'attribute' => 'validasi_pengejuan',
                'value' => function ($model) {
                    switch ($model->validasi_pengejuan){
                        case 0:
                            return 'Belum di proses';
                            break;
                        case 1:
                            return 'Sudah di validasi';
                            break;
                        case 2:
                            return 'Ditolak';
                            break;
                        default:
                            return 'Belum di proses';
                            break;

                    }
                }
            ],
            'validasi_tanggal',
            'validasi_oleh',
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '100px'],
                'template' => '{view} {update} {validasi} {delete}',
                'buttons' => [
                    'validasi' => function ($url, $model, $key) {
                        if($model->validasi_pengejuan) return null;
                        $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-ok"]);
                        return Html::a($icon, $url, [
                            'title' => 'Validasi',
                            'aria-label' => 'Validasi',
                            'data-pjax' => '0',
                            'data-confirm' => 'Yakin akan di validasi?',
                            'data-method' => 'post',
                        ]);
                    }
                ]
            ],
        ],
    ]); ?>


</div>
