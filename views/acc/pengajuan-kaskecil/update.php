<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TbPengajuan */
$actionName = Yii::t('app', 'Update Pengajuan Kas Kecil: {name}', [
    'name' => $model->id_pengajuan,
]);
$this->title = Yii::$app->name . ' ' . Yii::t('app', 'Update Pengajuan Kas Kecil: {name}', [
    'name' => $model->id_pengajuan,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Update Pengajuan Kas Kecil'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_pengajuan, 'url' => ['view', 'id' => $model->id_pengajuan]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tb-pengajuan-update">

    <?= $this->render('_form', [
        'model' => $model,
        'actionName' => $actionName,
    ]) ?>

</div>
