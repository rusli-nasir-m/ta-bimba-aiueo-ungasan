<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TbPengajuan */

$this->title = $model->id_pengajuan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tb Pengajuan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tb-pengajuan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>

        <?php
        if(!$model->validasi_pengejuan){
        echo Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_pengajuan], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Validasi'), ['validasi', 'id' => $model->id_pengajuan], [
            'class' => 'btn btn-warning',
            'data-pjax' => '0',
            'data-confirm' => 'Yakin akan di validasi?',
            'data-method' => 'post',
        ])?>
        <?php
            echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id_pengajuan], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]);
        }
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_pengajuan',
//            'id_pegawai',
            'jenis_pengajuan',
            'kegiatan',
            'id_jurnal',
            'total_pengajuan',
            'validasi_pengejuan',
            'validasi_tanggal',
            'validasi_oleh',
        ],
    ]) ?>
    <div class="row">
        <div class="col-md-12">
            <h3>Jurnal</h3>
            <?php
            $jurnal = $model->jurnal;
            if($jurnal){
                echo DetailView::widget([
                     'model' => $jurnal,
                    'attributes' => [
                         'no',
                        'tgl',
                        'keterangan',
                    ]
                ]);
            }
            ?>
        </div>
    </div>
</div>
