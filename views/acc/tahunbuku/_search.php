<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\Tahunbuku */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tahunbuku-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tahunbuku') ?>

    <?= $form->field($model, 'awalan') ?>

    <?= $form->field($model, 'aktif') ?>

    <?= $form->field($model, 'keterangan') ?>

    <?php // echo $form->field($model, 'cacah') ?>

    <?php // echo $form->field($model, 'tanggalmulai') ?>

    <?php // echo $form->field($model, 'tanggalselesai') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
