<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tahunbuku */
$actionName = Yii::t('app', 'Buat Tahun Buku');
$this->title = Yii::$app->name . ' ' . $actionName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tahun Buku'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tahunbuku-create">

    <?= $this->render('_form', [
        'model' => $model,
        'actionName' => $actionName
    ]) ?>

</div>
