<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tahunbuku */
$actionName = Yii::t('app', 'Update Tahunbuku: {name}', [
    'name' => $model->id,
]);
$this->title = Yii::$app->name . ' ' . Yii::t('app', 'Update Tahunbuku: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tahunbuku'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tahunbuku-update">

    <?= $this->render('_form', [
        'model' => $model,
        'actionName' => $actionName,
    ]) ?>

</div>
