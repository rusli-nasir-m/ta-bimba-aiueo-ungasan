<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\GenerateSPP */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tb-spp-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tahun')->dropDownList(listTahun(date('Y',strtotime('-5 year')),date('Y'))) ?>

    <?= $form->field($model, 'bulan')->dropDownList(getParams('bulan')) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
