<?php


/** @var $model \app\models\TbSpp */

use yii\grid\GridView;
use yii\widgets\DetailView;

$jurnal = $model->jurnalPiutang;

?>
<?php
if($jurnal){
    $dataProvider = new \yii\data\ActiveDataProvider([
        'query' => $jurnal->getJurnalDetails(),
        'pagination' => false,
        'sort' => false
    ]);
?>
    <?= DetailView::widget([
        'model' => $jurnal,
        'attributes' => [
            'no',
            'tgl',
            'keterangan:ntext',
            'tahunBuku.tahunbuku',
            'sumberjurnal',
        ],
    ]) ?>
    <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    'akun.kode',
                    'akun.nama',
                    'debit:decimal',
                    'kredit:decimal',
                ]
            ])?>
        </div>
    </div>
    <?php
}
?>
