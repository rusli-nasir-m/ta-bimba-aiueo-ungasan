<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TbSpp */

$this->title = Yii::t('app', 'Kalkulasi Tagihan SPP');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tb Spps'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-spp-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
