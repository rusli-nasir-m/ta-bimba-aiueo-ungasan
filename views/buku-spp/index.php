<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\BukuSpp */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Buku SPP');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-spp-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Kalkukasi Tagihan SPP'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id_spp',
            [
                 'attribute' => 'nim_murid',
                'value' => function($model){
                    $muridNim = $model->nim_murid? $model->murid->nim_murid:null;
                    $muridNama = $model->nim_murid? $model->murid->nama:null;
                    return $muridNim . ' - ' . $muridNama;
                }
            ],
            [
                'attribute' => 'tahun',
                'filter' => listTahun(date('Y',strtotime('-5 year')),date('Y'))
            ],
            [
                'attribute' => 'bulan',
                'value' => 'namaBulan',
                'filter' => getParams('bulan')
            ],
            [
                'attribute' => 'jatuh_tempo',
                'format' => 'date',
                'filter' => DatePicker::widget([
                      'model' => $searchModel,
                    'attribute' => 'jatuh_tempo',
                    'dateFormat' => 'php:Y-m-d',
                    'options' => ['class' => 'form-control']
                ])
            ],
            [
                 'attribute' =>  'jumlah',
                'format' => 'decimal',
                'contentOptions' => ['class' => 'text-right']
            ],
            'user.username',
            [
                'attribute' => 'lunas',
                'value' => function($model){
                    return $model->lunas? 'Lunas':'Belum Lunas';
                },
                'filter' => [
                     0  => 'Belum Lunas',
                     1  => 'Lunas',
                ],
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
