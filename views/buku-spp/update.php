<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TbSpp */

$this->title = Yii::t('app', 'Update Tb Spp: {name}', [
    'name' => $model->id_spp,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tb Spps'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_spp, 'url' => ['view', 'id' => $model->id_spp]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tb-spp-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
