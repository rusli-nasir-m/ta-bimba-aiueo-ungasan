<?php

use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TbSpp */

$this->title = $model->id_spp;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tb Spps'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="tb-spp-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_spp], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id_spp], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_spp',
            'nim_murid',
            'tahun',
            'bulan',
            'jumlah',
            'id_pembayaran',
            'id_user',
        ],
    ]) ?>
    <div class="row">
        <div class="col-md-12">
            <?= Tabs::widget([
                   'items' => [
                       [
                           'label' => 'Jurnal Piutang',
                           'content' => $this->render('_vjpiutang',['model' => $model]),
                           'active' => true
                       ],
                       [
                           'label' => 'Jurnal Pembayaran',
                           'content' => $this->render('_vjpembayaran',['model' => $model]),
                       ],
                   ]
            ])?>
        </div>
    </div>
</div>
