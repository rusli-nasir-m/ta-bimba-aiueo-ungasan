<?php



/** @var $this \yii\web\View */
/** @var $model \app\models\LabaRugi */
/** @var $dataProvider \yii\data\SqlDataProvider */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\jui\DatePicker;

$this->title = 'Laba Rugi';
?>
<div class="laporan-perubahan-modal">
    <div class="panel panel-primary no-print">
        <div class="panel-body">
            <?php $form = ActiveForm::begin()?>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <h1><?= $this->title?></h1>
                            <?= ''//$form->field($model,'tahunBuku')->dropDownList($tahunBuku,['prompt' => '-- Tahun Buku--'])?>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6"><?= $form->field($model,'fromDate')->widget(DatePicker::className(),[
                                        'dateFormat' => 'php:Y-m-d',
                                        'options' => ['class' => 'form-control']
                                    ])->label('Dari Tanggal')?>
                                </div>
                                <div class="col-md-6"><?= $form->field($model,'toDate')->widget(DatePicker::className(),[
                                        'dateFormat' => 'php:Y-m-d',
                                        'options' => ['class' => 'form-control']
                                    ])->label('Sampai Tanggal')?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                        <?= Html::submitButton('Filter',['class' => 'btn btn-primary'])?>
                        <?= Html::a('Print',['print','LabaRugi' => $model],['class' => 'btn btn-info','target' => '_blank'])?>
                    </div>
                </div>
            </div>
            <?php $form::end()?>
        </div>
    </div>
    <div class="row print-report">
        <div class="col-md-12">
            <?= \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'showFooter' => true,
                'columns' => [
                    [
                        'attribute' => 'no',
                        'label' => 'No Transaksi',
                        'format' => 'text',
                        'footer' => 'Total',
                        'footerOptions' => [
                            'colspan' => 6,
                        ]
                    ],
                    [
                        'attribute' => 'tanggal_transaksi',
                        'format' => 'date',
                        'footerOptions' => ['style' => 'display: none;'],
                    ],
                    [
                        'attribute' => 'sumberjurnal',
                        'footerOptions' => ['style' => 'display: none;'],
                    ],
                    [
                        'attribute' => 'kode_akun',
                        'footerOptions' => ['style' => 'display: none;'],
                    ],
                    [
                        'attribute' => 'nama_akun',
                        'footerOptions' => ['style' => 'display: none;'],
                    ],
                    [
                        'attribute' => 'nama_kategori_akun',
                        'label' => 'Kategori Akun',
                        'format' => 'raw',
                        'value' => function($model){
                            return $model['nama_kategori_akun'] . '(' . $model['nama_kelompok_akun'] . ')';
                        },
                        'footerOptions' => ['style' => 'display: none;'],
                    ],
                    [
                        'attribute' => 'debit',
                        'format' => 'decimal',
                        'footer' => call_user_func(function ($provider){
                            $total=0;
                            foreach($provider as $item){
                                $total+=$item['debit'];
                            }
                            return Yii::$app->formatter->asDecimal($total);
                        },$dataProvider->models)
                    ],
                    [
                        'attribute' => 'kredit',
                        'format' => 'decimal',
                        'footer' => call_user_func(function ($provider){
                            $total=0;
                            foreach($provider as $item){
                                $total+=$item['kredit'];
                            }
                            return Yii::$app->formatter->asDecimal($total);
                        },$dataProvider->models)
                    ],
                ]
            ])?>
        </div>
    </div>
</div>
