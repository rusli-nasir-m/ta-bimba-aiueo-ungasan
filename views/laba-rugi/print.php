<?php



/* @var $this \yii\web\View */
/* @var $model \app\models\LabaRugi */
/* @var $dataProvider array */

use app\models\Tahunbuku;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

$tahunBuku = ArrayHelper::map(Tahunbuku::find()->all(),'id','tahunbuku');

$css = <<<CSS
.print-report{
    border: solid 1px gray;
    padding: 5px;
}
.report-header{
    padding-bottom: 2pt;
}
@media print {
    .print-report{
        border: none;
        padding: 0;
    }    
}

CSS;
$this->registerCss($css);
$kelompokakun = \app\models\KelompokAkun::findAll(['id' => [4,5]]);
$kategoriAkun = \app\models\KategoriAkun::findAll(['kelompok_akun_id' => [4,5]]);
$kategoriAkunKey = ArrayHelper::getColumn($kategoriAkun,'id_kategori');
$akun = \app\models\Akun::findAll(['kategori_akun_id' => $kategoriAkunKey]);

$listakun = ArrayHelper::index($akun,'id','kategori_akun_id');
$listKategoriAkun = ArrayHelper::map($kategoriAkun,'id_kategori','kategori_akun','kelompok_akun_id');

?>

<div class="laporan-perubahan-modal">
    <div class="row print-report">
        <div class="col-md-12">
            <h1 class="text-center report-header">Laporan Laba Rugi</h1>
            <p>Tanggal: <b><?= Yii::$app->formatter->asDate($model->fromDate,'long')?> s/d <b><?= Yii::$app->formatter->asDate($model->toDate,'long')?></b></p>
            <hr>
            <table class="table table-sm table-condensed table-bordered">
                <?php
                $subTotal3= [];
                foreach ($kelompokakun as $item) {
                    ?>
                    <tr>
                        <th colspan="5"><?= $item->nama?></th>
                    </tr>
                    <?php
                    $x1 = $listKategoriAkun[$item->id];
                    $subTotal2 = 0;
                    foreach ($x1 as $key => $val) {
                        ?>
                        <tr>
                            <td colspan="5" style="padding-left: 20px"><?=$val?></td>
                        </tr>
                        <?php
                        $x2 = $listakun[$key];
                        $dataAkun = isset($dataProvider[$key])?$dataProvider[$key]:null;
                        $subTotal1 = 0;
                        foreach ($x2 as $idx=> $list) {
                            $dataAkunData = isset($dataAkun[$idx])?$dataAkun[$idx]:null;
                            $tot = 0;
                            if($dataAkunData){
                                $tot = isset($dataAkunData['total'])?$dataAkunData['total']:0;
                                $subTotal1 += $tot;
                            }
                            ?>
                            <tr>
                                <td colspan="3" style="padding-left: 40px"><?=$list['kode']?> - <?=$list['nama']?></td>
                                <td></td>
                                <td class="text-right"><?=Yii::$app->formatter->asCurrency($tot,'IDR')?></td>
                            </tr>
                            <?php
                        }
                        $subTotal2 += $subTotal1;
                        ?>
                        <tr>
                            <td colspan="3" style="padding-left: 40px"><?=strtoupper('Sub Total ' . $val)?></td>
                            <td></td>
                            <td class="text-right"><?=Yii::$app->formatter->asCurrency($subTotal1,'IDR')?></td>
                        </tr>
                        <?php
                    }
                $subTotal3[$item->id] =isset($subTotal3[$item->id])?$subTotal3[$item->id] + $subTotal2:$subTotal2;
                    ?>
                    <tr>
                        <th colspan="4"><?= strtoupper('Total ' . $item->nama)?></th>
                        <td class="text-right"><?=Yii::$app->formatter->asCurrency($subTotal2,'IDR')?></td>
                    </tr>
                    <?php
                }
                ?>
                <tr>
                    <th colspan="4">LABA</th>
                    <td class="text-right"><?=Yii::$app->formatter->asCurrency($subTotal3[4] - $subTotal3[5],'IDR')?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<?php
$js = <<<JS
print();
JS;
$this->registerJs($js);
?>
