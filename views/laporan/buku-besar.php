<?php



/* @var $this \yii\web\View */
/* @var $model \app\models\BukuBesar */
/* @var $dataProvider \yii\data\SqlDataProvider */


use yii\grid\GridView;
use yii\grid\SerialColumn;

$tahunBuku = \yii\helpers\ArrayHelper::map(\app\models\Tahunbuku::find()->all(),'id','tahunbuku');
$this->title = 'Laporan Buku Besar';

$css = <<<CSS
.print-report{
    border: solid 1px gray;
    padding: 5px;
}
.report-header{
    padding-bottom: 2pt;
}
@media print {
    .print-report{
        border: none;
        padding: 0;
    }    
}

CSS;
$this->registerCss($css);
?>
<div class="laporan-perubahan-modal">
    <div class="panel panel-primary no-print">
        <div class="panel-body">
            <?php $form = \yii\bootstrap\ActiveForm::begin()?>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model,'tahunBuku')->dropDownList($tahunBuku,['prompt' => '-- Tahun Buku--'])?>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6"><?= $form->field($model,'fromDate')->widget(\yii\jui\DatePicker::className(),[
                                        'dateFormat' => 'php:Y-m-d',
                                        'options' => ['class' => 'form-control']
                                    ])?></div>
                                <div class="col-md-6"><?= $form->field($model,'toDate')->widget(\yii\jui\DatePicker::className(),[
                                        'dateFormat' => 'php:Y-m-d',
                                        'options' => ['class' => 'form-control']
                                    ])?></div>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                        <?= \yii\bootstrap\Html::submitButton('Filter',['class' => 'btn btn-primary'])?>
                    </div>
                </div>
            </div>
            <?php $form::end()?>
        </div>
    </div>
    <div class="row print-report">
        <div class="col-md-12">
            <h1 class="text-center report-header">Laporan Buku Besar</h1>
            <p>Tanggal: <b><?= Yii::$app->formatter->asDate($model->fromDate,'long')?> s/d <b><?= Yii::$app->formatter->asDate($model->toDate,'long')?></b></p>
            <hr>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'showFooter' => true,
                'columns' => [
                    ['class' => SerialColumn::className()],
                    'no',
                    'tgl:date:Tanggal',
                    'username:text:Petugas',
                    'keterangan:ntext:Transaksi',
                    [
                        'attribute' => 'debit',
                        'format' => 'decimal',
                        'contentOptions' => ['class' => 'text-right'],
                        'footerOptions' => ['class' => 'text-right'],
                        'footer' => call_user_func(function ($models){
                            $debits = 0;
                            foreach ($models as $model) {
                                $debits+=(float)$model['debit'];
                            }
                            return Yii::$app->formatter->asDecimal($debits);
                        },$dataProvider->getModels())
                    ],
                    [
                        'attribute' => 'kredit',
                        'format' => 'decimal',
                        'contentOptions' => ['class' => 'text-right'],
                        'footerOptions' => ['class' => 'text-right'],
                        'footer' => call_user_func(function ($models){
                            $debits = 0;
                            foreach ($models as $model) {
                                $debits+=(float)$model['kredit'];
                            }
                           return Yii::$app->formatter->asDecimal($debits);
                        },$dataProvider->getModels())
                    ],
                ],
            ])?>
        </div>
    </div>
</div>
