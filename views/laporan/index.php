<?php



/* @var $this \yii\web\View */

use yii\bootstrap\Html;
use yii\bootstrap\Tabs;

$actionName = Yii::t('app', 'Laporan');
$this->title = $actionName;

echo Tabs::widget([
    'items' => [
        [
            'label' => 'Laporan Akuntansi',
            'content' => $this->render('sub/lap_akuntansi',['title' => $this->title . ' Akuntansi']),
            'active' => true
        ],
        [
            'label' => 'Laporan Transaksi',
            'content' => $this->render('sub/lap_transaksi',[
                'title' => $this->title . ' Transaksi',

            ]),
        ],
//        [
//            'label' => 'Aset',
//            'content' => $this->render('sub/lap_aset',[
//                'title' => $this->title . ' Aset',
//
//            ]),
//        ],
    ]
]);
?>
