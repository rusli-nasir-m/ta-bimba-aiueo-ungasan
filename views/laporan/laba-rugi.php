<?php



/* @var $this \yii\web\View */
/* @var $model \app\models\search\Laporan */

use app\models\Tahunbuku;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

$tahunBuku = ArrayHelper::map(Tahunbuku::find()->all(),'id','tahunbuku');

$css = <<<CSS
.print-report{
    border: solid 1px gray;
    padding: 5px;
}
.report-header{
    padding-bottom: 2pt;
}
@media print {
    .print-report{
        border: none;
        padding: 0;
    }    
}

CSS;
$this->registerCss($css);
$kelompokakun = \app\models\KelompokAkun::findAll(['id' => [4,5]]);
$kategoriAkun = \app\models\KategoriAkun::findAll(['kelompok_akun_id' => [4,5]]);
$kategoriAkunKey = ArrayHelper::getColumn($kategoriAkun,'id_kategori');
$akun = \app\models\Akun::findAll(['kategori_akun_id' => $kategoriAkunKey]);

$listakun = ArrayHelper::index($akun,'id','kategori_akun_id');
$listKategoriAkun = ArrayHelper::map($kategoriAkun,'id_kategori','kategori_akun','kelompok_akun_id');

?>

<div class="laporan-perubahan-modal">
    <div class="panel panel-primary no-print">
        <div class="panel-body">
            <?php $form = ActiveForm::begin()?>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model,'tahunBuku')->dropDownList($tahunBuku,['prompt' => '-- Tahun Buku--'])?>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6"><?= $form->field($model,'dariTanggal')->widget(\yii\jui\DatePicker::className(),[
                                        'dateFormat' => 'php:Y-m-d',
                                        'options' => ['class' => 'form-control']
                                    ])?></div>
                                <div class="col-md-6"><?= $form->field($model,'sampaiTanggal')->widget(\yii\jui\DatePicker::className(),[
                                        'dateFormat' => 'php:Y-m-d',
                                        'options' => ['class' => 'form-control']
                                    ])?></div>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                        <?= \yii\bootstrap\Html::submitButton('Filter',['class' => 'btn btn-primary'])?>
                    </div>
                </div>
            </div>
            <?php $form::end()?>
        </div>
    </div>

    <div class="row print-report">
        <div class="col-md-12">
            <h1 class="text-center report-header">Laporan Laba Rugi</h1>
            <p>Tanggal: <b><?= Yii::$app->formatter->asDate($model->dariTanggal,'long')?> s/d <b><?= Yii::$app->formatter->asDate($model->sampaiTanggal,'long')?></b></p>
            <hr>
            <table class="table table-sm">
                <?php foreach ($kelompokakun as $item) {
                    ?>
                    <tr>
                        <th><?= $item->nama?></th>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php
                    $x1 = $listKategoriAkun[$item->id];
                    foreach ($x1 as $key => $val) {
                        ?>
                        <tr>
                            <th></th>
                            <td><?=$val?></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <?php
                        $x2 = $listakun[$key];
                        foreach ($x2 as $idx=> $list) {
                            ?>
                            <tr>
                                <th></th>
                                <th></th>
                                <td><?=$list['kode']?></td>
                                <td><?=$list['nama']?></td>
                                <td class="text-right"><?=Yii::$app->formatter->asCurrency(0,'IDR')?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        <tr>
                            <th></th>
                            <td><?=strtoupper('Sub Total ' . $val)?></td>
                            <td></td>
                            <td></td>
                            <td class="text-right"><?=Yii::$app->formatter->asCurrency(0,'IDR')?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr>
                        <th><?= strtoupper('Sub Total ' . $item->nama)?></th>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="text-right"><?=Yii::$app->formatter->asCurrency(0,'IDR')?></td>
                    </tr>
                    <?php
                }?>
                <tr>
                    <th>LABA</th>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-right"><?=Yii::$app->formatter->asCurrency(0,'IDR')?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
