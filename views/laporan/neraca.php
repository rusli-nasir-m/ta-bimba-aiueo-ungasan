<?php



/* @var $this \yii\web\View */
/* @var $model \app\models\search\Laporan */

use app\models\Tahunbuku;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

$tahunBuku = ArrayHelper::map(Tahunbuku::find()->all(),'id','tahunbuku');

$css = <<<CSS
.print-report{
    border: solid 1px gray;
    padding: 5px;
}
.report-header{
    padding-bottom: 2pt;
}
@media print {
    .print-report{
        border: none;
        padding: 0;
    }    
}

CSS;
$this->registerCss($css);
$aktiva = \app\models\KelompokAkun::findAll(['id' => [1]]);
$kewajiban = \app\models\KelompokAkun::findAll(['id' => [2]]);
$modal = \app\models\KelompokAkun::findAll(['id' => [3]]);

$kategoriAkunAktiva = \app\models\KategoriAkun::findAll(['kelompok_akun_id' => [1]]);
$kategoriAkunKewajiban = \app\models\KategoriAkun::findAll(['kelompok_akun_id' => [2]]);
$kategoriAkunModal = \app\models\KategoriAkun::findAll(['kelompok_akun_id' => [3]]);

//$kategoriAkunKey = ArrayHelper::getColumn($kategoriAkun,'id_kategori');
$akun = \app\models\Akun::find()->all();

$listakun = ArrayHelper::index($akun,'id','kategori_akun_id');
$listKategoriAkunaktiva = ArrayHelper::map($kategoriAkunAktiva,'id_kategori','kategori_akun','kelompok_akun_id');
$listKategoriAkunKewajiban = ArrayHelper::map($kategoriAkunKewajiban,'id_kategori','kategori_akun','kelompok_akun_id');
$listKategoriAkunModal = ArrayHelper::map($kategoriAkunModal,'id_kategori','kategori_akun','kelompok_akun_id');

?>

<div class="laporan-perubahan-modal">
    <div class="panel panel-primary no-print">
        <div class="panel-body">
            <?php $form = ActiveForm::begin()?>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model,'tahunBuku')->dropDownList($tahunBuku,['prompt' => '-- Tahun Buku--'])?>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6"><?= $form->field($model,'dariTanggal')->widget(\yii\jui\DatePicker::className(),[
                                        'dateFormat' => 'php:Y-m-d',
                                        'options' => ['class' => 'form-control']
                                    ])?></div>
                                <div class="col-md-6"><?= $form->field($model,'sampaiTanggal')->widget(\yii\jui\DatePicker::className(),[
                                        'dateFormat' => 'php:Y-m-d',
                                        'options' => ['class' => 'form-control']
                                    ])?></div>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                        <?= \yii\bootstrap\Html::submitButton('Filter',['class' => 'btn btn-primary'])?>
                    </div>
                </div>
            </div>
            <?php $form::end()?>
        </div>
    </div>

    <div class="row print-report">
        <div class="col-md-12">
            <h1 class="text-center report-header">Laporan Neraca</h1>
            <p>Tanggal: <b><?= Yii::$app->formatter->asDate($model->dariTanggal,'long')?> s/d <b><?= Yii::$app->formatter->asDate($model->sampaiTanggal,'long')?></b></p>
            <hr>
            <div class="row">
                <div class="col-xs-6" style="border-right: solid 1px gray;padding: 10px">
                    <h2>Harta</h2>
                    <table class="" width="100%">
                        <?php foreach ($aktiva as $item) {
                            ?>
                            <tr>
                                <th><?= $item->nama?></th>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php
                            $x1 = $listKategoriAkunaktiva[$item->id];
                            foreach ($x1 as $key => $val) {
                                ?>
                                <tr>
                                    <th></th>
                                    <td><?=$val?></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <?php
                                $x2 = $listakun[$key];
                                foreach ($x2 as $idx=> $list) {
                                    ?>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <td><?=$list['kode']?></td>
                                        <td><?=$list['nama']?></td>
                                        <td class="text-right"><?=Yii::$app->formatter->asCurrency(0,'IDR')?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                <tr>
                                    <th></th>
                                    <td><?=strtoupper('Sub Total ' . $val)?></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-right"><?=Yii::$app->formatter->asCurrency(0,'IDR')?></td>
                                </tr>
                                <?php
                            }

                        }?>
                        <tr>
                            <th>TOTAL HARTA</th>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="text-right"><?=Yii::$app->formatter->asCurrency(0,'IDR')?></td>
                        </tr>
                    </table>
                </div>
                <div class="col-xs-6">
                    <h2>Kewajiban</h2>
                    <table style="width: 100%">
                        <?php foreach ($kewajiban as $item) {
                            ?>
                            <tr>
                                <th><?= $item->nama?></th>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php
                            $x1 = $listKategoriAkunKewajiban[$item->id];
                            foreach ($x1 as $key => $val) {
                                ?>
                                <tr>
                                    <th></th>
                                    <td><?=$val?></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <?php
                                $x2 = $listakun[$key];
                                foreach ($x2 as $idx=> $list) {
                                    ?>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <td><?=$list['kode']?></td>
                                        <td><?=$list['nama']?></td>
                                        <td class="text-right"><?=Yii::$app->formatter->asCurrency(0,'IDR')?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                <tr>
                                    <th></th>
                                    <td><?=strtoupper('Sub Total ' . $val)?></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-right"><?=Yii::$app->formatter->asCurrency(0,'IDR')?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            <tr>
                                <th><?= strtoupper('Sub Total ' . $item->nama)?></th>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="text-right"><?=Yii::$app->formatter->asCurrency(0,'IDR')?></td>
                            </tr>
                            <?php
                        }?>
                    </table>

                    <table width="100%">
                        <?php foreach ($modal as $item) {
                            ?>
                            <tr>
                                <th><?= $item->nama?></th>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php
                            $x1 = $listKategoriAkunModal[$item->id];
                            foreach ($x1 as $key => $val) {
                                ?>
                                <tr>
                                    <th></th>
                                    <td><?=$val?></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <?php
                                $x2 = $listakun[$key];
                                foreach ($x2 as $idx=> $list) {
                                    ?>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <td><?=$list['kode']?></td>
                                        <td><?=$list['nama']?></td>
                                        <td class="text-right"><?=Yii::$app->formatter->asCurrency(0,'IDR')?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                <tr>
                                    <th></th>
                                    <td><?=strtoupper('Sub Total ' . $val)?></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-right"><?=Yii::$app->formatter->asCurrency(0,'IDR')?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            <tr>
                                <th><?= strtoupper('Sub Total ' . $item->nama)?></th>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="text-right"><?=Yii::$app->formatter->asCurrency(0,'IDR')?></td>
                            </tr>
                            <?php
                        }?>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>
