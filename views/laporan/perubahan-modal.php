<?php



/* @var $this \yii\web\View */
/* @var $model \app\models\search\Laporan */

use app\models\Tahunbuku;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

$tahunBuku = ArrayHelper::map(Tahunbuku::find()->all(),'id','tahunbuku');

$css = <<<CSS
.print-report{
    border: solid 1px gray;
    padding: 5px;
}
.report-header{
    padding-bottom: 2pt;
}
@media print {
    .print-report{
        border: none;
        padding: 0;
    }    
}

CSS;
$this->registerCss($css);
?>

<div class="laporan-perubahan-modal">
    <div class="panel panel-primary no-print">
        <div class="panel-body">
            <?php $form = ActiveForm::begin()?>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model,'tahunBuku')->dropDownList($tahunBuku,['prompt' => '-- Tahun Buku--'])?>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6"><?= $form->field($model,'dariTanggal')->widget(\yii\jui\DatePicker::className(),[
                                        'dateFormat' => 'php:Y-m-d',
                                        'options' => ['class' => 'form-control']
                                    ])?></div>
                                <div class="col-md-6">
                                    <div class="pull-right">
                                        <?= \yii\bootstrap\Html::submitButton('Filter',['class' => 'btn btn-primary'])?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $form::end()?>
        </div>
    </div>

    <div class="row print-report">
        <div class="col-md-12">
            <h1 class="text-center report-header">Laporan Perubahan Modal</h1>
            <p>Tanggal: <b><?= Yii::$app->formatter->asDate($model->dariTanggal,'long')?></b></p>
            <table class="table table-condensed table-sm">
                <tr>
                    <th>Modal di awal Desember 2020</th>
                    <td class="text-right"><?= Yii::$app->formatter->asDecimal(0)?></td>
                </tr>
                <tr>
                    <th>Investasi pada Desember 2020</th>
                    <td class="text-right"><?= Yii::$app->formatter->asDecimal(0)?></td>
                </tr>
                <tr>
                    <th>Pengambilan pada Desember 2020</th>
                    <td class="text-right"><?= Yii::$app->formatter->asDecimal(0)?></td>
                </tr>
                <tr>
                    <th>Laba Rugi pada Desember 2020</th>
                    <td class="text-right"><?= Yii::$app->formatter->asDecimal(0)?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
