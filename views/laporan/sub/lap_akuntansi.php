<?php



/* @var $this \yii\web\View */
/* @var $title string */

use yii\bootstrap\Html;

?>
<br>
<div class="panel panel-primary">
    <div class="panel-heading"><?=  Html::encode($title) ?></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <h3>Neraca</h3>
                <p>Menampilakan yang anda miliki(aset), apa yang anda hutang(liabilitas), apa yang anda sudah insvestasikan pada perusahaan anda(ekuitas)</p>
                <p><?= Html::a('Lihat Laporan', ['/neraca'],['class' => 'btn btn-primary'])?></p>
            </div>
            <div class="col-md-4">
                <h3>Laba Rugi</h3>
                <p>(Early Release) Menampilkan setiap tipe transaksi dan jumlah total untuk pendapatan dan pengeluaran anda</p>
                <p><?= Html::a('Lihat Laporan', ['/laba-rugi'],['class' => 'btn btn-primary'])?></p>
            </div>
            <div class="col-md-4">
                <h3>Perubahan Modal</h3>
                <p>Menampilkan pergerakan atau perubahan dalam ekuitas pemilik yang terjadi dalam periode tertentu</p>
                <p><?= Html::a('Lihat Laporan', ['/laporan/perubahan-modal'],['class' => 'btn btn-primary'])?></p>
            </div>
            <div class="col-md-4">
                <h3>Buku Besar</h3>
                <p>Laporan ini menampilkan semua transaksi yang telah dilakukan untuk suatu periode. Laporan ini bermanfaat jika anda memerlukan daftar kronologis untuk semua transaksi yang telah dilakukan oleh perusahaan anda</p>
                <p><?= Html::a('Lihat Laporan', ['/laporan/buku-besar'],['class' => 'btn btn-primary'])?></p>
            </div>
            <div class="col-md-4">
                <h3>Jurnal</h3>
                <p>Laporan semua jurnal per transaksi yang terjadi dalam periode waktu. Hal ini berguna untuk melacak dimana transaksi anda masuk ke masing masing rekening</p>
                <p><?= Html::a('Lihat Laporan', ['/laporan/jurnal-umum'],['class' => 'btn btn-primary'])?></p>
            </div>
        </div>
    </div>
</div>
