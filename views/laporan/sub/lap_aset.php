<?php



/* @var $this \yii\web\View */
/* @var $title string */

use yii\bootstrap\Html;

?>
<br>
<div class="panel panel-primary">
    <div class="panel-heading"><?=  Html::encode($title) ?></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <h3>Ringkasan Aset Tetap</h3>
                <p>Menampilkan daftar aset tetap yang tercatat, dengan modal awal, akumulasi penyusutan, dan nilai bukunya</p>
                <p><?= Html::a('Lihat Laporan', ['/laporan/ringkasan-aset-tetap'],['class' => 'btn btn-primary'])?></p>
            </div>
            <div class="col-md-4">
                <h3>Detail Aset Tetap</h3>
                <p>Menampilkan daftar transaksi yang terkait dengan setiap aset, dan menjelaskan bagaimana transaksi tersebut mempengaruhi nilai bukunya</p>
                <p><?= Html::a('Lihat Laporan', ['/laporan/detail-aset-tetap'],['class' => 'btn btn-primary'])?></p>
            </div>
        </div>
    </div>
</div>
