<?php



/* @var $this \yii\web\View */
/* @var $title string */

use yii\bootstrap\Html;

?>
<br>
<div class="panel panel-primary">
    <div class="panel-heading"><?=  Html::encode($title) ?></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <h3>Daftar Pembayaran SPP</h3>
                <p>Menunjukkan dafatar kronologis dari semua transaksi pembayaran spp, termasuk nomer, user yang bertugas, tanggal jatuh tempo dan jumlah nilai</p>
                <p><?= Html::a('Lihat Laporan', ['/buku-spp'],['class' => 'btn btn-primary'])?></p>
            </div>
<!--            <div class="col-md-4">-->
<!--                <h3>Piutang</h3>-->
<!--                <p>Menampilkan tagihan yang belum dibayar untuk setiap orang tua murid, termasuk nomer, tanggal jatuh tempo, jumlah nilai, dan sisa tagihan yang terhutang</p>-->
<!--                <p>--><?//= Html::a('Lihat Laporan', ['/laporan/piutang'],['class' => 'btn btn-primary'])?><!--</p>-->
<!--            </div>-->
            <!--
            <div class="col-md-4">
                <h3>Usia Piutang</h3>
                <p>Laporan ini memberikan ringkasan piutang secara bulanan, serta jumlah total dari waktu kewaktu</p>
                <p><?= Html::a('Lihat Laporan', ['/laporan/usia-piutang'],['class' => 'btn btn-primary'])?></p>
            </div>
            -->

        </div>
    </div>
</div>
