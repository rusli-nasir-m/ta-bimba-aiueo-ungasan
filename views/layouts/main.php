<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
$css = <<<CSS
.bimba-logo{
    margin: 10px !important;
    width: 100px !important;
}

.container {
    width: 95%;
}
CSS;
$this->registerCss($css);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="row hidden-print">
        <div class="col-md-4">
            <img class="bimba-logo" src="<?= Yii::$app->urlManager->baseUrl?>/images/logo.jpg" alt="" width="100px">
        </div>
        <div class="col-md-8">
            <h1>Sistem Informasi Akuntansi biMBA - AIUEO unit Ungasan</h1>
        </div>
    </div>
    <!--
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right', 'id' => 'navbar_menu'],
        'items' => [
            ['id' => 'home_menu', 'label' => 'Home', 'url' => ['/site/index']],
            ['id' => 'about_menu', 'label' => 'About', 'url' => ['/site/about']],
            ['id' => 'contact_menu', 'label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
            ['id' => 'login_menu', 'label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>
    -->
    <div class="container">
        <?= Alert::widget() ?>
        <div class="row">
            <div class="col-md-3 hidden-print ">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <?php
                        if(Yii::$app->user->isGuest){
                            $menuList = [];
                        }else{

                            switch (Yii::$app->user->identity->role){
                                case \app\models\User::ROLE_KEPALA_AKADEMIK:
                                    $menuList = getParams('menu_kepala_akademik');
                                    break;
                                case \app\models\User::ROLE_PIMPINAN:
                                    $menuList = getParams('menu_pimpinan');
                                    break;
                                case \app\models\User::ROLE_FINANCIAL_ACCOUNT:
                                    $menuList = getParams('menu_finance');
                                    break;
                                case \app\models\User::ROLE_ACCOUNTING:
                                    $menuList = getParams('menu_accounting');
                                    break;
                                case \app\models\User::ROLE_STAF_KEUANGAN:
                                    $menuList = getParams('menu_staf_keuangan');
                                    break;
                                case \app\models\User::ROLE_SUPER_USER:
                                    $menuList = \yii\helpers\ArrayHelper::merge(getParams('menu_kepala_akademik'),getParams('menu_pimpinan'),getParams('menu_accounting'),getParams('menu_finance'),getParams('menu_staf_keuangan'));
                                    break;

                            }
                        }
//                        echo "<pre>";
//                        print_r($menuList);
//                        echo "</pre>";
//                        die();

                        $menu = \yii\helpers\ArrayHelper::merge([
                            ['id' => 'home_menu','label' => 'Dashboard', 'url' => ['site/index']],

                        ],$menuList,[
                            Yii::$app->user->isGuest ? (
                            ['id' => 'login_menu', 'label' => 'Login', 'url' => ['/site/login']]
                            ) : (
                                '<span class="list-group-item">'
                                . Html::beginForm(['/site/logout'], 'post')
                                . Html::submitButton(
                                    'Logout (' . Yii::$app->user->identity->username . ')',
                                    ['class' => 'btn btn-link logout']
                                )
                                . Html::endForm()
                                . '</span>'
                            )
                        ]);
                        ?>
                        <?= \app\widgets\ListGroupMenu::widget([
                            'id' => 'navbar_menu',
                            'items' => $menu,
                        ])?>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <?= $content ?>
            </div>
        </div>

    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
