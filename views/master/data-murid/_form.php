<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TbMurid */
/* @var $form yii\widgets\ActiveForm */

$title = $actionName;
?>

<div class="tb-murid-form">
    <div class="panel panel-primary">
        <div class="panel-heading"><?=  Html::encode($title) ?></div>
        <?php $form = ActiveForm::begin(); ?>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'nim_murid')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'kode_kelas')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Kelas::find()->all(),'kode_kelas','nama_kelas')) ?>

                    <?= $form->field($model, 'tanggal_lahir')->widget(\yii\jui\DatePicker::className(),[
                        'dateFormat' => 'php:Y-m-d',
                        'options' => ['class' => 'form-control']
                    ]) ?>

                    <?= $form->field($model, 'alamat')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'no_telp')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'asal_sekolah')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'tgl_masuk')->widget(\yii\jui\DatePicker::className(),[
                            'dateFormat' => 'php:Y-m-d',
                        'options' => ['class' => 'form-control']
                    ]) ?>
                    <?= $form->field($model, 'status')->dropDownList($model::listStatus()) ?>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
