<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TbMurid */
$actionName = Yii::t('app', 'Tambah Data Murid');
$this->title = Yii::$app->name . ' ' . $actionName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data Murid'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-murid-create">

    <?= $this->render('_form', [
        'model' => $model,
        'actionName' => $actionName
    ]) ?>

</div>
