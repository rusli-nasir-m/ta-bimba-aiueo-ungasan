<?php

/**
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $this \yii\web\View
 * @var $model \app\models\TbMurid
 */

use yii\bootstrap\Html;

$namaSiswa = $model->nama;
$this->title = "Daftar Tagihan SPP {$namaSiswa}";
$totalTagihan = 0;

if($dataProvider->count){
    foreach ($dataProvider->getModels() as $item) {
        /**
         * @var $item \app\models\TbSpp
         */
        $totalTagihan += $item->jumlah;
    }
}
?>

<div class="panel panel-primary">
    <div class="panel-heading"><?= Html::encode($this->title) ?></div>
    <div class="panel-body">
        <?= \yii\widgets\DetailView::widget([
            'model' => $model,
            'attributes' => [
                'nim_murid',
                'nama',
                'kelas.nama_kelas',
            ]
        ])?>
    </div>
</div>

<?= \yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'emptyText' => 'Tidak ada tunggakan SPP.',
    'columns' => [
        [
            'class' => 'yii\grid\CheckboxColumn',
            'checkboxOptions' => function($model) {
                return ['value' => $model->nim_murid];
            },
        ],
        'tahun',
        'bulan',
        'jumlah',
        'id_pembayaran',
    ]
])?>
<?php
if($dataProvider->count){

}
?>
