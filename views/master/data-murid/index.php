<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TbMurid */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Data Murid');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-murid-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Tambah Data Murid'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Buku SPP'), ['/buku-spp'], ['class' => 'btn btn-info']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nim_murid',
            'nama',
//            'kode_kelas',
            [
                'attribute' => 'tanggal_lahir',
                'format' => 'date',
                'filter' => \yii\jui\DatePicker::widget([
                      'attribute' =>   'tanggal_lahir',
                    'model' => $searchModel,
                    'dateFormat' => 'php:Y-m-d',
                    'options' => ['class' => 'form-control']
                ])
            ],
            'alamat',
            'no_telp',
            'asal_sekolah:ntext',
            [
                 'attribute' => 'tgl_masuk',
                'format' => 'date',
                'filter' => \yii\jui\DatePicker::widget([
                    'attribute' =>   'tgl_masuk',
                    'model' => $searchModel,
                    'dateFormat' => 'php:Y-m-d',
                    'options' => ['class' => 'form-control']
                ])
            ],
            [
                 'attribute' => 'status',
                'filter' => $searchModel::listStatus()
            ],
            //'created_at',
            //'updated_at',

            [
                 'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '100px'],
                'template' => '{view} {update} {daftar-tagihan-spp} {delete}',
                'buttons' => [
                     'daftar-tagihan-spp' => function ($url, $model, $key) {
                         $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-th-list"]);
                        return Html::a($icon, $url, [
                            [
                                'title' => 'Buku SPP',
                                'aria-label' => 'Buku SPP',
                                'data-pjax' => '0',
                            ]
                        ]);
                     }
                ]
            ],
        ],
    ]); ?>


</div>
