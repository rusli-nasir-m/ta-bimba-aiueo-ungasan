<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pegawai */
/* @var $form yii\widgets\ActiveForm */
/* @var $actionName string */

$title = $actionName;
?>

<div class="pegawai-form">
    <div class="panel panel-primary">
        <div class="panel-heading"><?=  Html::encode($title) ?></div>
        <?php $form = ActiveForm::begin(); ?>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'id_pegawai')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'nama_pegawai')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'jenis_kelamin')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'golongan')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'jabatan')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
