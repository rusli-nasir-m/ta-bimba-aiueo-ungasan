<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pegawai */
$actionName = Yii::t('app', 'Update Pegawai: {name}', [
    'name' => $model->id_pegawai,
]);
$this->title = Yii::$app->name . ' ' . Yii::t('app', 'Update Pegawai: {name}', [
    'name' => $model->id_pegawai,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pegawai'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_pegawai, 'url' => ['view', 'id' => $model->id_pegawai]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pegawai-update">

    <?= $this->render('_form', [
        'model' => $model,
        'actionName' => $actionName,
    ]) ?>

</div>
