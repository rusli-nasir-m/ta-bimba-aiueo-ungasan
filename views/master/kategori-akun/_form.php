<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KategoriAkun */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kategori-akun-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kategori_akun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kelompok_akun_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
