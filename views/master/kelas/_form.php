<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $actionName string */
/* @var $model app\models\Kelas */
/* @var $form yii\widgets\ActiveForm */

$title = $actionName;
?>

<div class="kelas-form">
    <div class="panel panel-primary">
        <div class="panel-heading"><?=  Html::encode($title) ?></div>
        <?php $form = ActiveForm::begin(); ?>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'kode_kelas')->textInput(['disabled' => true,'placeholder' => '--Auto Number--']) ?>
                    <?= $form->field($model, 'nama_kelas')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'jumlah_pertemuan')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'durasi')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'harga')->textInput(['maxlength' => true]) ?>


                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
