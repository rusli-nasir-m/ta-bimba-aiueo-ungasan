<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kelas */
$actionName = Yii::t('app', 'Tambah Kelas');
$this->title = Yii::$app->name . ' ' . $actionName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kelas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-create">

    <?= $this->render('_form', [
        'model' => $model,
        'actionName' => $actionName
    ]) ?>

</div>
