<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Kelas */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::$app->name . ' ' .Yii::t('app', 'Kelas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Tambah Kelas'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kode_kelas',
            'nama_kelas',
            'jumlah_pertemuan',
            'durasi',
            'harga:decimal',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
