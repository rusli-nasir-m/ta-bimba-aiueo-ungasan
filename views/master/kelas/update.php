<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kelas */
$actionName = Yii::t('app', 'Update Kelas: {name}', [
    'name' => $model->nama_kelas,
]);
$this->title = Yii::$app->name . ' ' . Yii::t('app', 'Update Kelas: {name}', [
    'name' => $model->nama_kelas,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kelas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kode_kelas, 'url' => ['view', 'id' => $model->kode_kelas]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="kelas-update">

    <?= $this->render('_form', [
        'model' => $model,
        'actionName' => $actionName,
    ]) ?>

</div>
