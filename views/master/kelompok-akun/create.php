<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KelompokAkun */
$actionName = Yii::t('app', 'Create Kelompok Akun');
$this->title = Yii::$app->name . ' ' . $actionName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kelompok Akun'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelompok-akun-create">

    <?= $this->render('_form', [
        'model' => $model,
        'actionName' => $actionName
    ]) ?>

</div>
