<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KelompokAkun */
$actionName = Yii::t('app', 'Update Kelompok Akun: {name}', [
    'name' => $model->id,
]);
$this->title = Yii::$app->name . ' ' . Yii::t('app', 'Update Kelompok Akun: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kelompok Akun'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="kelompok-akun-update">

    <?= $this->render('_form', [
        'model' => $model,
        'actionName' => $actionName,
    ]) ?>

</div>
