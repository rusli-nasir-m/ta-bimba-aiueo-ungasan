<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */
$actionName = Yii::t('app', 'Tambah Pengguna');
$this->title = Yii::$app->name . ' ' . $actionName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <?= $this->render('_form', [
        'model' => $model,
        'actionName' => $actionName
    ]) ?>

</div>
