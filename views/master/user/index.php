<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', '<i class="glyphicon glyphicon-plus"></i> Tambah User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
//            'password_hash',
//            'password_raw',
            [
                 'attribute' => 'role',
                'value' => function($model){
                    return ArrayHelper::getValue(getParams('user_access'),$model->role);
                }
            ],
            [
                'attribute' => 'status',
                'value' => function($model){
                    return ArrayHelper::getValue(getParams('status_data'),$model->status);
                }
            ],
            //'created_at',
            //'updated_at',
            //'logged_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
