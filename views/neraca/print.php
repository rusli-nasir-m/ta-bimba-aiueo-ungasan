<?php


use app\models\Akun;
use app\models\KategoriAkun;
use app\models\KelompokAkun;
use yii\helpers\ArrayHelper;

/** @var $this \yii\web\View */
/* @var $dataProvider array */


$aktiva = KelompokAkun::findAll(['id' => [1]]);
$kewajiban = KelompokAkun::findAll(['id' => [2]]);
$modal = KelompokAkun::findAll(['id' => [3]]);

$kategoriAkunAktiva = KategoriAkun::findAll(['kelompok_akun_id' => [1]]);
$kategoriAkunKewajiban = KategoriAkun::findAll(['kelompok_akun_id' => [2]]);
$kategoriAkunModal = KategoriAkun::findAll(['kelompok_akun_id' => [3]]);

//$kategoriAkunKey = ArrayHelper::getColumn($kategoriAkun,'id_kategori');
$akun = Akun::find()->all();

$listakun = ArrayHelper::index($akun,'id','kategori_akun_id');
$listKategoriAkunaktiva = ArrayHelper::map($kategoriAkunAktiva,'id_kategori','kategori_akun','kelompok_akun_id');
$listKategoriAkunKewajiban = ArrayHelper::map($kategoriAkunKewajiban,'id_kategori','kategori_akun','kelompok_akun_id');
$listKategoriAkunModal = ArrayHelper::map($kategoriAkunModal,'id_kategori','kategori_akun','kelompok_akun_id');

$css = <<<CSS
.print-report{
    border: solid 1px gray;
    padding: 5px;
}
.report-header{
    padding-bottom: 2pt;
}

@media print {
    .print-report{
        border: none;
        padding: 0;
    }    
}

CSS;
$this->registerCss($css);

$js = <<<JS
print();
JS;
//$this->registerJs($js);
?>
<div class="row print-report">
    <div class="col-md-12">
        <h1 class="text-center report-header">Laporan Neraca</h1>
        <p>Tanggal: <b><?= Yii::$app->formatter->asDate($model->fromDate,'long')?> s/d <b><?= Yii::$app->formatter->asDate($model->toDate,'long')?></b></p>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-condensed" width="100%">
                    <tr>
                        <th colspan="5"><h2>Harta</h2></th>
                    </tr>
                    <?php
                    $subTotal3= [];
                    foreach ($aktiva as $item) {
                        ?>
                        <tr>
                            <th colspan="5"><?= $item->nama?></th>
                        </tr>
                        <?php
                        $x1 = $listKategoriAkunaktiva[$item->id];
                        $subTotal2 = 0;
                        foreach ($x1 as $key => $val) {
                            $dataAkun = isset($dataProvider[$key])?$dataProvider[$key]:null;
                            ?>
                            <tr>
                                <th></th>
                                <td colspan="4"><?=$val?></td>
                            </tr>
                            <?php
                            $subTotal1 = 0;
                            $x2 = $listakun[$key];
                            foreach ($x2 as $idx=> $list) {
                                $dataAkunData = isset($dataAkun[$idx])?$dataAkun[$idx]:null;
                                $tot = 0;
                                if($dataAkunData){
                                    $tot = isset($dataAkunData['total'])?$dataAkunData['total']:0;
                                    $subTotal1 += $tot;
                                }
                                ?>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th colspan="2"><?=$list['kode']?> - <?=$list['nama']?></th>
                                    <td class="text-right"><?=Yii::$app->formatter->asCurrency($tot,'IDR')?></td>
                                </tr>
                                <?php
                            }
                            $subTotal2 += $subTotal1;
                            ?>
                            <tr>
                                <th></th>
                                <td colspan="3"><?=strtoupper('Sub Total ' . $val)?></td>
                                <td class="text-right"><?=Yii::$app->formatter->asCurrency($subTotal1,'IDR')?></td>
                            </tr>
                            <?php
                        }
                        $subTotal3[$item->id] =isset($subTotal3[$item->id])?$subTotal3[$item->id] + $subTotal2:$subTotal2;
                    }?>
                    <tr class="info">
                        <th colspan="4">TOTAL HARTA</th>
                        <td class="text-right"><?=Yii::$app->formatter->asCurrency($subTotal2,'IDR')?></td>
                    </tr>
                    <tr>
                        <th colspan="5"><h2>Kewajiban</h2></th>
                    </tr>
                    <?php foreach ($kewajiban as $item) {
                        ?>
                        <tr>
                            <th colspan="5"><?= $item->nama?></th>
                        </tr>
                        <?php
                        $x1 = $listKategoriAkunKewajiban[$item->id];
                        $subTotal2 = 0;
                        foreach ($x1 as $key => $val) {
                            $dataAkun = isset($dataProvider[$key])?$dataProvider[$key]:null;
                            ?>
                            <tr>
                                <th></th>
                                <td colspan="4"><?=$val?></td>
                            </tr>
                            <?php
                            $x2 = $listakun[$key];
                            $subTotal1 = 0;
                            foreach ($x2 as $idx=> $list) {
                                $dataAkunData = isset($dataAkun[$idx])?$dataAkun[$idx]:null;
                                $tot = 0;
                                if($dataAkunData){
                                    $tot = isset($dataAkunData['total'])?$dataAkunData['total']:0;
                                    $subTotal1 += $tot;
                                }
                                ?>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <td colspan="2"><?=$list['kode']?> - <?=$list['nama']?></td>
                                    <td class="text-right"><?=Yii::$app->formatter->asCurrency($tot,'IDR')?></td>
                                </tr>
                                <?php
                            }
                            $subTotal2 += $subTotal1;
                            ?>
                            <tr>
                                <th></th>
                                <td colspan="3"><?=strtoupper('Sub Total ' . $val)?></td>
                                <td class="text-right"><?=Yii::$app->formatter->asCurrency($subTotal1,'IDR')?></td>
                            </tr>
                            <?php
                        }
                        $subTotal3[$item->id] =isset($subTotal3[$item->id])?$subTotal3[$item->id] + $subTotal2:$subTotal2;
                        ?>
                        <tr>
                            <th colspan="4"><?= strtoupper('Sub Total ' . $item->nama)?></th>
                            <td class="text-right"><?=Yii::$app->formatter->asCurrency($subTotal2,'IDR')?></td>
                        </tr>
                        <?php
                    }?>
                    <tr><th colspan="5"><h2>Modal</h2></th></tr>
                    <?php foreach ($modal as $item) {
                        ?>
                        <tr>
                            <th colspan="5"><?= $item->nama?></th>
                        </tr>
                        <?php
                        $x1 = $listKategoriAkunModal[$item->id];
                        $subTotal2 = 0;
                        foreach ($x1 as $key => $val) {
                            $dataAkun = isset($dataProvider[$key])?$dataProvider[$key]:null;
                            ?>
                            <tr>
                                <th></th>
                                <td colspan="4"><?=$val?></td>
                            </tr>
                            <?php
                            $x2 = $listakun[$key];
                            $subTotal1 = 0;
                            foreach ($x2 as $idx=> $list) {
                                $dataAkunData = isset($dataAkun[$idx])?$dataAkun[$idx]:null;
                                $tot = 0;
                                if($dataAkunData){
                                    $tot = isset($dataAkunData['total'])?$dataAkunData['total']:0;
                                    $subTotal1 += $tot;
                                }
                                ?>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <td colspan="2"><?=$list['kode']?> - <?=$list['nama']?></td>
                                    <td class="text-right"><?=Yii::$app->formatter->asCurrency($tot,'IDR')?></td>
                                </tr>
                                <?php
                            }
                            $subTotal2 += $subTotal1;
                            ?>
                            <tr>
                                <th></th>
                                <td colspan="3"><?=strtoupper('Sub Total ' . $val)?></td>
                                <td class="text-right"><?=Yii::$app->formatter->asCurrency($subTotal1,'IDR')?></td>
                            </tr>
                            <?php
                        }
                        $subTotal3[$item->id] =isset($subTotal3[$item->id])?$subTotal3[$item->id] + $subTotal2:$subTotal2;
                        ?>
                        <tr>
                            <th colspan="4"><?= strtoupper('Sub Total ' . $item->nama)?></th>
                            <td class="text-right"><?=Yii::$app->formatter->asCurrency($subTotal2,'IDR')?></td>
                        </tr>
                        <?php
                    }?>
                </table>
            </div>
        </div>
    </div>
</div>