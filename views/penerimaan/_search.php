<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\TbKasMasuk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tb-kas-masuk-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_kas_masuk') ?>

    <?= $form->field($model, 'kode_penerimaan') ?>

    <?= $form->field($model, 'tanggal_penerimaan') ?>

    <?= $form->field($model, 'jumlah') ?>

    <?= $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'id_jurnal') ?>

    <?php // echo $form->field($model, 'sumber_penerimaan') ?>

    <?php // echo $form->field($model, 'nim_murid') ?>

    <?php // echo $form->field($model, 'cara_bayar') ?>

    <?php // echo $form->field($model, 'bank') ?>

    <?php // echo $form->field($model, 'reff_no') ?>

    <?php // echo $form->field($model, 'jenis_permohonan') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
