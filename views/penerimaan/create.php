<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TbKasMasuk */
$actionName = Yii::t('app', 'Tambah Penerimaan');
$this->title = Yii::$app->name . ' ' . $actionName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Penerimaan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-kas-masuk-create">

    <?= $this->render('_form', [
        'model' => $model,
        'actionName' => $actionName
    ]) ?>

</div>
