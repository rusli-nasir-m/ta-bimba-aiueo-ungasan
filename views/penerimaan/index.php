<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TbKasMasuk */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Penerimaan');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-kas-masuk-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Tambah Penerimaan SPP'), ['create','jp' => 1], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Tambah Penerimaan Pendaftaran'), ['create','jp' => 2], ['class' => 'btn btn-success']) ?>
        <?= ''//Html::a(Yii::t('app', 'Tambah Penerimaan Lain-Lain'), ['create','jp' => 3], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id_kas_masuk',
            'tanggal_penerimaan',
            [
                 'attribute' => 'jumlah',
                'format' => 'decimal',
                'contentOptions' => ['class' => 'text-right']
            ],
//            'created_at',
            //'updated_at',
            //'id_jurnal',
            'dataPenerimaan.nama:text:Jenis Penerimaan',
            'nim_murid',
//            'cara_bayar',
//            'bank',
            //'reff_no',
            'jenis_permohonan',
            [
                'class' => 'yii\grid\ActionColumn',
            ],
        ],
    ]); ?>


</div>
