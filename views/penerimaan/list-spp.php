<?php



/* @var $this \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */

echo \yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'id' => 'list_daftar_tagihan_spp',
    'columns' => [
        [
            'class' => 'yii\grid\CheckboxColumn',
            'multiple' => true,
            'cssClass' => 'grid-payment-check',
            'headerOptions' => ['class' => 'as'],
            'checkboxOptions' => function ($model, $key, $index, $column) {
                return [
                    'value' => $model->jumlah,
                    'data-key' => $model->id_spp,
                ];
            },
        ],
//        'id_spp',
//        'nim_murid',
        'tahun',
        'bulan',
        'jumlah',
        'jatuh_tempo',
    ]
]);
?>