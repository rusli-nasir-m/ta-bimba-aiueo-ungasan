<?php

use yii\bootstrap\ActiveForm;

$css = <<<CSS
.ui-autocomplete {
    max-height: 200px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
}
/* IE 6 doesn't support max-height
* we use height instead, but this forces the menu to always be this tall
*/
* html .ui-autocomplete {
height: 100px;
}
.daftar-barang{
    max-height: 580px;    
    overflow: auto;
    border-right: 1px solid #ccc!important;
    border-left: 1px solid #ccc!important;
    
}
.each{
    border-bottom: 1px solid #555;
    padding: 3px 0;
    }
.acItem > span {
    padding: 1px 0;
    display: block;
    
}        
.acItem .name{
  font-size: 14px;
  font-weight: bold;
  font-family: Arial, Helvetica, sans-serif;
}

.acItem .price{
  font-size: 14px;
  font-weight: bold;
  font-family:"Courier New", Courier, monospace;
}

.acItem .desc{
  font-size: 10px;  
  font-family: Arial, Helvetica, sans-serif;
  color:#555;
}

.form-control.has-error {
    border-color: #dd4b39;
    box-shadow: none;
} 
.pengiriman_transaksi, .pay_transaksi{
  display: none;
}
CSS;
$this->registerCss($css);

/* @var $this \yii\web\View */
/* @var $model \app\models\TbKasMasuk */
$this->title = 'Penerimaan SPP';
$title = $this->title;
$akunKas = \app\models\Akun::find()->where(['kategori_akun_id'=> 1])->all();
$listAakunKas = \yii\helpers\ArrayHelper::map($akunKas,'kode','nama')
?>
<div class="transaksi-penerimaan-spp-index">
    <div class="panel panel-primary">
        <?php $form = ActiveForm::begin([
             'enableClientScript' => false,
            'enableAjaxValidation' => false,
            'enableClientValidation' => false,
        ]); ?>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model,'cariSiswa')->textInput()?>
                    <?= $form->field($model, 'nim_murid')->hiddenInput()->label(false) ?>
                    <h3>Daftar Tunggakan Pembayaran SPP</h3>
                    <div id="daftar_tagihan_spp">Silahkan cari siswa untuk melihat tagihan</div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>