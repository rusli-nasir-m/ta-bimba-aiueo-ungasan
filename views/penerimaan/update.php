<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TbKasMasuk */
$actionName = Yii::t('app', 'Update Penerimaan: {name}', [
    'name' => $model->id_kas_masuk,
]);
$this->title = Yii::$app->name . ' ' . Yii::t('app', 'Update Penerimaan: {name}', [
    'name' => $model->id_kas_masuk,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Penerimaan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_kas_masuk, 'url' => ['view', 'id' => $model->id_kas_masuk]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tb-kas-masuk-update">

    <?= $this->render('_form', [
        'model' => $model,
        'actionName' => $actionName,
    ]) ?>

</div>
