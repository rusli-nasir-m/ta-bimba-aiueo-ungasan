<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TbKasMasuk */

$this->title = $model->id_kas_masuk;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tb Kas Masuk'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tb-kas-masuk-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_kas_masuk], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id_kas_masuk], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_kas_masuk',
            'kode_penerimaan',
            'tanggal_penerimaan',
            'jumlah',
            'created_at',
            'updated_at',
            'id_jurnal',
            'sumber_penerimaan',
            'nim_murid',
            'cara_bayar',
            'bank',
            'reff_no',
            'jenis_permohonan',
        ],
    ]) ?>

</div>
