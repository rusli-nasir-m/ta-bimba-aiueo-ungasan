<?php

use app\models\search\JenisPengeluaran;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TbPengeluaran */
/* @var $form yii\widgets\ActiveForm */

$title = $actionName;
$listAkunAktiva = \yii\helpers\ArrayHelper::map(Yii::$app->db->createCommand("SELECT id,CONCAT(kode,'~',nama ) as nama, nama_kategori_akun FROM v_tb_akun GROUP BY id ORDER BY nama_kategori_akun")->queryAll(),'id','nama','nama_kategori_akun');

?>

<div class="tb-pengeluaran-form">
    <div class="panel panel-primary">
        <div class="panel-heading"><?=  Html::encode($title) ?></div>
        <?php $form = ActiveForm::begin(); ?>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'id_pengeluaran')->textInput(['maxlength' => true,'disabled' => true,'placeholder' => '--auto number--']) ?>
                    <?= $form->field($model, 'tanggal_realisasi')->textInput(['readonly' => true])
                    //->widget(\yii\jui\DatePicker::className(),[
//                        'dateFormat' => 'php:Y-m-d',
//                        'options' => ['class' => 'form-control','readonly' => true]
//                    ])
                    ?>

                    <?= $form->field($model, 'jenis_permohonan')->dropDownList(JenisPengeluaran::selectOptions(),[
                        'prompt' => 'Pilih Jenis permohonan'
                    ]) ?>
                    <?= $form->field($model, 'rekeningDebit')->dropDownList($listAkunAktiva,[
                        'prompt' => '--Rekening Debit--'
                    ]) ?>
                    <?= $form->field($model, 'rekeningKredit')->dropDownList($listAkunAktiva,[
                        'prompt' => '--Rekening Kredit--'
                    ]) ?>
                    <?= $form->field($model, 'jumlah_pengeluaran')->textInput() ?>

                    <?= $form->field($model, 'keperluan')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'keterangan')->textInput(['maxlength' => true]) ?>


                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php
$urlz = \yii\helpers\Url::to(['jenis-pengeluaran']);
$js = <<<JS
$('#tbpengeluaran-jenis_permohonan').on('blur',function() {
  var val = $(this).val();
    $.get('{$urlz}',{id:val},function(result) {
        $('#tbpengeluaran-rekeningdebit').val(result.rekdebit);
        $('#tbpengeluaran-rekeningkredit').val(result.rekkredit);  
        $('#tbpengeluaran-keperluan').val(result.ketrangan);  
        $('#tbpengeluaran-keterangan').val(result.ketrangan);  
    })
    
})
JS;
$this->registerJs($js);
?>