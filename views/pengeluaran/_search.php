<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\TbPengeluaran */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tb-pengeluaran-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_pengeluaran') ?>

    <?= $form->field($model, 'id_user') ?>

    <?= $form->field($model, 'id_pengajuan') ?>

    <?= $form->field($model, 'jenis_pengeluaran') ?>

    <?= $form->field($model, 'tanggal_realisasi') ?>

    <?php // echo $form->field($model, 'jumlah_pengeluaran') ?>

    <?php // echo $form->field($model, 'keperluan') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <?php // echo $form->field($model, 'id_jurnal') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
