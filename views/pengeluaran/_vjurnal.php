<?php



/* @var $this \yii\web\View */
/* @var $model \app\models\TbPengeluaran|\yii\db\ActiveRecord */

$jurnal = $model->jurnal;
?>
<?php
if($jurnal){
    $dataProvider = new \yii\data\ActiveDataProvider([
        'query' => $jurnal->getJurnalDetails(),
        'pagination' => false,
        'sort' => false
    ]);
    ?>
    <?= \yii\widgets\DetailView::widget([
        'model' => $jurnal,
        'attributes' => [
            'no',
            'tgl',
            'keterangan:ntext',
            'tahunBuku.tahunbuku',
            'sumberjurnal',
        ],
    ]) ?>
    <div class="row">
        <div class="col-md-12">
            <?= \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    'akun.kode',
                    'akun.nama',
                    'debit:decimal',
                    'kredit:decimal',
                ]
            ])?>
        </div>
    </div>
    <?php
}
?>