<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TbPengeluaran */
$actionName = Yii::t('app', 'Tambah Pengeluaran');
$this->title = Yii::$app->name . ' ' . $actionName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengeluaran'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-pengeluaran-create">

    <?= $this->render('_form', [
        'model' => $model,
        'actionName' => $actionName
    ]) ?>

</div>
