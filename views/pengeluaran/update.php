<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TbPengeluaran */
$actionName = Yii::t('app', 'Update Pengeluaran: {name}', [
    'name' => $model->id_pengeluaran,
]);
$this->title = Yii::$app->name . ' ' . Yii::t('app', 'Update Pengeluaran: {name}', [
    'name' => $model->id_pengeluaran,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengeluaran'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_pengeluaran, 'url' => ['view', 'id' => $model->id_pengeluaran]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tb-pengeluaran-update">

    <?= $this->render('_form', [
        'model' => $model,
        'actionName' => $actionName,
    ]) ?>

</div>
