<?php

use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TbPengeluaran */

$this->title = 'Transaksi Pengeluaran '.$model->id_pengeluaran;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tb Pengeluaran'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->id_pengeluaran;
\yii\web\YiiAsset::register($this);
?>
<div class="tb-pengeluaran-view">

    <h1><?= Html::encode($model->id_pengeluaran) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_pengeluaran], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id_pengeluaran], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_pengeluaran',
            'user.username:text:User',
//            'id_pengajuan',
            'jenis_pengeluaran',
            'tanggal_realisasi',
            'jumlah_pengeluaran',
            'keperluan',
            'keterangan',
//            'id_jurnal',
        ],
    ]) ?>
    <div class="row">
        <div class="col-md-12">
            <?= Tabs::widget([
                'items' => [
                    [
                        'label' => 'Jurnal',
                        'content' => $this->render('_vjurnal',['model' => $model]),
                    ],
                ]
            ])?>
        </div>
    </div>
</div>
