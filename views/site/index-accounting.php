<?php


/* @var $this \yii\web\View */


?>
<div class="site-index-super-user">
    <div class="panel panel-success">
        <div class="panel-heading text-center">Akses Accounting</div>
        <div class="panel-body">
            <div class="text-center">
                <div class="row">
                    <div class="col-md-6">
                        <div class="jumbotron">
                            <h2><?= Yii::$app->formatter->asCurrency(0,'IDR')?></h2>
                            <p>Pemasukan Bulan Ini</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="jumbotron">
                            <h2><?= Yii::$app->formatter->asCurrency(\app\models\TbPengeluaran::getTotalPengeluaran(),'IDR')?></h2>
                            <p>Pengeluaran Bulan Ini</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="jumbotron">
                            <h2><?= Yii::$app->formatter->asCurrency(\app\models\TbSpp::getPiutangSpp(),'IDR')?></h2>
                            <p>Total Piutang SPP</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
