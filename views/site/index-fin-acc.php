<?php


/* @var $this \yii\web\View */


?>
<div class="site-index-super-user">
    <div class="panel panel-success">
        <div class="panel-heading text-center">Akses Financial Accounting</div>
        <div class="panel-body">
            <div class="text-center">
                <div class="row">
                    <div class="col-md-6">
                        <div class="jumbotron" style="border: 1px solid grey;border-radius: 10px">
                            <p>Saldo</p>
                            <h2><?= Yii::$app->formatter->asCurrency(0,'IDR')?></h2>
                            <hr>
                            <table width="100%">
                                <tr>
                                    <th>Pemasukan</th>
                                    <td class="text-right"><?= Yii::$app->formatter->asCurrency(0,'IDR')?></td>
                                </tr>
                                <tr>
                                    <th>Pengeluaran</th>
                                    <td class="text-right"><?= Yii::$app->formatter->asCurrency(\app\models\TbPengeluaran::getTotalPengeluaran(),'IDR')?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="jumbotron">
                            <h2><span class="glyphicon glyphicon-user"></span> <?= \app\models\TbMurid::jumlahMuridNonaktif()?>  </h2>
                            <p>Jumlah Murid Aktif</p>
                        </div>
                        <div class="jumbotron">
                            <h2><?= Yii::$app->formatter->asCurrency(\app\models\TbSpp::getPiutangSpp(),'IDR')?></h2>
                            <p>Jumlah Piutang SPP</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
