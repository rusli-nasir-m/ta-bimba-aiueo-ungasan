<?php


/* @var $this \yii\web\View */

use app\models\TbMurid;
use app\models\TbSpp;
use practically\chartjs\Chart;

\app\assets\ChartJsAsset::register($this);

$curYear = date('Y');
?>
<div class="site-index-super-user">
    <div class="panel panel-success">
        <div class="panel-heading text-center">Akses Kepala Akademik</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <?= Chart::widget([
                        'type' => Chart::TYPE_BAR,
                        'clientOptions' => [
                            'showAllTooltips' => true,
                            'legend' => ['display' => false],
                            'title' => [
                                'display' => true,
                                'text' => "Perkembangan Jumlah Murid Tahun {$curYear}",
                            ],
                            'tooltips' => [
                                'displayColors' => false,
                                'custom' => new \yii\web\JsExpression("
                                        function(tooltip) {
                                            if (!tooltip) return;
                                            // disable displaying the color box;
                                            tooltip.displayColors = false;
                                        }
                                     "),
                                'callbacks' =>[
                                    'title' => new \yii\web\JsExpression("
                                            function(tooltipItem, data) {
                                                return ''
                                            }
                                         "),
                                    'label' => new \yii\web\JsExpression("
                                            function(tooltipItem, data) {
                                                return tooltipItem.value
                                            }
                                         ")]
                            ]
                        ],
                        'datasets' => [
                            [
                                'data' => TbMurid::perkembanganMuridPerTahun($curYear)
                            ]
                        ]
                    ]);
                    ?>
                </div>
                <div class="col-md-6">
                    <div class="jumbotron">
                        <h2>Jumlah Murid Aktif Bulan Ini</h2>
                        <p><?= TbMurid::jumlahMuridaktif()?> Siswa</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="jumbotron">
                        <h2><?= Yii::$app->formatter->asCurrency(TbSpp::getPiutangSpp(),'IDR')?></h2>
                        <p>Total Piutang SPP</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="jumbotron">
                        <h2>Jumlah Murid Tidak Aktif Per Bulan</h2>
                        <p><?= TbMurid::jumlahMuridNonaktif()?> Siswa</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
