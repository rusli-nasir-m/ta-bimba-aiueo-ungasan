<?php


/* @var $this \yii\web\View */


?>
<div class="site-index-super-user">
    <div class="panel panel-success">
        <div class="panel-heading text-center">Akses Keuangan</div>
        <div class="panel-body">
            <div class="text-center">
                <div class="row">
                    <div class="col-md-6">
                        <div class="jumbotron">
                            <h2><span class="glyphicon glyphicon-user"></span> <?= \app\models\TbMurid::jumlahMuridaktif()?> </h2>
                            <p>Jumlah Murid Aktif</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="jumbotron">
                            <h2>Rp.0</h2>
                            <p>Jumlah Pemasukan</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="jumbotron">
                            <h2><?= Yii::$app->formatter->asCurrency(\app\models\TbSpp::getPiutangSpp(),'IDR')?></h2>
                            <p>Jumlah Piutang SPP</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
