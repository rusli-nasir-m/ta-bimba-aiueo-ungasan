<?php

/* @var $this yii\web\View */

$this->title = Yii::$app->name . ' | Dasbor';
?>
<div class="site-index">
    <div class="panel panel-success">
        <div class="panel-heading">Ringkasan Bisnis</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="jumbotron">
                        <h2>Rp.0</h2>
                        <p>Pendapatan Hari Ini</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="jumbotron">
                        <h2>Rp.0</h2>
                        <p>Pendapatan Bulan Ini</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="jumbotron">
                        <h2>Rp.0</h2>
                        <p>Total Biaya Pengeluaran</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="jumbotron">
                        <h2>Rp.0</h2>
                        <p>Total Piutang</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-info">
        <div class="panel-heading">Informasi Probadi</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="jumbotron">
                        <h2>Rp.0</h2>
                        <p>Gaji Pokok</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="jumbotron">
                        <h2>Rp.0</h2>
                        <p>Potongan Gaji Bulan Ini</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="jumbotron">
                        <h2>1 kali</h2>
                        <p>Tidak Hadir Tanpa Keterangan</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
//$gaji = new \app\models\TbGaji();
//echo '<pre>';
//print_r( \yii\helpers\ArrayHelper::toArray($gaji->kalkulasiGaji(1)));
//echo '</pre>';
?>
</div>
