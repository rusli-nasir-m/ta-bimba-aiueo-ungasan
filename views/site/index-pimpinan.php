<?php


/* @var $this \yii\web\View */

use app\models\TbMurid;
use practically\chartjs\Chart;

$curYear = date('Y');
\app\assets\ChartJsAsset::register($this);
?>
<div class="site-index-pimpinan">
    <div class="panel panel-success">
        <div class="panel-heading text-center">Akses Pimpinan</div>
        <div class="panel-body">
            <div class="text-center">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="jumbotron">
                            <p>Saldo</p>
                            <h2><?= Yii::$app->formatter->asCurrency(0,'IDR')?></h2>
                            <hr>
                            <table width="100%">
                                <tr>
                                    <th>Pemasukan</th>
                                    <td class="text-right"><?= Yii::$app->formatter->asCurrency(0,'IDR')?></td>
                                </tr>
                                <tr>
                                    <th>Pengeluaran</th>
                                    <td class="text-right"><?= Yii::$app->formatter->asCurrency(0,'IDR')?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="jumbotron">
                            <h2><?= Yii::$app->formatter->asCurrency(\app\models\TbSpp::getPiutangSpp(),'IDR')?></h2>
                            <p>Total Piutang SPP</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="jumbotron">
                            <h2><span class="glyphicon glyphicon-user"></span> <?= TbMurid::jumlahMuridaktif()?> </h2>
                            <p>Jumlah Murid Aktif</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <?= Chart::widget([
                            'type' => Chart::TYPE_BAR,
                            'clientOptions' => [
                                'showAllTooltips' => true,

                                'legend' => ['display' => false],
                                'title' => [
                                    'display' => true,
                                    'text' => "Perkembangan Murid Tahun {$curYear}",
                                ],
                                'tooltips' => [
                                    'displayColors' => false,
                                    'custom' => new \yii\web\JsExpression("
                                        function(tooltip) {
                                            if (!tooltip) return;
                                            // disable displaying the color box;
                                            tooltip.displayColors = false;
                                        }
                                     "),
                                    'callbacks' =>[
                                        'title' => new \yii\web\JsExpression("
                                            function(tooltipItem, data) {
                                                return ''
                                            }
                                         "),
                                        'label' => new \yii\web\JsExpression("
                                            function(tooltipItem, data) {
                                                return tooltipItem.value
                                            }
                                         ")]
                                ]
                            ],
                            'datasets' => [
                                [
                                    'data' => TbMurid::perkembanganMuridPerTahun($curYear)
                                ]
                            ]
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
