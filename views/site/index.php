<?php

/* @var $this yii\web\View */

$this->title = Yii::$app->name . ' | Dasbor';
?>
<div class="site-index">
    <div class="panel panel-success">
        <div class="panel-heading">Ringkasan Bisnis</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="jumbotron">
                        <h2>Rp.0</h2>
                        <p>Pendapatan Hari Ini</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="jumbotron">
                        <h2>Rp.0</h2>
                        <p>Pendapatan Bulan Ini</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="jumbotron">
                        <h2>Rp.0</h2>
                        <p>Total Biaya Pengeluaran</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="jumbotron">
                        <h2>Rp.0</h2>
                        <p>Total Piutang</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
