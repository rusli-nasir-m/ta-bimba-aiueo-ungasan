<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">

    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title text-center"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-body">
                    <p>Masukkan username dan password anda!</p>
                    <?php $form = ActiveForm::begin([
                        'id' => $model->formName(),

                    ]); ?>

                    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                    <?= $form->field($model, 'password')->passwordInput() ?>
                    <div class="form-group">
                        <?= Html::button('Login', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button','id' => 'but_submit']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$url = \yii\helpers\Url::to(['site/index']);
$js = <<<JS

    $("#but_submit").on('click',function(){
        var form = $('#{$model->formName()}');
        $.ajax({
            url:form.attr('action'),
            method:'post',
            data:form.serialize(),
            success:function(response){
                var msg = "";
                if(response.success === 1){
                        swal('Success!', response.message,response.status);
                    setTimeout(function() {
                        window.location = "{$url}";
                    },1000)
                }else{
                    swal('Error!', response.message,response.status);
                    form.trigger("reset");
                }
            }
        });
    });
JS;
$this->registerJs($js);
?>
