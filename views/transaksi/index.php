<?php



/* @var $this \yii\web\View */

?>
<div class="transaksi-index">
    <?= \yii\bootstrap\Tabs::widget([
        'items' => [
            [
                'label' => 'Form Pembayaran SPP',
                'content' => $this->render('sub/pembayaran_spp'),
                'active' => true
            ],
            [
                'label' => 'Pengajuan',
                'content' => $this->render('sub/pengajuan'),
            ],
        ]
    ])?>
</div>
