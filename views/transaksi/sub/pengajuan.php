<?php



/* @var $this \yii\web\View */

use yii\bootstrap\Html;

?>
<div class="pengajuan-index">
    <div class="panel panel-primary">
        <div class="panel-heading">Pengajuan Kas Kecil</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                <h2>Permintaan Kas Kecil</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>Detail Kas Keluar</h2>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Akun</th>
                                <th>Nama akun</th>
                                <th>Jenis</th>
                                <th>Keterangan</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>Detail Dana</h2>
                    Action <?= Html::button('Acc Pengajuan',['class' => 'btn btn-primary'])?> // <?= Html::button('Tolak Pengajuan',['class' => 'btn btn-warning'])?>
                </div>
            </div>
        </div>
    </div>
</div>
