<?php

namespace app\widgets;

use Yii;
use yii\widgets\Menu;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * Displays a multi-level list group menu.
 *
 * Example html code of the list group menu is in `http://www.yiiframework.com/doc-2.0/guide-index.html`, e.g.:
 *
 * ```
 * <div id="navigation" class="list-group">
 *     <a class="list-group-item active" href="#navigation-11146" data-toggle="collapse" data-parent="#navigation" aria-expanded="true">yii\bootstrap <b class="caret"></b></a>
 *     <div id="navigation-11146" class="submenu panel-collapse collapse in" aria-expanded="true">
 *         <a class="list-group-item" href="./yii-bootstrap-activefield.html">ActiveField</a>
 *         <a class="list-group-item active" href="./yii-bootstrap-activeform.html">ActiveForm</a>
 *
 *         <a class="list-group-item" href="#navigation-10986" data-toggle="collapse" data-parent="#navigation">yii\behaviors <b class="caret"></b></a>
 *         <div id="navigation-10986" class="submenu panel-collapse collapse">
 *         <a class="list-group-item" href="./yii-behaviors-attributebehavior.html">AttributeBehavior</a>
 *         </div>
 *
 *         <a class="list-group-item" href="./yii-bootstrap-alert.html">Alert</a>
 *     </div>
 *     <a class="list-group-item" href="#navigation-10985" data-toggle="collapse" data-parent="#navigation">yii\behaviors <b class="caret"></b></a>
 *     <div id="navigation-10985" class="submenu panel-collapse collapse">
 *     <a class="list-group-item" href="./yii-behaviors-attributebehavior.html">AttributeBehavior</a>
 *     </div>
 * </div>
 * ```
 *
 * The following example shows how to use ListGroupMenu:
 *
 * ```php
 * echo ListGroupMenu::widget([
 *     'items' => $menuItems,   // same as menu
 *     'options' => ['id' => 'yii2doc'],   // optional
 *     'activateParents' => true,   ///optional
 * ]);
 * ```
 *
 */
class ListGroupMenu extends Menu
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->options = ArrayHelper::merge(['tag' => 'div', 'id' => 'navigation', 'class' => 'list-group'], $this->options);

        $this->registerClientScript();
    }

    protected function normalizeItems($items, &$active)
    {
        foreach ($items as $i => $item) {
            if(is_array($items[$i])){
                if (isset($item['visible']) && !$item['visible']) {
                    unset($items[$i]);
                    continue;
                }
                if (is_array($item) && !isset($item['label'])) {
                    $item['label'] = '';
                }

                $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
                if (is_array($item)) {
                    $items[$i]['label'] = $encodeLabel ? Html::encode($item['label']) : $item['label'];
                }

                $hasActiveChild = false;
                if (isset($item['items'])) {
                    $items[$i]['items'] = $this->normalizeItems($item['items'], $hasActiveChild);
                    if (empty($items[$i]['items']) && $this->hideEmptyItems) {
                        unset($items[$i]['items']);
                        if (!isset($item['url'])) {
                            unset($items[$i]);
                            continue;
                        }
                    }
                }
                if (!isset($item['active'])) {
                    if (($this->activateParents && $hasActiveChild) || ($this->activateItems && $this->isItemActive($item))) {
                        $active = $items[$i]['active'] = true;
                    } else {
                        $items[$i]['active'] = false;
                    }
                } elseif ($item['active'] instanceof \Closure) {
                    $active = $items[$i]['active'] = call_user_func($item['active'], $item, $hasActiveChild, $this->isItemActive($item), $this);
                } elseif ($item['active']) {
                    $active = true;
                }
            }
        }

        return array_values($items);
    }

    /**
     * Registers the needed JavaScript.
     */
    public function registerClientScript()
    {
        $view = $this->getView();
        $view->registerCss(<<<CSS
.submenu a:hover, .submenu a:active, .submenu a.active, .submenu a.active:hover, .submenu a.active:active {
    background: #44b5f6;
    border-color: #44b5f6;
    border-radius: 0;
    color: #fff;
}
CSS
        );
    }

    /**
     * @inheritdoc
     */
    protected function renderItems($items)
    {

        $lines = [];
        foreach ($items as $i => $item) {
            if (!empty($item['items'])) {
                $menu = Html::a(
                    $item['label'] . '<b class="caret"></b>',
                    '#' . $this->options['id'] . '-' . $item['id'],
                    [
                        'class' => 'list-group-item' . ($item['active'] ? ' active' : ''),
                        'data-toggle' => 'collapse',
                        'data-parent' => '#' . $this->options['id'],
                    ]
                );
                $menu .= Html::tag(
                    'div',
                    $this->renderItems($item['items']),
                    [
                        'id' => $this->options['id'] . '-' . $item['id'],
                        'class' => 'submenu panel-collapse collapse'. ($item['active'] ? ' in' : ''),
                    ]
                );

            } else {
                $menu = $this->renderItem($item);
            }
            $lines[] = $menu;
        }
        return implode("\n", $lines);
    }

    /**
     * @inheritdoc
     */
    protected function renderItem($item)
    {
        if (is_array($item) && isset($item['url'])) {
            return Html::a(
                $item['label'],
                $item['url'],
                ['class' => 'list-group-item' . ($item['active'] ? ' active' : '')]
            );
        }else{
            return $item;
        }

    }
}
